//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "MyHello"
#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/Support/Streams.h"
#include "llvm/ADT/Statistic.h"
using namespace llvm;

STATISTIC(HelloCounter, "Counts number of functions greeted");
STATISTIC(empty, "Counts nothing");

namespace {
  // Hello - The first implementation, without getAnalysisUsage.
  struct Hello : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    Hello() : FunctionPass(&ID) {}

	 virtual bool runOnFunction(Function &F) {
		unsigned NumBB;	  
		unsigned NumInst;
		
		HelloCounter++;
		empty = 0;
		NumBB = 0;
		
		//sudiptac: iterate over all the basic blocks inside the function
		for (Function::iterator I = F.begin(), E = F.end(); I != E; I++) {
		  //dynamic cast and get the typed basic block pointer
		  BasicBlock* BB = dyn_cast<BasicBlock>(I);
		  NumBB++;
		  NumInst = 0;

		  //sudiptac: iterate over all the instructions in the basic block
		  for (BasicBlock::iterator Ib = BB->begin(), Eb = BB->end(); Ib != Eb; Ib++) {
					 Instruction* Inst = dyn_cast<Instruction>(Ib);
					 cerr << "I found an instruction opcode \"" << Inst->getOpcodeName() << "\" \n";
					 NumInst++;
		  }
		  
		  cerr << "Number of instructions in basic block " << NumBB << " = " << NumInst << "\n";
		}  
      std::string fname = F.getName();
      EscapeString(fname);
		cerr << "Total number of basic blocks = " << NumBB << "\n"; 
      cerr << "First Implementation MyHello(sudiptac): " << fname << "\n";
      return false;
    }
  };
}

char Hello::ID = 0;
static RegisterPass<Hello> X("MyHello1", "sudiptac: MyHello World Pass");

namespace {
  // Hello2 - The second implementation with getAnalysisUsage implemented.
  struct Hello2 : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    Hello2() : FunctionPass(&ID) {}

    virtual bool runOnFunction(Function &F) {
      HelloCounter++;
      std::string fname = F.getName();
      EscapeString(fname);
      cerr << "Second Implementation MyHello(sudiptac): " << fname << "\n";
      return false;
    }

    // We don't modify the program, so we preserve all analyses
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
      AU.setPreservesAll();
    };
  };
}

char Hello2::ID = 0;
static RegisterPass<Hello2>
Y("MyHello2", "sudiptac: Hello World Pass (with getAnalysisUsage implemented)");
