//===- WcetTest.cpp - writing an LLVM pass for performance testing  ---------------===//
//										current testing for cache performance
//                    	The LLVM Compiler Infrastructure
//
//
//===-----------------------------------------------------------------------------===//


#define DEBUG_TYPE "CacheTest"
//sudiptac: remove this hardcoding
//and possibly move the definitions 
//to some header file
//first few definitions correspond to 
//cache configuration....therefore, 
//it is also advisable for making these 
//parameters configurable from command 
//line
#define ASSOCIATIVITY 4
#define NUM_CACHE_SETS 4
//memory block size is defined in terms 
//of number of instructions it can hold
#define MEM_BLK_SIZE 4

#include "llvm/Pass.h"
#include "llvm/Module.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Constants.h"
#include "llvm/Instructions.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/FunctionUtils.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/Support/Streams.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/ExecutionEngine/Interpreter.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/Target/TargetSelect.h"
#include "llvm/Support/ManagedStatic.h"

using namespace llvm;

STATISTIC(NumFunction, "number of functions analyzed");
STATISTIC(NumBB, "number of basic blocks analyzed");
STATISTIC(NumInst, "number of instructions analyzed");
STATISTIC(NumNewBB, "number of new basic blocks inserted");
STATISTIC(NumMemBlocks, "number of code memory blocks in program");

namespace {
	//Main class of cache performance testing			
  struct CacheTest : public ModulePass {
    static char ID; // Pass identification, replacement for typeid
		
		//collects the map from basic block to accessed memory blocks
		DenseMap <const Value*, std::vector<unsigned> > MemBlockInBB;
		//collects the cache conflict map for a particular memory block
		DenseMap <unsigned, std::vector<unsigned> > CacheConflictMap;
		//global variables created for each memory block
		DenseMap<unsigned, GlobalVariable *> MemGlobal;		
		//global variaables created for cache miss computation in 
		//different cache sets
		DenseMap<unsigned, GlobalVariable *> CacheMissGlobal;		

    CacheTest() : ModulePass(&ID) {}

		unsigned getNumCacheSets() {return NUM_CACHE_SETS;}
		
		unsigned getCacheAssoc() {return ASSOCIATIVITY;}
		
		unsigned getCacheSet(unsigned MemBlock) {return MemBlock % getNumCacheSets();}
		
		void FindOrConstructCacheConflictMap(unsigned MemBB, Module &M);

		void FindOrConstructGlobalVarForCacheMiss(unsigned SetNo, Module &M);
		
		void FindOrConstructGlobalVarForMem(unsigned MemBB, Module &M);

		void PrintMemoryBlockSequence(Module &M);

		void GenerateMemoryBlockSequence(Module &M);

		void UpdateCacheConflict(Module &M, BasicBlock &BB, Value &Conflict, unsigned MemBB);
		
		void AnnotateBBWithCacheMiss(BasicBlock &BB, Module &M);

		void TransformCodeForCacheTest(Module &M);
		
		virtual bool runOnModule(Module &M) {
			//generate memory block sequences for each basic block 
			GenerateMemoryBlockSequence(M);
			//debug purpose
			PrintMemoryBlockSequence(M);
			//transform the code for cache performance testing
			TransformCodeForCacheTest(M);

////debug----
			 // Now we create the JIT.
   ExecutionEngine* EE = EngineBuilder(&M).create();
   Function *FooF = dyn_cast<Function>(M.begin());
 
   cerr << "We just constructed this LLVM module:\n\n" << M;
   cerr << "\n\nRunning foo: ";
   //ceouts().flush();
 
   // Call the `foo' function with no arguments:
   std::vector<GenericValue> noargs;
   GenericValue gv = EE->runFunction(FooF, noargs);
 
   // Import result of execution:
   cerr << "Result: " << gv.IntVal << "\n";
////end debug-----


		  return false;
    }
  };
}

//this function is for debug purpose
//print the memory block sequences collected by "GenerateMemoryBlockSequence"
void CacheTest::PrintMemoryBlockSequence(Module &M)
{
			cerr << "************ <Printing memory blocks of pogram> ************\n";
			for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
					 Function *F = dyn_cast<Function>(IM);

					 for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								//get the local memory block map
								std::vector<unsigned> LocalMemBlock = MemBlockInBB[BB];
								//get the size of basic block (in terms of number of memory blocks)
								std::vector<unsigned>::size_type BBSize = LocalMemBlock.size();

								//iterate through all the memory blocks collected from BB a priori and 
								//print all of them out
								cerr << "=========begin a basic block [" << "size = " << BBSize << "]==========\n";
								for (unsigned BBSI = 0; BBSI < BBSize; BBSI++)
												cerr << " " << LocalMemBlock[BBSI];
								cerr << "\n=========end of basic block===========\n";
					 }
			}	
			cerr << "************ <End Printing> ************\n";
}

//sudiptac: this function collects the memory block sequences in a program. 
//output is a Map structure "MemBlockInBB", which is indexed by each basic 
//block in the program and the value is a sequence (vector) of different 
//memory blocks accessed in the particular basic block....
void CacheTest::GenerateMemoryBlockSequence(Module &M)
{	
			for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
					 //sudiptac: should be reset at the module level. but currently OK			
					 unsigned LastBlockVisited = 0;			
					 unsigned InstructionCounter = 0;			
					 Function *F = dyn_cast<Function>(IM);

					 for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								//use this vector to collect all the memory blocks accessed in basic 
								//block BB
								std::vector<unsigned> LocalMemBlock;
								std::vector<unsigned>::size_type BBSize = LocalMemBlock.size();
								
								//collect the memory block sequence from a single basic block
								for (BasicBlock::iterator IB = BB->begin(), EB = BB->end(); IB != EB; IB ++) {
											//push unique memory blocks 
											if (LocalMemBlock.empty() || LocalMemBlock[BBSize - 1] != LastBlockVisited)
												LocalMemBlock.push_back(LastBlockVisited);	
											//increment instruction counter	
											InstructionCounter++;
											//if the upcoming instruction falls in the boundary of 
											//a new memory block, increment memory block counter
											if (InstructionCounter % MEM_BLK_SIZE == 0) {
												LastBlockVisited++;
												NumMemBlocks++;
											}
											//recompute code size
											BBSize = LocalMemBlock.size();
								}
								//insert the local memory block vector into the global memory block map
								MemBlockInBB[BB] = LocalMemBlock;
					 }
			}	
}

//transform code for cache performance testing --- entry level call
void CacheTest::TransformCodeForCacheTest(Module &M)
{
			//Module* M = CloneModule(&Mx);

		  //iterate over all the functions in the module M
		  for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
					 Function* F = dyn_cast<Function>(IM);
					 NumFunction++;
					 std::string fname = F->getName();
					 EscapeString(fname);
					 cerr << "===========In Function (" << fname << ").....===========\n";
					 std::vector<BasicBlock *> OriginalBBList;

					 //iterate over all the basic blocks inside the function F 
					 for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								//dynamic cast and get the typed basic block pointer
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								NumBB++;
								OriginalBBList.push_back(BB);
					 }

					 for (unsigned BBI = 0; BBI < OriginalBBList.size(); BBI++) {
								//split the successor edges of basic block BB to annotate with cache 
								//information
								AnnotateBBWithCacheMiss(*(OriginalBBList[BBI]), M);

								//iterate over all the instructions in the basic block
								for (BasicBlock::iterator IB = OriginalBBList[BBI]->begin(), EB = OriginalBBList[BBI]->end(); \
												IB != EB; IB++) {
												
												Instruction* Inst = dyn_cast<Instruction>(IB);
												NumInst++;
												cerr << "I found an instruction opcode \"" << Inst->getOpcodeName() << "\" \n";
								}
					 }  
		  }
}

//find or construct a cache conflict map indexed by different memory blocks in the program
void CacheTest::FindOrConstructCacheConflictMap(unsigned MemBB, Module &M) 
{
		std::vector<unsigned>::iterator II;

		//sudiptac: lazy create the cache conflict map, only when needed
		if (CacheConflictMap[MemBB].empty()) {
				for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
						Function *F = dyn_cast<Function>(IM);
						for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								//get the local memory block map
								std::vector<unsigned> LocalMemBlock = MemBlockInBB[BB];
								//get the size of basic block (in terms of number of memory blocks)
								std::vector<unsigned>::size_type BBSize = LocalMemBlock.size();
								
								//go through the memory blocks and create the cache conflict 
								//mapping for memory block "MemBB"
								for (unsigned BBSI = 0; BBSI < BBSize; BBSI++) {
										if(getCacheSet(LocalMemBlock[BBSI]) == getCacheSet(MemBB) && 
												LocalMemBlock[BBSI] != MemBB) {
																//check if the conflict block already exists in the mapping
																II = find(CacheConflictMap[MemBB].begin(), CacheConflictMap[MemBB].end(), \
																				LocalMemBlock[BBSI]);
																//insert only unique cache conflict blocks				
																if (II == CacheConflictMap[MemBB].end())				
																				CacheConflictMap[MemBB].push_back(LocalMemBlock[BBSI]);
										}		
								}
						}
				}
		}	
}

//find or construct a global for the corresponding memory block (MemBB)
void CacheTest::FindOrConstructGlobalVarForCacheMiss(unsigned CacheSet, Module &M) 
{
				if (! CacheMissGlobal.count(CacheSet)) {
								//create the global variable counting cache miss for cache set "CacheSet"
								GlobalVariable *CacheMiss = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
												false, GlobalValue::ExternalLinkage, 0, \
												"__cache_miss" + Twine("_") + Twine(CacheSet), 0, false);
				
								//sudiptac: **IMP** initialize each newly declared global variables 
								Constant* CacheMissInit = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), \
												0, true);
								CacheMiss->setInitializer(CacheMissInit);
												
								//store the cache miss global in a map, so that no duplicate is 
								//generated in future
								CacheMissGlobal[CacheSet] = CacheMiss;
				}
}

//find or construct a global for the corresponding memory block (MemBB)
void CacheTest::FindOrConstructGlobalVarForMem(unsigned MemBB, Module &M) 
{
				if (! MemGlobal.count(MemBB)) {
								//create the global variable conflict
								GlobalVariable *Conflict = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
												false, GlobalValue::ExternalLinkage, 0, \
												"__conflict_cache_test" + Twine("_") + Twine(MemBB), 0, false);
				
								//sudiptac: **IMP** initialize each newly declared global variables 
								Constant* ConflictInit = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), \
												0, true);
								Conflict->setInitializer(ConflictInit);
												
								//store the conflict global in map, so that no duplicate is 
								//generated in future
								MemGlobal[MemBB] = Conflict;
				}
}

//annotate basic block (BB) successors with cache performance
void CacheTest::AnnotateBBWithCacheMiss(BasicBlock &BB, Module &M)
{
			//get the terminator instruction of BB	
			TerminatorInst *TI = BB.getTerminator();
			//get pointer to cache associativity constant
			Value* Assoc = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), getCacheAssoc());
			//get the parent function of basic block "BB"
			Function *F = BB.getParent();
			Function::iterator IF = &BB;

			for (unsigned SuccI = 0, SuccE = TI->getNumSuccessors(); SuccI != SuccE; SuccI++)	{
				//get the original successor of BB
				BasicBlock* DestBB = TI->getSuccessor(SuccI);

				//for each accessed memory block in DestBB, we shall need one conditional 
				//block and one computational block for instrumenting cache performance. 
				std::vector<BasicBlock*> NewBBCond;
				std::vector<BasicBlock*> NewBBComp;
				
				//get the size of the original successor block (in terms of 
				//code memory blocks)
				std::vector<unsigned>::size_type BBSize = MemBlockInBB[DestBB].size();
				std::vector<unsigned> LocalMem = MemBlockInBB[DestBB];

				//create some basic blocks for cache performance instrumentation
				for (unsigned BBSI = 0; BBSI < BBSize; BBSI++) {
								//create two empty basic blocks, first one is conditional 
								//and the second one computes cache misses for the original 
								//successor basic block of BB
								BasicBlock* BBCond = BasicBlock::Create(TI->getContext(), \
												BB.getName() + "." + DestBB->getName() + "_cache_test_cond" + "." + Twine(BBSI));				
								BasicBlock* BBComp = BasicBlock::Create(TI->getContext(), \
												BB.getName() + "." + DestBB->getName() + "_cache_test_comp" + "." + Twine(BBSI));

								NewBBCond.push_back(BBCond);
								NewBBComp.push_back(BBComp);
				}

				//instrument cache performance with the original code
				//iterate through the collected memory blocks and generate instrumented code
				for (int BBI = BBSize - 1; BBI >= 0; BBI--) {
								unsigned BBSI = (unsigned)BBI;
								unsigned MemBB = LocalMem[BBSI];
	
								//we also need to connect the first newly created conditional basic 
								//block or NewBBCond[0] with the original basic block "BB"
								if (BBSI == 0)
												TI->setSuccessor(SuccI, NewBBCond[BBSI]);
			
								//create the global variable only if the global variable for the corresponding 
								//memory block is not created
								FindOrConstructGlobalVarForMem(MemBB, M);
				
								//global value is always a pointer type. Therefore for a typed comparison, 
								//we need to first create a load instruction that would get the value of 
								//global variable
								Value *ConflictVal = new LoadInst(MemGlobal[MemBB], "__conflict_cache_test_val", NewBBCond[BBSI]);
				
								//create conditional instruction
								Value* CondInst = new ICmpInst(*(NewBBCond[BBSI]), ICmpInst::ICMP_SGE, ConflictVal, Assoc, \
												"cond");

								//set the conditional targets of new basic blocks. NewBBCond[i] can branch to 
								//NewBBComp[i] or NewBBCond[i+1]. note that we need special hadling for the 
								//last basic block that is when i == BBSize-1. In that case, NewBBCond[i] can 
								//point to either NewBBComp[i] or DestBB (recall that "DestBB" was the original 
								//destination of basic block "BB" before any instrumentation)
								if (BBSI < BBSize - 1)
												BranchInst::Create(NewBBComp[BBSI], NewBBCond[BBSI + 1], CondInst, NewBBCond[BBSI]);
								else
												BranchInst::Create(NewBBComp[BBSI], DestBB, CondInst, NewBBCond[BBSI]);
				
								//insert the instructions into NewBBComp to modify/update cache conflict count
								UpdateCacheConflict(M, *(NewBBComp[BBSI]), *(MemGlobal[MemBB]), MemBB);
								
								//set the successor of NewBBComp[i] to NewBBCond[i+1], note that 
								//we need special handling when "i" is the last created basic block, 
								//as NewBBComp[i] must unconditionally point to "DestBB" or the 
								//previous destination basic block of "BB"
								if (BBSI < BBSize - 1)
												BranchInst::Create(NewBBCond[BBSI+1], NewBBComp[BBSI]);
								else
												BranchInst::Create(DestBB, NewBBComp[BBSI]);
				
								//insert NewBBCond[i] and NewBBComp[i] into the function in sequence 
								//and just after BB
								IF = &BB;
								F->getBasicBlockList().insert(++IF, NewBBCond[BBSI]);
								IF = NewBBCond[BBSI];
								F->getBasicBlockList().insert(++IF, NewBBComp[BBSI]);

								//we do not have to update any other information as the transformation 
								//is merely done for our analysis purpose and is totally independent 
								//of any other transformation or analysis
								//-----end-----
				
								//for debug statistics
								NumNewBB += 2;
				}
		}
}

//update cache conflict count in the newly inserted basic block
void CacheTest::UpdateCacheConflict(Module &M, BasicBlock &BB, Value &Conflict, unsigned MemBB)
{
				//sudiptac: implement
				unsigned CacheSet = getCacheSet(MemBB);
				
				//create a zero constant
				Value* Zero = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0);
				//create a one constant
				Value* One = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 1);
				
				//Step 1: reset the conflict count for the memory block which is just 
				//loaded into the cache
				new StoreInst(Zero, &Conflict, &BB);

				//Step 2: increment the number of cache misses in the respective cache 
				//set
				
				//lazy create a global variable which computes the number of cache miss
				//in the respective cache set
				FindOrConstructGlobalVarForCacheMiss(CacheSet, M);

				//increment cache miss count by creating a binary operator add
				//load-increment-store chain
				Value *CacheMissVal = new LoadInst(CacheMissGlobal[CacheSet], "__cache_miss_val", &BB);
				Value *MissIncrement = BinaryOperator::CreateAdd(CacheMissVal, One, "__cache_miss_incr", &BB);
				new StoreInst(MissIncrement, CacheMissGlobal[CacheSet], &BB);

				//Step 3: increment the conflict count of all other memory blocks that 
				//may conflict in cache with memory block "MemBB"

				//first, lazy construct the cache conflict map for memory block "MemBB"
				FindOrConstructCacheConflictMap(MemBB, M);
				
				//get the size of conflict
				std::vector<unsigned>::size_type ConflictSize = CacheConflictMap[MemBB].size();
				std::vector<unsigned> CacheConflict = CacheConflictMap[MemBB];

				//go through the cache conflict list and increment respective cache conflict count
				for (unsigned CCI = 0; CCI < ConflictSize; CCI++) {
								//fetch the conflicting memory block id
								unsigned ConflictMem = CacheConflict[CCI];
								//sudiptac: ****CAUTION**** note that some global variables related 
								//to cache conflict may not have been created (as we do a lazy construction). 
								//therefore, you need to call the LAZY global variable allocator here 
								//as well
								FindOrConstructGlobalVarForMem(ConflictMem, M);
								//load-increment-store chain
								Value *ConflictVal = new LoadInst(MemGlobal[ConflictMem], "__conflict_load_val", &BB);
								Value *ConflictIncrement = BinaryOperator::CreateAdd(ConflictVal, One, \
												"__conflict_incr_val", &BB);
								new StoreInst(ConflictIncrement, MemGlobal[ConflictMem], &BB);
				}
				
				//we are done here with instrumentation
}

char CacheTest::ID = 0;
static RegisterPass<CacheTest> X("CacheTest", "cache performance testing");


