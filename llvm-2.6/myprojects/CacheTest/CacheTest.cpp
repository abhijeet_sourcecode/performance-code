//===----------------------------------------------------------------------===//
// main for CacheTest (this file will contain the core structure of performance 
// technique)
//

//performance test related header files
#if 0
#include "CodeTransform.h"
#include "PathProgram.h"
#include "SATEncoder.h"
#include "Invariant.h"
#endif
#include "DynamicSymExecution.h"
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <utility>


using namespace llvm;


typedef std::vector<std::pair<BasicBlock*,bool> > TraceType;
//print the basic block execution trace
void PrintBBList(std::vector<BasicBlock *> BBList, const char* prefix) 
{
  unsigned TraceSize = BBList.size();

  for (unsigned TBBI = 0; TBBI < TraceSize; TBBI++) {
    const char* BBName = (BBList[TBBI]->getName()).data();
    cerr << prefix << " " << BBName << "\n";
  }
}

void process(Module &M, std::ifstream &ISYM, const char* inputF)
{
  // Now we create the LLVM interpreter
  EngineBuilder EBuilder(&M);
  EBuilder.setEngineKind(EngineKind::Interpreter);
  ExecutionEngine* EE = EBuilder.create();
  // get the "main" function of your program
  Function *F = M.getFunction("main");

  //sanity check
  assert(F && "you are a SATAN.....no \"main\" function found in test program");

  cerr << "\nRunning the code.....\n";

  //Call the `main' function with no arguments:
  std::vector<GenericValue> noargs;
  //initialize the BB trace collector
  EE->InitBBTraceCollector();
  //initialize the execution trace collector
  EE->InitExecTraceCollector();
  GenericValue gv = EE->runFunction(F, noargs);

  //collect basic block trace
  cerr << "\nGenerating basic block trace.....\n";
  std::vector<BasicBlock *> BBTrace = EE->getBBTraceCollector();

#ifdef _DEBUG_PP
  //debug trace
  PrintBBList(BBTrace, "[EX]");
#endif

  //collect the execution trace with concrete values --- this will 
  //be used for dynamic symbolic execution of the respective trace
  TraceType &ExecTrace = EE->getExecTraceCollector();

  // FIXME: if there are more than one paths.....
  if (true) {
    //perform dynamic symbolic execution on the trace to get the path 
    //condition
    cerr << "\nNow creating the dynamic symbolic execution engine.....\n";
    DynamicSymExec DSym(&M, &BBTrace, &ExecTrace, inputF);
    //for debug purpose
    //DSym.PrintExecTrace();
    //debug end
    cerr << "\nBuilding path condition from the execution trace.....\n";
    //create the symbolic input names from an input file
    DSym.CreateSymbolicInputs(ISYM);
    //run the dynamic symbolic execution to build the path condition
    DSym.runDynamicPathProgramExploration(inputF);
  }
}


bool run(Module &M, std::ifstream &ISYM, const char* inputF) 
{
  cerr << "\nProcessing...";
  process(M, ISYM, inputF);

  return false;
}

int main(int argc, char **argv) {
  
  std::ofstream time_file;
  struct timeval start_time,end_time;
  double total_time;

  gettimeofday(&start_time, NULL);
  //time_file << start_time.tv_sec << "\n"; 
  //time_file << start_time.tv_usec << "\n"; 

  cerr << "\nCachetest driver started...";
  //we expect the input bitcode file as first argument and the output bitcode 
  //file as the second argument
  //
  if (argc < 3) 
    assert(0 && "insert an input bitcode file and a file describing symbolic input names");
  Module* M;
  std::auto_ptr<MemoryBuffer> Buffer;

  //read the input bitcode file into buffer
  Buffer.reset(MemoryBuffer::getFileOrSTDIN(argv[1]));

  //create the Module structure from buffer
  if (Buffer.get())
    M = ParseBitcodeFile(Buffer.get(), getGlobalContext());

  //check the symbol file which descibes the symbolic inputs
  std::ifstream ISYM;
  ISYM.open(argv[2], std::ios::in);

  //run cache performance testing main loop
  run(*M, ISYM, argv[1]);

  ISYM.close();

  gettimeofday(&end_time, NULL);

  total_time = (end_time.tv_sec - start_time.tv_sec)+((end_time.tv_usec - start_time.tv_usec)/1000000.0);
  cerr << "\nCachetest driver finished";
  cerr << "\nTotal time taken " << total_time <<" seconds\n";

  return 0;
}

