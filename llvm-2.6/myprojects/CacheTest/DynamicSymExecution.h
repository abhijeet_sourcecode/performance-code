#ifndef __DYNAMIC_SYM_EXECUTION_H_
#define __DYNAMIC_SYM_EXECUTION_H_
#include <sys/time.h>
#include "llvm/Pass.h"
#include "llvm/Module.h"
#include "llvm/ModuleProvider.h"
#include "llvm/PassManager.h"
#include "llvm/PassManagers.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Constants.h"
#include "llvm/Instructions.h"
#include "llvm/ValueSymbolTable.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/BasicInliner.h"
#include "llvm/Transforms/Utils/FunctionUtils.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/Support/Streams.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/ExecutionEngine/Interpreter.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/Target/TargetSelect.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Assembly/PrintModulePass.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/raw_ostream.h"
//sudiptac: header file related to STP, SMT solver libraries
#include "../../../stp/src/c_interface/c_interface_llvm.h"
#include <memory>
#include <algorithm>
#include <fstream>
#include <queue>
#include <vector>
#if 0
//sudiptac: performance test related header files
#include "PathProgram.h"
#include "CodeTransform.h"
#include "SATEncoder.h"
#include "Invariant.h"
#endif

using namespace llvm;


class DynamicSymExec {

  public:
    typedef std::vector<std::pair<BasicBlock*, bool> > TraceType;
    typedef std::pair<Instruction *, Value *> UsePair;
    typedef std::vector<std::pair<BasicBlock*, BasicBlock*> > BrListType;
    //the following type is meant to capture a partial path condition
    //(1) the first element is the SMT expression describing the path condition 
    //(2) the second element is the branch outcome path (not just the symbolic 
    //but the full branch path) for the respective partial path
    typedef std::pair<Expr, std::vector<bool> > PCPair;
    typedef std::map<std::vector<bool>, Expr> PCTrackerType;
    typedef std::map<std::vector<bool>,int> PCAssertMap;
    //element type of a path program bucket
    typedef struct bt {
      std::pair<long,long> MinMaxCacheMiss;
      std::pair<long,long> MinMaxExeCycles;
      APInt Cmiss;
    } BucketElemType;
    //typedef std::pair<std::pair<int,int>, APInt > BucketElemType;

    struct BasicBlockSet_t{
      BasicBlock *bblock;
      struct BasicBlockSet_t *next;
    };
    typedef struct BasicBlockSet_t BasicBlockSet_t;

    struct controlGraph_t{
      BasicBlock *bblock;
      BasicBlockSet_t *dSet;
      struct controlGraph_t *next;
    };
    typedef struct controlGraph_t controlGraph_t;

    controlGraph_t *CDG ;


  private:
    Module* M;
    TraceType* ExecTrace;
    std::vector<BasicBlock *> *BBTrace;
    //holds the symbolic input name array
    std::vector<Value *> SymbolicInputVector;
    //holds the LLVM bitcode filename to be handled by KLEE
    std::string KLEEFileName;
    //holds the path file name, to be replayed by KLEE
    std::string KLEEPathFileName;
    //holds the current branch outcome vector in memory
    std::vector<bool> BranchOutcomeVector;
//    std::vector<bool> ViolatedAssertVector;
    //tracks all the partial path conditions
    PCTrackerType PartialPathConditionTracker;
    //tracks the number of assertions with partial path conditions
//    PCAssertMap PartialPathAssertionTracker;
    //tracks all the unexpanded partial path conditions
    PCTrackerType UnexpandedPathConditionTracker;
    //bucket map structure (condition, min cache miss, max cache miss)
    std::map<Expr, BucketElemType > BucketMap;
    //number of path program buckets (with respect to cache performance)
    uint32_t NoOfBuckets;
    //input dependent branch pair list
    BrListType SymBranchList;
    //program under testing
    const char* inputFileName;
    //loop information (the pass need to be scheduled for generation)
    LoopInfo* LI;
    PostDominatorTree* PDT;

  public:
    DynamicSymExec(Module* Mod, std::vector<BasicBlock *> *BBList, TraceType *ExTrace, const char* inputF) : NoOfBuckets(0) {

      M = Mod;
      BBTrace = BBList;
      ExecTrace = ExTrace;
      inputFileName = inputF;
      CDG = NULL;
    }

    //return program under test
    const char* GetTestFileName() {
      return inputFileName;
    }

    //set basic block trace
    void SetBBTrace(std::vector<BasicBlock *>* BBT) {
      BBTrace = BBT;
    }				

    //set execution trace
    void SetExeTrace(TraceType* EXT) {
      ExecTrace = EXT;
    }				

    //create the list of symbolic input names
    void CreateSymbolicInputs(std::ifstream &ISYM);

    //lookup a symbolic input Value in the global symbol table
    //generally used during the inspection of a load instruction
    bool LookupSymbolicInput(Value* LoadValue);

    //replay a path file using KLEE
    int runKLEE();

    //initialize KLEE path file
    void InitKLEEFileName();

    //check whether a particular path is blocked by a path program
    bool CheckForBlockedPathByPathProgram(const std::vector<bool> &BBVector, bool isPath);

    //deviate from the currently executed path by negating the last 
    //branch of a partial path condition
    void TryToDeviateFromCurrentPath();

    //dynamic exploration of path programs.....top level routine
    void runDynamicPathProgramExploration(const char* );

    //build all partial path conditions from current path condition
    void BuildPartialPathConditions();

    //build all the partial branch outcome vectors (0-1-0 format) till a 
    //particular input dependent branch
    std::vector<bool>* BuildPartialBranchIdentityVector(int EndBranchId, bool negate);

    //Build branch outcome vector for the currently executed path
    void BuildBranchOutcomeList();

    //SMT output file manipulation (we design some APIs at the STP solver 
    //side --- which accesses the AST created by the solver)
    uint32_t GetAndSetAllConcreteValues(Expr PPCond, Function *F);

    //store a concrete value into a symbolic input
    void SetConcreteValue(Value* Input, uint64_t InputValue, Function* F, \
        long* ArrayVector, long NameSize, unsigned* NSym);

    //delete all concrete value setup after executing the function "F"
    void DestroyAllConcreteValues(Function* F, uint32_t NSym);

    //schedule loop generation pass and generate loop information
    void GenerateLoopInfo();

    //forcefully inline all the function (**CAUTION** LLVM code base has 
    //been changed to make this work)
    void ForceInlining();
    //schedule PostDominatorTree pass and generate the CDG
    void GenerateCDG();
    void report_error(int er);
    controlGraph_t* search(BasicBlock*);
    controlGraph_t* searchByLabel(std::string);
    void display_CDG();
    void PrintBBTrace();
    int read_assertions(std::string task);
    int makeRecord(std::string task);
    int readUnique(std::string task);
    int getBranches(std::string task);
    int preProcess_step1();
    void preProcess_step2();
    void processedCDG();
    void preProcess();
    int getAffectedAsserts(std::vector<bool>);
    void uniqueViolations();
    void uniqueTouched();
    void adjustAssertionVectors();
    int map_block_to_assertion(std::string );
    void incrementTouched(BasicBlock *);
    void assertion_computation();
    int violatedAsserts();
    void dumpViolatedBlocks(std::string line);
};

#endif
