#include "SATEncoder.h"
#define SOLVER "minisat"
#define PREFIX "MINISAT_X_"
#define POSITIVE 1
#define NEGATED 0
#define SMALLSKIP " "
#define UNSATISFIABLE "UNSAT"
#define INTERNAL_CALL "klee_make_symbolic"


//Log the number of path programs from a subject program (for debugging)
void SATEncoder::computePathProgramNumbers(Module &M)
{
		DenseMap<std::pair<BasicBlock*, BasicBlock*>, unsigned> pathVector;

		cerr << "start printing the paths.....\n";
		
		//for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
				//Function* F = dyn_cast<Function>(IM);
				Function* F = SATModule->getFunction("main");
				//initialization 
				for (Function::iterator IF1 = F->begin(), EF1 = F->end(); IF1 != EF1; IF1++) {
					BasicBlock* BBSrc = dyn_cast<BasicBlock>(IF1);
					for (Function::iterator IF2 = F->begin(), EF2 = F->end(); IF2 != EF2; IF2++) {
							BasicBlock* BBDst = dyn_cast<BasicBlock>(IF2);
							std::pair<BasicBlock*, BasicBlock*> Edge1(BBSrc, BBDst);
							std::pair<BasicBlock*, BasicBlock*> Edge2(BBDst, BBSrc);
							pathVector[Edge1] = 0;
							pathVector[Edge2] = 0;
					}
				}
				#if 0 
				for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
						BasicBlock* BB = dyn_cast<BasicBlock>(IF);
						std::pair<BasicBlock*, BasicBlock*> Edge(BB,BB);
						pathVector[Edge] = 1;
				}
				#endif
				for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
						BasicBlock* BB = dyn_cast<BasicBlock>(IF);
						for (succ_iterator SI = succ_begin(BB), SE = succ_end(BB); SI != SE; SI++) {
								Loop* LBB = LI->getLoopFor(BB);
								BasicBlock* LoopHeader = NULL;
								if (LBB)
									LoopHeader = dyn_cast<BasicBlock>(LBB->getHeader());
								//initialization for the <successor> relation, bit not for the backedge
								if (LoopHeader != *SI) {
									std::pair<BasicBlock*, BasicBlock*> Edge(BB, *SI);
									pathVector[Edge] = 1; 
								}
						}
				}
				cerr << "finished initialization.....\n";
				unsigned count = 0;
				for (Function::iterator IFa = F->begin(), EFa = F->end(); IFa != EFa; IFa++) {
					for (Function::iterator IFb = F->begin(), EFb = F->end(); IFb != EFb; IFb++) {
						for (Function::iterator IFc = F->begin(), EFc = F->end(); IFc != EFc; IFc++) {
								BasicBlock* BBa = dyn_cast<BasicBlock>(IFa);
								BasicBlock* BBb = dyn_cast<BasicBlock>(IFb);
								BasicBlock* BBc = dyn_cast<BasicBlock>(IFc);
								std::pair<BasicBlock*, BasicBlock*> Edgebc(BBb, BBc);		
								std::pair<BasicBlock*, BasicBlock*> Edgeba(BBb, BBa);		
								std::pair<BasicBlock*, BasicBlock*> Edgeac(BBa, BBc);		
			
								pathVector[Edgebc] = pathVector[Edgebc] + pathVector[Edgeba] * pathVector[Edgeac];		
						}
					}
				}
		//}

		DenseMap<std::pair<BasicBlock*, BasicBlock*>, unsigned >::iterator II; 
		
		//print the debug information
		cerr << "start printing the paths.....\n";
		while (!pathVector.empty()) {
			II = pathVector.begin();
			std::pair<BasicBlock*, BasicBlock*> Edge = II->first;
			BasicBlock* src = Edge.first;
			BasicBlock* dest = Edge.second;
			
			cerr << "<" << src->getName().data() << "," << dest->getName().data() << "> = " << II->second << "\n";	
			pathVector.erase(II);
		}
		cerr << "end printing the paths.....\n";
}

//this function creates an MSCC decomposition of Function F
void SATEncoder::ConstructMSCCDecomposition(Function *F)
{
				//go through all the basic blocks in the function and insert them into
				//MSCC entry/exit mapping if they happen to have incoming/outgoing edges 
				//across different MSCCs
				for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								//get the MSCC identity of the basic block BB
								Loop* LBB = LI->getLoopFor(BB);
								Loop* MSCCBB = getMSCCFor(BB);

								//keep a record of all the loops for generating Loop CNF clauses
								if (LBB) {
										std::vector<Loop*>::iterator II;
										II = find(AllLoops.begin(), AllLoops.end(), LBB);
										if (II == AllLoops.end())
												AllLoops.push_back(LBB);	
								}

								//traverse through all the successors of BB and check whether any 
								//successor jumps to a different MSCC
								for (succ_iterator SI = succ_begin(BB), SE = succ_end(BB); \
												SI != SE; SI++) {
												BasicBlock* DestBB = *SI;
												//get the MSCC for the destination basic block
												//sudiptac: changing the LoopFor to MSCCFor
												Loop* LDestBB = LI->getLoopFor(DestBB);
												Loop* MSCCDestBB = getMSCCFor(DestBB);
												//if the source and destination MSCC are different, <BB,DestBB>
												//corresponds to a cross-Loop edge and an outgoing edge from 
												//MSCC "LBB". Therefore, insert <BB,DestBB> into the MSCCExitInfo 
												//of "LBB"
												if (MSCCBB && MSCCBB != MSCCDestBB) {
																std::pair<BasicBlock*, BasicBlock*> ExitEdge(BB, DestBB);
																MSCCExitInfo[LBB].push_back(ExitEdge);
												}
												//(1) source and destination of the control edge belong to 
												//different loops
												//(2) different MSCC or going to lower loop depth
												if (LBB && LBB != LDestBB && (!LDestBB || MSCCBB != MSCCDestBB || 
														LBB->getLoopDepth() > LDestBB->getLoopDepth())) {
																std::pair<BasicBlock*, BasicBlock*> ExitEdge(BB, DestBB);
																LoopExitInfo[LBB].push_back(ExitEdge);
												}
								}
								//now traverse through the predecessor list of basic block and check 
								//whether any edge jumps to this MSCC i.e. "LBB"
								for (pred_iterator PI = pred_begin(BB), PE = pred_end(BB); \
												PI != PE; PI++) {
												BasicBlock* SrcBB = *PI;
												//get the MSCC for the predecessor basic block
												//sudiptac: changing the LoopFor to MSCCFor
												Loop* LSrcBB = LI->getLoopFor(SrcBB);
												Loop* MSCCSrcBB = getMSCCFor(SrcBB);
												//if the source and destination belong to different MSCC, then 
												//insert the edge <SrcBB,BB> into the MSCCEntryInfo of "LBB"
												if (MSCCBB && MSCCBB != MSCCSrcBB) {
																std::pair<BasicBlock*, BasicBlock*> EntryEdge(SrcBB, BB);
																MSCCEntryInfo[LBB].push_back(EntryEdge);
												}
												//(1) source and destination of the control edge belong to 
												//different loops
												//(2) different MSCC or going to lower loop depth
												if (LBB && LBB != LSrcBB && (!LSrcBB || MSCCBB != MSCCSrcBB || 
														LBB->getLoopDepth() > LSrcBB->getLoopDepth())) {
																std::pair<BasicBlock*, BasicBlock*> EntryEdge(BB, SrcBB);
																LoopEntryInfo[LBB].push_back(EntryEdge);
												}
								}
				}
				////=====finish MSCC decomposition
}

//this function creates the SAT encoding of the MSCC decomposition of a module. 
//SAT encoding is computed according to the "MINISAT" solver format and is 
//dumped into a file. This created file is later processed by MINISAT to extract 
//different path programs
void SATEncoder::ConstructSATEncoding()
{
				//iterate over all the basic blocks of "main" and produce the SAT encoding
				Function* F = SATModule->getFunction("main");
				//sanity check
				assert(F && "you are a SATAN.....no \"main\" function found in test program");
				std::vector<Function*> ToBeRemovedFromParent;
				
				//before doing everything, inline the functions using the basic inline 
				//implementor class. I am not sure what it does when you have recursive 
				//calls. with the current implementation, I manually write an iteartive 
				//version of the recursive function. Overall, I assume that there are 
				//no recursive function in your test program

				BasicInliner InlineF;
				//it's a whole sale inlining dude, therefore insert all the functions inside 
				//the queue to be inlined
				for (Module::iterator IM = SATModule->begin(), EM = SATModule->end(); IM != EM; IM++) {
						Function* FCall = dyn_cast<Function>(IM);
						if (!(FCall->getName().equals(INTERNAL_CALL)))
								InlineF.addFunction(FCall);
						if (FCall != F && !(FCall->getName().equals(INTERNAL_CALL)))
								ToBeRemovedFromParent.push_back(FCall);
				}
				//call the inliner, "true" argument force the inliner to always inline irrespective of 
				//the cost.....because note that we do not care about the cost of inlining, as it is 
				//entirely virtual for the purpose of quantitative analysis
				InlineF.inlineFunctions(true);

				//now erase all the functions from the parent module except "main", phew.....
				//just like that, everybody is gone ;-)
				for (unsigned RI = 0; RI < ToBeRemovedFromParent.size(); RI++) {
						Function* FCall = ToBeRemovedFromParent[RI];
						FCall->dropAllReferences();
				}		

				//create a function pass manager to add existing loop analysis. note that 
				//to create the SAT encoding of MSCC decomposition, we need loop informat
				//-ion, and therefore, we first schedule the "LoopInfo" pass
				FunctionPassManager* OurFPM;
				OurFPM = new FunctionPassManager(new ExistingModuleProvider(SATModule));
				//create the loop info generation pass
				LI = createLoopInfoPass();
				OurFPM->add(LI);
				OurFPM->doInitialization();
				
				//iterate over all the basic blocks of "main" and produce the SAT encoding
				F = SATModule->getFunction("main");
				//sanity check
				assert(F && "you are a SATAN.....no \"main\" function found in test program");

				//run the LoopInfo Pass to get the informations abour program loops
				OurFPM->run(*F);

				//now as we have the loop information, construct an MSCC decomposition of 
				//the module
				ConstructMSCCDecomposition(F);
				
				for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								TerminatorInst* TI = BB->getTerminator();

								//special handling for entry and exit node
								//
								//construct special CNF clauses for program "entry" node
								if (BB == &(BB->getParent()->getEntryBlock())) {
												cerr << "\n\nentry node found.....generating clauses for entry node.....\n";
												ConstructClausesForEntryNode(BB);
								}
				
								//construct special CNF clauses for the program "exit" node
								if (TI && TI->getOpcode() == Instruction::Ret) {
												cerr << "\n\nexit node found.....generating clauses for exit node.....\n";
												ConstructClausesForExitNode(BB);
								}

								for (succ_iterator SI = succ_begin(BB), SE = succ_end(BB); SI != SE; SI++) {
												BasicBlock* DestBB = *SI;
												Loop* LBB = getMSCCFor(BB);
												Loop* LDestBB = getMSCCFor(DestBB);
												//we do not care if both the source and the destination of the control 
												//flow edge belong to the same MSCC. The SAT encoding is based on MSCC 
												//decomposition and the edges inside the same MSCC are not distinguished
												if (!LBB || !LDestBB || LBB != LDestBB) 
																ConstructCNFClausesForEdge(BB, DestBB);

								}
				}

				//finally, construct the CNF clauses to distinguish edges inside a loop/MSCC
				//crucial to distinguish performance characteristic inside a loop/MSCC
				ConstructCNFClausesForMSCCEdges();
			
				//compute some path information, only for debugging
				computePathProgramNumbers(*SATModule);
}

//construct CNF clauses to distinguish different paths inside a loop.....different 
//paths inside a loop may exhibit totally different performance characteristics

//sudiptac: this is a bit unoptimized way of doing this, however, we perform this 
//encoding only once. Therefore, should not create a bottleneck in analysis performance
void SATEncoder::ConstructCNFClausesForMSCCEdges() 
{
				//fetch the main function of SAT module
				Function* F = SATModule->getFunction("main");
				//sanity check
				assert(F && "you are a SATAN.....no \"main\" function found in test program");

				for (unsigned LoopI = 0; LoopI < AllLoops.size(); LoopI++) {
						Loop* MSCCLoop = AllLoops[LoopI];	
						
						//for each loop, generate CNF clauses connecting its entry and exit (loops 
						//are, of course, bounded						
						std::vector<PredType> SuccList = LoopExitInfo[MSCCLoop];
						std::vector<PredType> PredList = LoopEntryInfo[MSCCLoop];
						std::vector<LiteralType> ExitLpComponents;
						
						//add loop clauses of the form entry1 \/ entry2 -> exit1 \/ exit2
						//where entry* are the set of loop entry edges and exit* are the 
						//set of loop exit edges
						
						//CNF clauses to encode that all loops are bounded
						//loop entry-exit clauses ---<<<<<start>>>>>---
						for (unsigned SuccI = 0; SuccI < SuccList.size(); SuccI++) {
								PredType Edge = SuccList[SuccI];
								ConstructEdgePredicates(Edge.first, Edge.second);
								PredType ExPair(Edge.first, Edge.second);
								LiteralType InLp(ExPair, POSITIVE);
								//save the exit loop edge predicates in the vector
								ExitLpComponents.push_back(InLp);
						}

						for (unsigned PredI = 0; PredI < PredList.size(); PredI++) {
								ClauseType AddCNFClause;
								PredType Edge = PredList[PredI];
								ConstructEdgePredicates(Edge.first, Edge.second);
								PredType InPair(Edge.first, Edge.second);
								LiteralType OutLp(InPair, NEGATED);
								//first add all the entry loop literals
								for (unsigned sizeI = 0; sizeI < ExitLpComponents.size(); sizeI++) 
									AddCNFClause.push_back(ExitLpComponents[sizeI]);
								AddCNFClause.push_back(OutLp);
								//finished building the clause.....add it to the SAT encoder database
								CNFFormula.push_back(AddCNFClause);
								NumClauses++;
						}
						//loop entry-exit clauses ---<<<<<end>>>>>---
				}
						
				//basic block in-flow-out-flow clauses --<<<<<start>>>>>--
				for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
						BasicBlock* BB = dyn_cast<BasicBlock>(IF);

						//for each basic block inside a loop, we want to create clauses as 
						//follows: 
						//entry1 \/ entry2 -> exit1 \/ exit2, where *entry* denotes the 
						//incoming edges into the basic block and *exit* denotes the exit 
						//edges of the basic block

						Loop* LBB = LI->getLoopFor(BB);

						//the following CNF clauses are generated only for the basoc blocks inside 
						//a loop, skip all the basic blocks which can be executed at most once
						if (!LBB) continue;

						std::vector<LiteralType> ExitComponents;

						for (succ_iterator SI = succ_begin(BB), SE = succ_end(BB); SI != SE; SI++) {
							BasicBlock* DestBB = *SI;
							ConstructEdgePredicates(BB, DestBB);
							PredType OutPair(BB, DestBB);
							LiteralType OutL(OutPair, POSITIVE);
							ExitComponents.push_back(OutL);
						}
									
						for (pred_iterator PI = pred_begin(BB), PE = pred_end(BB); PI != PE; PI++) {
							ClauseType AddCNFClause;
							BasicBlock* PredBB = *PI;
							ConstructEdgePredicates(PredBB, BB);
							PredType InPair(PredBB, BB);
							LiteralType InL(InPair, NEGATED);
						
							//first add the outgoing edges	
							for (unsigned sizeI = 0; sizeI < ExitComponents.size(); sizeI++) 
								AddCNFClause.push_back(ExitComponents[sizeI]);
								//add each of the incoming edge next
								AddCNFClause.push_back(InL);
								//finished one clause.....now add it into the SAT encoder database
								CNFFormula.push_back(AddCNFClause);
								NumClauses++;	
							}
							//basic block in-flow-out-flow clauses ---<<<<<end>>>>>---
								
							#if 0
								//another correct, but likely imprecise encoding of the loop edges of the 
								//form: e->entry1 \/ entry2 \/ .... \/ entryn
								for (succ_iterator SI = succ_begin(BB), SE = succ_end(BB); SI != SE; SI++) {
										BasicBlock* DestBB = *SI;
										Loop* LBB = LI->getLoopFor(BB);
										Loop* LDestBB = LI->getLoopFor(DestBB);
										//create the CNF clauses for MSCC loop (whole program loop excluded)
										if (LBB && LDestBB && LBB == LDestBB && LBB == MSCCLoop) {
												//BasicBlock* LoopHeader = dyn_cast<BasicBlock>(LBB->getHeader());
												//skip the backedge.....
												//if (LoopHeader == DestBB) continue;
												ClauseType CNFClause;
												ConstructEdgePredicates(BB, DestBB);
												PredType InInPair(BB, DestBB);
												LiteralType InInL(InInPair, NEGATED);
												CNFClause.push_back(InInL);
												//fetch all the entry flow edges to this loop
												//one clause is of the form: "e->entry1 \/ entry2 \/ ..... \/entryn"
												std::vector<PredType> PredList = MSCCEntryInfo[MSCCLoop];
												for (unsigned PredI = 0; PredI < PredList.size(); PredI++) {
														PredType Edge = PredList[PredI];
														ConstructEdgePredicates(Edge.first, Edge.second);
														PredType InPair(Edge.first, Edge.second);
														LiteralType InL(InPair, POSITIVE);
														CNFClause.push_back(InL);
														//finished one clause....now add it into the SAT 
														//encoding
														CNFFormula.push_back(CNFClause);
														//one more CNF clause added to the database
														NumClauses++;
												}
														
									  }
								}
								#endif
				}
}

//construct CNF clauses when the source of the edge is inside an MSCC but 
//the destination of the edge is outside of any loop
void SATEncoder::ConstructCNFClausesForEdge(BasicBlock* BB, BasicBlock* DestBB)
{
				//handle the special cases for source node and sink node,
				//that is when either "BB" is the entry of the program or 
				//"DestBB" is the exit of the program
				
				TerminatorInst *TI = DestBB->getTerminator();
			
				//generate incoming edge constraints only if the source node 
				//of the edge is not the program "entry"
				if (!(BB == &(BB->getParent()->getEntryBlock()))) {
								//note that we need to distinguish whether the source of the 
								//edge belongs to MSCC or not. In case the source belongs to 
								//some MSCC, we need to consider all the incoming edges to 
								//the corresponding MSCC while generating clauses
								if (getMSCCFor(BB))
												ConstructClausesForInConsMSCC(BB, DestBB);
								else
												ConstructClausesForInCons(BB, DestBB);
												
				}
				
				//sudiptac: note that there won't be any change for the outgoing
				//edge constraints encoding if the destination is outside any 
				//loop
				//
				//generate outgoing edge constraints only if the destination 
				//node of the edge is not the program "exit"
				if (!(TI && TI->getOpcode() == Instruction::Ret)) {
								//note that we need to distinguish whether the destination of 
								//the edge belongs to MSCC or not. In case the destination 
								//belongs to some MSCC, we need to consider all the outgoing 
								//edges to the corresponding MSCC while generating clauses
								if (getMSCCFor(DestBB))
												ConstructClausesForOutConsMSCC(BB, DestBB);
								else
												ConstructClausesForOutCons(BB, DestBB);
				}
}

//construct CNF clauses for special handling of program entry node (why special? 
//because any path must visit the entry node, that is why)
void SATEncoder::ConstructClausesForEntryNode(BasicBlock* EntryBB)
{
				succ_iterator SI = succ_begin(EntryBB), SE = succ_end(EntryBB);				

				//go through all the successors of "EntryBB" and construct the 
				//predicates for the corresponding outgoing edges from "EntryBB"
				for (; SI != SE; SI++) {
								BasicBlock* DestBB = *SI;
								ConstructEdgePredicates(EntryBB, DestBB);
				}
				
				//=====create the Clause C1 
				//Clause C1 says that at least one of the outgoing edges from the 
				//entry node must have been executed
				ClauseType CNFClause;

				for (succ_iterator SI = succ_begin(EntryBB); SI != SE; SI++) {
								BasicBlock* DestBB = *SI;
								PredType OutPair(EntryBB, DestBB);
								LiteralType OutL(OutPair, POSITIVE);
								CNFClause.push_back(OutL);
				}
				//add the clause into the final CNF formula
				CNFFormula.push_back(CNFClause);
				//one more CNF clause
				NumClauses++;
				//=====end constructing clause C1

				//the following loop creates Clauses to ensure that "exactly 
				//one" of the outgoing edges from the entry node was executed 
				//and no more
				for (succ_iterator SI = succ_begin(EntryBB); SI != SE; ) {	
								BasicBlock* DestBB1 = *SI;
								succ_iterator SII = ++SI;
								PredType OutPair1(EntryBB, DestBB1);
								LiteralType OutL1(OutPair1, NEGATED);
								
								for (; SII != SE; SII++) {
												ClauseType OtherCNFClause;
												OtherCNFClause.push_back(OutL1);
												BasicBlock* DestBB2 = *SII;
												PredType OutPair2(EntryBB, DestBB2);
												LiteralType OutL2(OutPair2, NEGATED);
												OtherCNFClause.push_back(OutL2);
												//finished one clause....now add it into the SAT 
												//encoding
												CNFFormula.push_back(OtherCNFClause);
												//one more CNF clause
												NumClauses++;
								}
				}
				//end constructing exclusion clauses
}

//construct special CNF clauses for the program exit node. these 
//clauses must satisfy the criteria that any path must visit the 
//program "exit" node
void SATEncoder::ConstructClausesForExitNode(BasicBlock* ExitBB)
{
				pred_iterator PI = pred_begin(ExitBB), PE = pred_end(ExitBB);				

				//go through all the predecessors of "ExitBB" and construct the 
				//predicates for the corresponding incoming edges to "ExitBB"
				for (; PI != PE; PI++) {
								BasicBlock* SrcBB = *PI;
								ConstructEdgePredicates(SrcBB, ExitBB);
				}
				
				//=====create the Clause C1 
				//Clause C1 says that at least one of the incoming edges to the 
				//exit node must have been executed
				ClauseType CNFClause;

				for (pred_iterator PI = pred_begin(ExitBB); PI != PE; PI++) {
								BasicBlock* SrcBB = *PI;
								PredType InPair(SrcBB, ExitBB);
								LiteralType InL(InPair, POSITIVE);
								CNFClause.push_back(InL);
				}
				//add the clause into the final CNF formula
				CNFFormula.push_back(CNFClause);
				//one more CNF clause
				NumClauses++;
				//=====end constructing clause C1

				//the following loop creates Clauses to ensure that "exactly 
				//one" of the incoming edges to the exit node was executed 
				//and no more
				for (pred_iterator PI = pred_begin(ExitBB); PI != PE; ) {	
								BasicBlock* SrcBB1 = *PI;
								pred_iterator PII = ++PI;
								PredType InPair1(SrcBB1, ExitBB);
								LiteralType InL1(InPair1, NEGATED);
								
								for (; PII != PE; PII++) {
												ClauseType OtherCNFClause;
												OtherCNFClause.push_back(InL1);
												BasicBlock* SrcBB2 = *PII;
												PredType InPair2(SrcBB2, ExitBB);
												LiteralType InL2(InPair2, NEGATED);
												OtherCNFClause.push_back(InL2);
												//finished one clause....now add it into the SAT 
												//encoding
												CNFFormula.push_back(OtherCNFClause);
												//one more CNF clause
												NumClauses++;
								}
				}
				//end constructing exclusion clauses
}

//construct CNF clauses to identify incoming edge constraints
void SATEncoder::ConstructClausesForInCons(BasicBlock* BB, BasicBlock* DestBB)
{
				pred_iterator PI = pred_begin(BB), PE = pred_end(BB);				
				PredType OutPair(BB, DestBB);

				
				//create the predicate for outgoing edge
				ConstructEdgePredicates(BB, DestBB);

				//go through all the predecessors of the basic block "BB" and 
				//construct the predicates for the corresponding incoming edges 
				//to "BB"
				for (; PI != PE; PI++) {
								BasicBlock* SrcSrcBB = *PI;
								ConstructEdgePredicates(SrcSrcBB, BB);
				}
				
				//=====create the Clause C1 
				//it says that if DestBB has been reached then one of the incoming 
				//edges of BB must have been executed (an exception is when BB is 
				//the entry block of program. this exception is handled separately)
				LiteralType OutL(OutPair, NEGATED);
				ClauseType CNFClause;
				CNFClause.push_back(OutL);

				for (pred_iterator PI = pred_begin(BB); PI != PE; PI++) {
								BasicBlock* SrcSrcBB = *PI;
								PredType InInPair(SrcSrcBB, BB);
								LiteralType InInL(InInPair, POSITIVE);
								CNFClause.push_back(InInL);
				}
				//add the clause into the final CNF formula
				CNFFormula.push_back(CNFClause);
				//one more CNF clause
				NumClauses++;
				//=====end constructing clause C1

				//the following loop creates Clauses to ensure that "exactly one" 
				//of the incoming edges of basic block "BB" can be executed and 
				//no more
				for (pred_iterator PI = pred_begin(BB); PI != PE; ) {
								BasicBlock* SrcSrcBB1 = *PI;
								PredType InInPair1(SrcSrcBB1, BB);
								LiteralType InInL1(InInPair1, NEGATED);
								pred_iterator PII = ++PI;
								
								for (; PII != PE; PII++) {
												ClauseType OtherCNFClause;
												OtherCNFClause.push_back(OutL);
												OtherCNFClause.push_back(InInL1);
												BasicBlock* SrcSrcBB2 = *PII;
												PredType InInPair2(SrcSrcBB2, BB);
												LiteralType InInL2(InInPair2, NEGATED);
												OtherCNFClause.push_back(InInL2);
												//finished one clause....now add it into the SAT 
												//encoding
												CNFFormula.push_back(OtherCNFClause);
												//one more CNF clause
												NumClauses++;
								}
				}
				//end constructing exclusion clauses
}

//construct CNF clauses to identify outgoing edge constraints
void SATEncoder::ConstructClausesForOutCons(BasicBlock* BB, BasicBlock* DestBB)
{
				succ_iterator SI = succ_begin(DestBB), SE = succ_end(DestBB);				
				PredType OutPair(BB, DestBB);

				
				//create the predicate for outgoing edge
				ConstructEdgePredicates(BB, DestBB);

				//go through all the successors of "DestBB" and construct the 
				//predicates for the corresponding outgoing edges from "DestBB"
				for (; SI != SE; SI++) {
								BasicBlock* DestDestBB = *SI;
								ConstructEdgePredicates(DestBB, DestDestBB);
				}
				
				//=====create the Clause C1 
				//it says that if DestBB has been reached then one of the outgoing 
				//edges from "DestBB" must have been executed (an exception is when 
				//DestBB is the exit block of program. this exception is handled 
				//separately)
				LiteralType OutL(OutPair, NEGATED);
				ClauseType CNFClause;
				CNFClause.push_back(OutL);

				for (succ_iterator SI = succ_begin(DestBB); SI != SE; SI++) {
								BasicBlock* DestDestBB = *SI;
								PredType OutOutPair(DestBB, DestDestBB);
								LiteralType OutOutL(OutOutPair, POSITIVE);
								CNFClause.push_back(OutOutL);
				}
				//add the clause into the final CNF formula
				CNFFormula.push_back(CNFClause);
				//one more CNF clause
				NumClauses++;
				//=====end constructing clause C1

				//the following loop creates Clauses to ensure that "exactly 
				//one" of the outgoing edges of basic block "DestBB" can be 
				//executed and no more
				for (succ_iterator SI = succ_begin(DestBB); SI != SE; ) {
								BasicBlock* DestDestBB1 = *SI;
								PredType OutOutPair1(DestBB, DestDestBB1);
								LiteralType OutOutL1(OutOutPair1, NEGATED);	
								succ_iterator SII = ++SI;

								for (; SII != SE; SII++) {
												ClauseType OtherCNFClause;
												OtherCNFClause.push_back(OutL);
												OtherCNFClause.push_back(OutOutL1);
												BasicBlock* DestDestBB2 = *SII;
												PredType OutOutPair2(DestBB, DestDestBB2);
												LiteralType OutOutL2(OutOutPair2, NEGATED);
												OtherCNFClause.push_back(OutOutL2);
												//finished one clause....now add it into the SAT 
												//encoding
												CNFFormula.push_back(OtherCNFClause);
												//one more CNF clause
												NumClauses++;
								}
				}
				//end constructing exclusion clauses
}

//construct CNF clauses for an edge when the source of the edge belong to some 
//MSCC and the destination of the edge is outside of any loop
void SATEncoder::ConstructClausesForInConsMSCC(BasicBlock* BB, BasicBlock* DestBB)
{
				PredType OutPair(BB, DestBB);

				//create the predicate for outgoing edge
				ConstructEdgePredicates(BB, DestBB);

				//since BB here belongs to an MSCC, we need to consider all the 
				//incoming edges to the corresponding MSCC
				Loop* LBB = getMSCCFor(BB);
				assert(LBB && "failed to get MSCC");
				std::vector<PredType> PredList = MSCCEntryInfo[LBB];
				for (unsigned PredI = 0; PredI < PredList.size(); PredI++) {
								PredType Edge = PredList[PredI];
								ConstructEdgePredicates(Edge.first, Edge.second);
				}
				
				//=====create the Clause C1 
				//it says that if DestBB has been reached then one of the incoming 
				//edges to the MSCC containing BB must have been executed 
				LiteralType OutL(OutPair, NEGATED);
				ClauseType CNFClause;
				CNFClause.push_back(OutL);

				for (unsigned PredI = 0; PredI < PredList.size(); PredI++) {
								PredType Edge = PredList[PredI];
								PredType InInPair(Edge.first, Edge.second);
								LiteralType InInL(InInPair, POSITIVE);
								CNFClause.push_back(InInL);
				}
				//add the clause into the final CNF formula
				CNFFormula.push_back(CNFClause);
				//one more CNF clause
				NumClauses++;
				//=====end constructing clause C1

				//the following loop creates Clauses to ensure that "exactly one" 
				//of the incoming edges of the MSCC containing basic block "BB" 
				//can be executed and no more
				for (unsigned PredI = 0; PredI < PredList.size(); PredI++) {
								PredType Edge1 = PredList[PredI];
								PredType InInPair1(Edge1.first, Edge1.second);
								LiteralType InInL1(InInPair1, NEGATED);
								unsigned PredII = ++PredI;

								for (; PredII < PredList.size(); PredII++) {
												ClauseType OtherCNFClause;
												OtherCNFClause.push_back(OutL);
												OtherCNFClause.push_back(InInL1);
												PredType Edge2 = PredList[PredII];
												PredType InInPair2(Edge2.first, Edge2.second);
												LiteralType InInL2(InInPair2, NEGATED);
												OtherCNFClause.push_back(InInL2);
												//finished one clause....now add it into the SAT 
												//encoding
												CNFFormula.push_back(OtherCNFClause);
												//one more CNF clause
												NumClauses++;
								}
				}
				//end constructing exclusion clauses
}

//construct CNF clauses to identify outgoing edge constraints, this function is 
//called for all the edges whose destination basic block belongs to some MSCC
void SATEncoder::ConstructClausesForOutConsMSCC(BasicBlock* BB, BasicBlock* DestBB)
{
				PredType OutPair(BB, DestBB);
				
				//create the predicate for outgoing edge
				ConstructEdgePredicates(BB, DestBB);
				
				//since "DestBB" resides inside an MSCC, we need to consider all outgoing 
				//edges from the corresponding MSCC
				Loop* LDestBB = getMSCCFor(DestBB);
				assert(LDestBB && "failed to get MSCC");
				std::vector<PredType> SuccList = MSCCExitInfo[LDestBB];
				for (unsigned SuccI = 0; SuccI < SuccList.size(); SuccI++) {
								PredType Edge = SuccList[SuccI];
								ConstructEdgePredicates(Edge.first, Edge.second);
				}
				
				//=====create the Clause C1 
				//it says that if DestBB has been reached then one of the outgoing 
				//edges from the MSCC containing "DestBB" must have been executed
				LiteralType OutL(OutPair, NEGATED);
				ClauseType CNFClause;
				CNFClause.push_back(OutL);

				for (unsigned SuccI = 0; SuccI < SuccList.size(); SuccI++) {
								PredType Edge = SuccList[SuccI];
								PredType OutOutPair(Edge.first, Edge.second);
								LiteralType OutOutL(OutOutPair, POSITIVE);
								CNFClause.push_back(OutOutL);
				}
				//add the clause into the final CNF formula
				CNFFormula.push_back(CNFClause);
				//one more CNF clause
				NumClauses++;
				//=====end constructing clause C1

				//the following loop creates Clauses to ensure that "exactly 
				//one" of the outgoing edges from the MSCC containing basic 
				//block "DestBB" can be executed and no more
				for (unsigned SuccI = 0; SuccI < SuccList.size(); SuccI++) {
								PredType Edge1 = SuccList[SuccI];
								PredType OutOutPair1(Edge1.first, Edge1.second);
								LiteralType OutOutL1(OutOutPair1, NEGATED);	
								unsigned SuccII = ++SuccI;

								for (; SuccII < SuccList.size(); SuccII++) {
												ClauseType OtherCNFClause;
												OtherCNFClause.push_back(OutL);
												OtherCNFClause.push_back(OutOutL1);
												PredType Edge2 = SuccList[SuccII];
												PredType OutOutPair2(Edge2.first, Edge2.second);
												LiteralType OutOutL2(OutOutPair2, NEGATED);
												OtherCNFClause.push_back(OutOutL2);
												//finished one clause....now add it into the SAT 
												//encoding
												CNFFormula.push_back(OtherCNFClause);
												//one more CNF clause
												NumClauses++;
								}
				}
				//end constructing exclusion clauses
}


//construct a literal for the control flow edge represented by <BB,DestBB>
void SATEncoder::ConstructEdgePredicates(BasicBlock* BB, BasicBlock* DestBB) 
{
				PredType BBPair(BB, DestBB);
				
				//lazy construct the literal. if the literal has been constructed 
				//before, then just use the old version
				if (!EdgePredicates.count(BBPair)) {
								/* cerr << "I am here to construct a predicate " << BB->getName().data() << ", " \
												<< DestBB->getName().data() << ".....\n"; */
								NumPredicates++;
								//construct the literal name
								Twine TS (PREFIX + BB->getName() + "_" + DestBB->getName());
								//construct the <name,identity> pair of a predicate
								std::pair<std::string,unsigned> Pred(TS.str(), NumPredicates);
								//save the literal name in the literal map
								EdgePredicates[BBPair] = Pred;
				}
}

//construct blocking clauses for a set of edges. The set of edges are called 
//blocking edges and they are extracted after analyzing a path program. The 
//blocking clauses will be added iteratively until the entire CNF formula 
//becomes unsatisfiable ---- which essentially means that we have analyzed 
//all the path programs and nothing is left to explore
void SATEncoder::AddBlockingClauses(std::vector<PredType> BlockingEdges, std::vector<PredType> IntfEdges)
{
				std::vector<PredType>::size_type BlockingSize = BlockingEdges.size();
				std::vector<PredType>::size_type IntfSize = IntfEdges.size();
				ClauseType CNFClause;
				
				//block the set of sufficient edges
				for (unsigned BlockI = 0; BlockI < BlockingSize; BlockI++) {
								PredType Edge = BlockingEdges[BlockI];
								//make sure that the predicate store has the element for 
								//the corresponding edge
								ConstructEdgePredicates(Edge.first, Edge.second);
								LiteralType BlockingLiteral(Edge, NEGATED);
								CNFClause.push_back(BlockingLiteral);
				}
				//unblock the set of interference edges
				for (unsigned BlockI = 0; BlockI < IntfSize; BlockI++) {
								PredType Edge = IntfEdges[BlockI];
								//make sure that the predicate store has the element for 
								//the corresponding edge
								ConstructEdgePredicates(Edge.first, Edge.second);
								LiteralType BlockingLiteral(Edge, POSITIVE);
								CNFClause.push_back(BlockingLiteral);
				}
				//the CNF clause has been constructed....now add it to the overall SAT encoding
				CNFFormula.push_back(CNFClause);
				NumClauses++;
}

//print the SAT encoding in a file that can be processsed by a satisfiability 
//solver
//
//our printing follows the "MINISAT" tool format..this function needs to be 
//changed if you use a different solver
void SATEncoder::PrintSATEncoding(unsigned int ExploreCount)
{	
				//manipulate the SAT filename to keep different versions (after 
				//each individual iteration of adding blocking clause)
				std::string CurrentSATFileName(SATFileName->data());
				if (ExploreCount) {
								std::string Index((Twine(ExploreCount)).str());
								CurrentSATFileName += ".";
								CurrentSATFileName += Index;
				}
				
				//open the output file which will store the SAT encoding
				std::ofstream OS(CurrentSATFileName.data(), std::ios_base::out|std::ios::trunc);
				
				//the entire encoding is stored in CNFFormula.....therefore, CNFFormula 
				//is our starting point
				
				//print the mandatory prefix section (problem, number of clauses, number 
				//of predicates etc)
				OS << "p cnf " << getNumPredicates() << " " << getNumClauses() << "\n";
				
				//go through all the CNF clauses
				for (unsigned IC = 0; IC < CNFFormula.size(); IC++) {
								ClauseType CNFC = CNFFormula[IC];
								
								//go through all the literals of clause CNFC1 and print them
								
								//print the identity (first: string, second: unsigned id)
								//we print in two formats --- the string format is human 
								//readable and is inserted as a comment (so that the 
								//human readable format is ignored by MINISAT)
								
								//put a comment tag
								OS << "c" << "    ";
								for (unsigned IL = 0; IL < CNFC.size(); IL++) {
												LiteralType PredL = CNFC[IL];

												//print the literal---note that the second element of 
												//the literal pair is its sign (either positive or 
												//negation) and the first element of the pair is the 
												//unique identity of the predicate
												//
												//print the sign
												if (PredL.second == NEGATED)
																OS << "-";
												OS << EdgePredicates[PredL.first].first << " | ";
								}
								//mark the end of the clause
								OS << "0";
								
								//we have finished printing one clause, let us put a newline
								OS << "\n";

								//now print the second format of CNF clause, which is actually 
								//processed by MINISAT
								for (unsigned IL = 0; IL < CNFC.size(); IL++) {
												LiteralType PredL = CNFC[IL];
												//print the sign
												if (PredL.second == NEGATED)
																OS << "-";
												//no comments here, please				
												OS << EdgePredicates[PredL.first].second << "    ";
								}
								//mark the end of the clause
								OS << "0";
								
								//we have finished printing one clause, let us put a newline
								OS << "\n";
				}
				OS.close();
}

//solve a given SAT file through MINISAT, this API must be changed if a different 
//SAT solver is used
bool SATEncoder::Solve(unsigned int ExploreCount)
{
				//manipulate the SAT filename to keep different versions (after 
				//each individual iteration of adding blocking clause)
				std::string CurrentSATFileName(SATFileName->data());
				std::string Index((Twine(ExploreCount)).str());
				if (ExploreCount) {
								CurrentSATFileName += ".";
								CurrentSATFileName += Index;
				}
				//set the solver type
				std::string SATCommand(SOLVER);
				SATCommand += SMALLSKIP;
				//add the input file
				SATCommand += CurrentSATFileName.data();
				SATCommand += SMALLSKIP;
				//add the output file 
				SATCommand += SATOutFile->data();
				//add the index of solution file
				SATCommand += ".";
				SATCommand += Index;
				//=====for debug purpose 
				cerr << "\ncalling SAT solver with the following command.....\n" << "\"" << SATCommand.data() << "\"" << "\n\n";
				//call the solver
				unsigned retcode = system(SATCommand.data());
				cerr << "\n SAT solver returned " << retcode << ".....\n";

				//check the solution file.....
				std::string OutFileName(SATOutFile->data());
				OutFileName += ".";
				OutFileName += Index;
				
				//return true if the SAT encoder database is UNSATISFIABLE.....
				//I hte this string manipulation stuff, but currently just 
				//lazy to hack the MINISAT codebase.....
				std::ifstream MinSolFd;
				std::string line;
				MinSolFd.open(OutFileName.c_str(), std::ios::in);
				std::getline(MinSolFd,line);
				MinSolFd.close();
				StringRef Sline(line);
				if (Sline.equals(UNSATISFIABLE))
					return true;
				
				return false;
}





