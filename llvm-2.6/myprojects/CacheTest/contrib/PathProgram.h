#ifndef __PATH_PROGRAM_H_
#define __PATH_PROGRAM_H_

#include "llvm/Pass.h"
#include "llvm/Module.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Constants.h"
#include "llvm/Instructions.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/FunctionUtils.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/Support/Streams.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/ExecutionEngine/Interpreter.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/Target/TargetSelect.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Assembly/PrintModulePass.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/raw_ostream.h"
#include <memory>
#include <algorithm>
#include <fstream>
#include "CodeTransform.h"

//main class for defining a path program
class PathProgram {
				private:
				char PathProgramID; //Path program identity

				//transformed code with cache performance 
				//instrumentation
				CodeTransform* CacheTransform;

				//set of basic blocks which belong to this 
				//path program
				std::vector<BasicBlock *> PathProgramBBList;

				//set of basic blocks which belong to the path 
				//of this path program
				std::vector<BasicBlock *> PathBBList;

				public:
				PathProgram() {}

				PathProgram(unsigned &ID, CodeTransform* CT) {
								PathProgramID = ID; 
								CacheTransform = CT;
				}
				
				//fetch the basic block list corresponding to the path program
				std::vector<BasicBlock *> getPathProgramBBList() {
								return PathProgramBBList;
				}

				//fetch the basic block list corresponding to the path of this 
				//path program
				std::vector<BasicBlock *>& getPathBBList() {
								return PathBBList;
				}

				//fetch the cache performance instrumented code
				CodeTransform* getCacheTransformedCode() {
								return CacheTransform;
				}

				//add a basic block to this path program
				void AddBBToPathProgramBBList(BasicBlock* BB) {
								PathProgramBBList.push_back(BB);
				}

				//construct a path program from a given path (basic block level trace)
				void ConstructPathProgramFromPath(std::vector<BasicBlock *> BBList);

				//find whether a specific basic block belongs to this path program
				bool findBBInPathProgram(BasicBlock* BB);

				//return the total number of memory blocks accessed in the path program
				int GetNumberOfMemoryBlocks();

};


#endif
