#include "Invariant.h"
#define TRUEBRANCH 1
#define FALSEBRANCH 0
//boom....too much hard-coding eh? ;-)
//file specifying path program or selective disabling of forks by KLEE
#define KLEE_SELECTIVE_FORK_FILE "bfork.log"
//file specifying the output of KLEE, maximum and minimum number of 
//cache misses
#define KLEE_CMISS_RECORD_FILE "pp-cache-miss.log"
#define KLEE_CPERF_RECORD_FILE "pp-cache-performance.log"
#define KLEE_DIR "klee-last/"
#define KLEE_CVC_FILE_PREFIX "test"
#define KLEE_TEST_SIZE 6
#define CVC_EXT ".cvc"
//#define KLEE_OTHER_OPTIONS " -write-cvcs -write-pcs -write-paths "
#define KLEE_OTHER_OPTIONS " -write-cvcs "
#define KLEE_EXE "klee"
#define SMALLSKIP " "
#define CACHE_MISS_THRESHHOLD 7

//given a path program, this function constructs the invariants at different 
//program points of the path program
void InvariantMap::ConstructInvariantOverPathProgram()
{
				PathProgram* CPP = getPathProgram();
				//fetch the set of basic blocks for the corresponding path program
				std::vector<BasicBlock *> PathProgramBBList = CPP->getPathProgramBBList();
				std::vector<BasicBlock*> BBList;
				std::vector<BasicBlock*>::iterator II;

				for (Module::iterator IM = M->begin(), EM = M->end(); IM != EM; IM++) {
								Function* F = dyn_cast<Function>(IM);
								BasicBlock* EntryBB = &(F->getEntryBlock());	
								//insert the entry block into analyzed BB list
								BBList.push_back(EntryBB);
								while(!BBList.empty()) {
												bool changed = 0;
												//fetch and erase the first basic block element from the worklist
												BasicBlock* BB = BBList.front();
												BBList.erase(BBList.begin());
												//perform an invariant update pass over the basic block
												GenerateBasicBlockInvariants(BB, changed);
												succ_iterator SI = succ_begin(BB), SE = succ_end(BB);
												for (; SI != SE; SI++) {
														//check whether the successor basic block belong to the corresponding
														//path program
														II = find(PathProgramBBList.begin(), PathProgramBBList.end(), *SI);			
														//we analyze the path program, therefore the successor is considered 
														//only if it belongs to the path program
														if (!(II == PathProgramBBList.end()) && changed) 
																BBList.push_back(*SI);
												}
								}
				}
}

//generate/update basic block level invariants
void InvariantMap::GenerateBasicBlockInvariants(BasicBlock* BB, bool &changed)
{
				for(BasicBlock::iterator II = BB->begin(), IE = BB->end(); II != IE; II++) {
								//Instruction* Inst = dyn_cast<Instruction>(II);
								//======sudiptac: TODO

				}
}

//analyze minimum and maximum number of cache misses along a particular path program
//-----this function handles some specific features related to KLEE symbolic 
//execution engine-----
void InvariantMap::AnalyzeCacheMissesOverPathProgram()
{
				//get the path program
				PathProgram* CPP = getPathProgram();
				
				//(1) create the fork disabling file for KLEE.....
				//the fork disabling file gives support to KLEE to reason over a path 
				//program, not just a path and not just the whole program
				CreatePathProgramBranches(CPP);

				//(2) Invoke KLEE with selective forks.....
				runSelectiveKLEE();

				//(3) read the file dumped by KLEE
				SetMaxMinCacheMisses();

				//(4)[OPTIONAL] debug cache performance
				DebugPathProgramCachePerformance();
}

//create the path program branch listing for KLEE for selective symbolic execution
void InvariantMap::CreatePathProgramBranches(PathProgram* CPP)
{
		//fetch the set of basic blocks in the respective path of this path program
		std::vector<BasicBlock*> &PathBBList = CPP->getPathBBList();
		std::vector<BasicBlock*>::size_type PathSize = PathBBList.size();

		//initialize the fork disable file produced for KLEE
		std::ofstream ForkFd;
		std::string KleeForkFile(KLEE_SELECTIVE_FORK_FILE);
		ForkFd.open(KleeForkFile.c_str(), std::ios::out);

		for (unsigned PathI = 0; PathI < PathSize; PathI++) {
				BasicBlock* BB = PathBBList[PathI];

				if (!CTModule->IsInstrumentedBlock(BB)) {
						TerminatorInst* TI = BB->getTerminator(); 
						if (TI->getOpcode() == Instruction::Br) {
								BranchInst* BI = dyn_cast<BranchInst>(TI);
								if (BI->isConditional()) {
										BasicBlock* TrueDest = BI->getSuccessor(0);											
										BasicBlock* FalseDest = BI->getSuccessor(1);
										//note that for the path program, we do not distinguish when the 
										//branch was actually taken, we just care whether it was ever 
										//taken
										
										//if the true destination in the path program, but not the false
										//one
										if (CPP->findBBInPathProgram(TrueDest) && !CPP->findBBInPathProgram(FalseDest)) {
												ForkFd << BB->getName().data() << "\n";
												ForkFd << TRUEBRANCH << "\n";
										}		
										//else if the false destination in the path program, but not the 
										//true one
										//else if (IIFalse != PathBBList.end() && IITrue == PathBBList.end()) {
										else if (!CPP->findBBInPathProgram(TrueDest) && CPP->findBBInPathProgram(FalseDest)) {
												ForkFd << BB->getName().data() << "\n";
												ForkFd << FALSEBRANCH << "\n";
										}
								}
						}
				}
		}
		ForkFd.close();
}

//run selectively few branches (according to the current path program) using KLEE.....
//KLEE will dump a record of cache misses.....
//*****CAUTION***** note that this function excepts two instumentations 
//(1) The original cache performance instrumentation for computing the cache misses, 
//and 
//(2) The KLEE specific annotations, i.e., to mark certain variables (inputs to the 
//program) as "symbolic"
//
//Otherwise, you are dead.....phew.....
void InvariantMap::runSelectiveKLEE()
{
		//get the Output bitcode filename 
		char* TransformedBitcodeFile = CTModule->GetOutputBitcodeFileName();
		std::string KleeCommand(KLEE_EXE);
		KleeCommand += SMALLSKIP;
		KleeCommand += KLEE_OTHER_OPTIONS;
		KleeCommand += SMALLSKIP;
		KleeCommand += TransformedBitcodeFile;

		unsigned retcode = system(KleeCommand.c_str());
		cerr << "\n KLEE has returned " << retcode << ".....\n";
}

//debug cache performance of the current path program
void InvariantMap::DebugPathProgramCachePerformance()
{
		int MemBlock;
		int Cmiss;
		std::string KleeCperfLog(KLEE_CPERF_RECORD_FILE);		
		std::ifstream CperfFd;
		//cache miss recorder
		std::map<unsigned,int> CmissLine;
		std::map<unsigned,std::vector<int> > MemArray;
		//a list of memory blocks which might be locked for a 
		//better cache performance
		std::map<unsigned, int> ProbableLockedMemBlocks;

		CperfFd.open(KleeCperfLog.c_str(), std::ios::in);
		if (!CperfFd.is_open()) {
				assert(0 && "you are a SATAN.....you did not record cache performance");
		}

		CperfFd >> MemBlock;
		CperfFd >> Cmiss;
		while (!CperfFd.eof()) {
				unsigned CacheSet = MemBlock % CTModule->getNumCacheSets();
				MemArray[CacheSet].push_back(MemBlock);
				if (!CmissLine.count(CacheSet)) {
						CmissLine[CacheSet] = Cmiss;		
				} else {
						CmissLine[CacheSet] = CmissLine[CacheSet] + Cmiss;		
				}
				
				//build a list of probably locked cache blocks (can be used as a feedback driven 
				//compiler optimization such as cache locking)
				if (Cmiss >= CACHE_MISS_THRESHHOLD && !ProbableLockedMemBlocks.count(CacheSet)) {
						ProbableLockedMemBlocks[CacheSet] = MemBlock;		
						CTModule->UpdateLockedMemoryBlocks(MemBlock);
				}
				
				CperfFd >> MemBlock;
				CperfFd >> Cmiss;
		}
		CperfFd.close();

#ifdef _DEBUG_CACHE_PERF
//#if 1
		//print the cache performance debugger's output
		cerr << "\n.....dumping cache performance debug information.....\n";
		for (unsigned SI = 0; SI < CTModule->getNumCacheSets(); SI++) {
				if (CmissLine.count(SI)) {
					cerr << "(Cache Set = " << SI << ", " << "Cmiss = " << CmissLine[SI] << ")\n";
					for (unsigned MEMI = 0; MEMI < MemArray[SI].size(); MEMI++) {
							cerr << " ( "<< MemArray[SI][MEMI] << " ) ";
					}
					cerr << "\n";
				}	
				else	
					cerr << "(Cache Set = " << SI << ", " << "Cmiss = " << 0 << ")\n";
		}
		cerr << "..................................................\n";
#endif
}

//fetch the maximum and minimum number of cache misses from the KLEE output file
void InvariantMap::SetMaxMinCacheMisses()
{
		long Cmiss;		
		long InstrCount;
		long ExeCycles;
		int ExploreCount = 0;
		std::string KleeCmissLog(KLEE_CMISS_RECORD_FILE);
		std::ifstream CmissFd;
		std::string line;
		std::vector<int> ValidList;
		
		//compute the cold cache misses here 
		//NOTE: cleekee: commented out as cold miss count included in instrumentation
		//PathProgram* CPP = getPathProgram();
		//int ColdCacheMiss = CPP->GetNumberOfMemoryBlocks();
		
		CmissFd.open(KleeCmissLog.c_str(), std::ios::in);
		while (!CmissFd.eof()) {
				CmissFd >> Cmiss;
				CmissFd >> InstrCount;
#ifdef _DEBUG_CACHE_PERF
				cerr << "I am getting a Cmiss " << Cmiss << "\n";
				cerr << "I am getting an Instruction count " << InstrCount << "\n";
#endif
				ExploreCount++;		
				//ignore the redundant state values
				if (Cmiss == -1) {
						assert(InstrCount == -1 && "Ouchhh.....invalid analysis state has error");
						cerr << "ignoring invalid analysis state.....don't worry, it's safe.....\n";
						continue;
				}
				//compute execution cycle (instruction + cache miss penalty)
				ExeCycles = InstrCount + CTModule->getMemLatency() * (Cmiss/* + ColdCacheMiss*/);
				if (Cmiss >= MaxCacheMiss)
						MaxCacheMiss = Cmiss;
				if (Cmiss <= MinCacheMiss || MinCacheMiss == -1)
						MinCacheMiss = Cmiss;		
				if (ExeCycles >= MaxExeCycles)
						MaxExeCycles = ExeCycles;
				if (ExeCycles <= MinExeCycles || MinExeCycles == -1)
						MinExeCycles = ExeCycles;		
				//update the valid analysis state list
				ValidList.push_back(ExploreCount);
		}

		//MinCacheMiss += ColdCacheMiss;
		//MaxCacheMiss += ColdCacheMiss;

		cerr << "\nOverall cache performance of the current path program  " << MinCacheMiss \
				<< " <= Cmiss <= " << MaxCacheMiss << " .....\n";
		CmissFd.close();
		
		//create the path program validity checker
		VC vc = vc_createValidityChecker();
		Expr OrCond = vc_falseExpr(vc);
	
		for (unsigned EI = 0; EI < ValidList.size(); EI++) {		
				//create validity checker to parse CVC files
				VC filevc = vc_createValidityChecker();
				int Idx = ValidList[EI];
				//what a hack!!!!!!
				if (Idx >= ExploreCount) continue;
				//build the bucket condition now
				std::string KleeCVCFileName(KLEE_DIR);
				KleeCVCFileName += KLEE_CVC_FILE_PREFIX;
				std::string IndexSuffix(Twine(Idx).str());
				std::string Index;
				
				//fill rest of the file name positions by dummy "0" character, what a 
				//mere waste of time ;-)
				for (unsigned II = 0; II < KLEE_TEST_SIZE - IndexSuffix.length(); II++)
						Index += "0";
				Index += IndexSuffix;		
				KleeCVCFileName += Index;
				KleeCVCFileName += CVC_EXT;
				cerr << "\n.....I am parsing a KLEE generated CVC file named "\
						 << KleeCVCFileName.c_str() << "\n";
				vc_parseExpr(filevc, KleeCVCFileName.c_str());
				cerr << "parsing successful\n";
				int NoAssertions = 0;
				Expr* AllConditions = vc_getAllAsserts(filevc, &NoAssertions);
				cerr << "\nprinting assertions.....\n";
				vc_printAsserts(filevc, 0);
				cerr << "\nfinish printing all assertions.....\n";
				Expr AndCond = vc_trueExpr(vc);
				for (int AI = 0; AI < NoAssertions; AI++) {
						Expr Prev = AndCond;		
						AndCond = vc_andExpr(vc, AndCond, AllConditions[AI]);		
						free(Prev);
				}
				//delete STP allocated memory
				delete AllConditions;
				Expr Prev = OrCond;
				OrCond = vc_orExpr(vc, OrCond, AndCond);
				free(Prev);
				vc_Destroy(filevc);
		}
		//FIXME: A potential memory leak
		//vc_Destroy(vc);
		
		//set the bucket cond
		BucketCond = OrCond;
}

//extract sufficient,interference edge pairs from the invariant map and the given path 
//program note that the following function might have different implementations with 
//varying degrees of sophistication
void InvariantMap::ExtractEdgesFromPathProgram()
{
				#if 0

				//get the path program
				PathProgram* CPP = getPathProgram();
				//fetch the set of basic blocks in the respective path of this path program
				std::vector<BasicBlock*> PathBBList = CPP->getPathBBList();
				std::vector<BasicBlock*>::size_type PathSize = PathBBList.size();
				
				//go through the list of basic blocks and check for conditional edges
				for (unsigned PathI = 0; PathI < PathSize; PathI++) {
								BasicBlock* BB = PathBBList[PathI];
								TerminatorInst* TI = BB->getTerminator();
								//check if this basic block has a conditional 
								//branch instruction at termination
								if (TI && TI->getNumSuccessors() > 1) {
												//fetch the destination block from the trace
												BasicBlock* DestBB = PathBBList[PathI + 1];
												//now we have got the <BB,DestBB> pair for the conditional 
												//edge. add the edge in the bloking set. The blocking set 
												//will be used to add blocking clauses to the SAT encoding
												std::pair<BasicBlock*, BasicBlock*> Edge(BB,DestBB);
												BlockEdges.push_back(Edge);
								}
				}

				#endif
			
				//.....<<<<<<Find the blocking and interfering control flow edges>>>>>>.....
				//Interfering edges are collected over the entire program. Interference edges 
				//are the set of control flow edges which did not appear in the current path 
				//program (**** scope for improvement in pruning path ****)
				//on the other hand, blocking clauses correspond to the set of conditional 
				//branch edges which have appeared in the current execution trace
				Function* F = M->getFunction("main");
				//sanity check
				assert(F && "you are a SATAN.....no \"main\" function found in test program");

				for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; ++IF) {
						BasicBlock* BB = dyn_cast<BasicBlock>(IF);

						//skip the instrumented basic blocks.....
						if (CTModule->IsInstrumentedBlock(BB))
								continue;
						
						//collect interference edges only for the conditional branch instructions
						TerminatorInst* TI = BB->getTerminator();
						BranchInst* BI = dyn_cast<BranchInst>(TI);
						
						if (BI && BI->isConditional()) {
								TraceType::iterator IIT;
								std::vector<std::pair<BasicBlock *, BasicBlock *> >::iterator II;
								std::pair<BasicBlock*, bool> TrueB(BB, true);
								std::pair<BasicBlock*, bool> FalseB(BB, false);

								//fetch the original basic block from the current module (skip the instrumented)
								BasicBlock* TrueDest = CTModule->GetOriginalBasicBlock(BI->getSuccessor(0));
								BasicBlock* FalseDest = CTModule->GetOriginalBasicBlock(BI->getSuccessor(1));

								assert(TrueDest && FalseDest && "Ouchhh.....error while extracting blocking edges.....");

								IIT = find(ExecTrace->begin(), ExecTrace->end(), TrueB);
								//not in the current execution trace?
								if (IIT == ExecTrace->end()) {
										EdgeType Edge(BB, TrueDest);
										InterferenceEdges.push_back(Edge);
								} else { 
										//block the conditional edge as it has already appeared in the trace
										EdgeType Edge(BB, TrueDest);
										II = find(SymBranchList.begin(), SymBranchList.end(), Edge);
										if (II != SymBranchList.end())
											BlockEdges.push_back(Edge);
										else 
											cerr << "Blocking clause not added, not a symbolic branch.....\n";
								}
								IIT = find(ExecTrace->begin(), ExecTrace->end(), FalseB);
								//not in the current execution trace?
								if (IIT == ExecTrace->end()) {
										EdgeType Edge(BB, FalseDest);
										InterferenceEdges.push_back(Edge);
								} else {
										//block the conditional edge as it has already appeared in the trace
										EdgeType Edge(BB, FalseDest);
										BlockEdges.push_back(Edge);
										II = find(SymBranchList.begin(), SymBranchList.end(), Edge);
										if (II != SymBranchList.end())
											BlockEdges.push_back(Edge);
										else 
											cerr << "Blocking clause not added, not a symbolic branch.....\n";
								}
						}
	
						#if 0					
						//if the basic block belongs to the path program then just skip it.....
						if (CPP->findBBInPathProgram(BB))
							continue;		
						
						//check whether it has any conflicting memory block
						//if (! CheckCacheConflictWithCurrentPathProgram(BB))
								//continue;

						//add the edge into the interference edge
						UpdateInterferenceEdges(BB);
						#endif
				}

}


#if 0

//find the control flow edges which may lead to the interfering basic block "BB"
void InvariantMap::UpdateInterferenceEdges(BasicBlock* BB)
{
				pred_iterator PI = pred_begin(BB), PE = pred_end(BB);				
				
				//no clause for the cache instrumented blocks
				if (CTModule->IsInstrumentedBlock(BB))
						return;		

				for (; PI != PE; ++PI) {
						BasicBlock* SrcBB = *PI;
						BasicBlock* OrigBB = CTModule->GetOriginalPredecessor(SrcBB);		
						std::pair<BasicBlock*, BasicBlock*> Edge(OrigBB, BB);
						InterferenceEdges.push_back(Edge);
				}
}

//check cache conflict of a basic block (outside the path program) with the 
//current path program
bool InvariantMap::CheckCacheConflictWithCurrentPathProgram(BasicBlock* BB)
{
				//accessed memory blocks in the basic block BB
				std::vector<unsigned> &MemInBB = CTModule->GetMemBlockFromBB(BB);

				PathProgram* CPP = getPathProgram();
				//fetch the set of basic blocks in the respective path of this path program
				std::vector<BasicBlock*> PathBBList = CPP->getPathBBList();
				std::vector<BasicBlock*>::size_type PathSize = PathBBList.size();

				for (unsigned SizeI = 0; SizeI < PathSize; SizeI++) {
						BasicBlock* PathBB = PathBBList[SizeI];
						//fetch the set of memory blocks accessed in PathBB
						std::vector<unsigned> &MemInPathBB = CTModule->GetMemBlockFromBB(PathBB);
						
						//no point if the conflict cannot reach the path program 
						if (!CheckReachability(PathBB, BB))
								continue;

						for (unsigned BBI = 0; BBI < MemInBB.size(); BBI++) {
								for (unsigned PathBBI = 0; PathBBI < MemInPathBB.size(); PathBBI++) {
										if (CTModule->getCacheSet(MemInPathBB[PathBBI]) == CTModule->getCacheSet(MemInBB[BBI]))
												return true;	
								}
						}
				}

				return false;
}

//check the reachability of a pair of basic blocks
bool InvariantMap::CheckReachability(BasicBlock* DestBB, BasicBlock* BB)
{
				LoopInfo* LI = SATFormula->getLoopInfoFromSAT();
				Loop* LBB = LI->getLoopFor(BB);
				Loop* LDestBB = LI->getLoopFor(DestBB);
				
				//this is a very crude simplistic implementation (hope you understand 
				//why ;-)).....can be or should be changed with varying degrees of 
				//sophistication
				if (LBB || LDestBB) return true;

				return false;
}
#endif


