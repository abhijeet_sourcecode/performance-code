#include "PathProgram.h"

//construct a path program from basic block level trace
//in our case, a path program additionally contains the following (with 
//respect to a given path):
//<1> Any back edge that might be present connecting the nodes in the path
//<2> Any basic block that is part of cache miss related instrumentation 
//between any two basic blocks present in the path
//In the PathProgram class, we also store the set of basic blocks in the 
//corresponding path (i.e. excluding the basic blocks of <1> and <2>)
void PathProgram::ConstructPathProgramFromPath(std::vector<BasicBlock*> BBList)
{
				std::vector<BasicBlock *>::size_type TraceSize = BBList.size();
				CodeTransform* CacheT;
				PathBBList.clear();

				for(unsigned PathI = 0; PathI < TraceSize; PathI++) {
								//any basic block in the path is definitely in the path 
								//program
								PathProgramBBList.push_back(BBList[PathI]);
								CacheT = getCacheTransformedCode();
								//keep a copy of the basic block in the corresponding path
								//=====this path must contain basic blocks that are present 
								//_only_ in the original module and _not_ inserted during 
								//code instrumentation phase=====
								if (!(CacheT->IsInstrumentedBlock(BBList[PathI])))
												PathBBList.push_back(BBList[PathI]);
								//get the related basic blocks to construct the path program.
								//we need to include all the instrumented basic blocks that 
								//might come across the path. note that this depends on the 
								//way we have transformed the code (which may well differ for 
								//checking different performance metrics)
								BasicBlock* RelatedBB = CacheT->getRelatedBlock(BBList[PathI]);

								//sanity check: instrumented basic block cannot be the last basic
								//block in the trace
								assert((!RelatedBB || PathI < TraceSize - 1) && 
												"wrong instrumented basic block obtained");
								
								//insert the related basic block in sequence
								if (RelatedBB)
												PathProgramBBList.push_back(RelatedBB);
				}

				//====end constructing a path program
}

//find whether a basic block belong to this path program
bool PathProgram::findBBInPathProgram(BasicBlock* BB)
{
				std::vector<BasicBlock *>::iterator II;
        II = find(PathProgramBBList.begin(), PathProgramBBList.end(), BB);
        
				return !(II == PathProgramBBList.end());
}

//find number of memory blocks accessed in the path program 
//used to compute the cold cache misses
int PathProgram::GetNumberOfMemoryBlocks() 
{
			int TotalBlocks = 0;
			std::map<unsigned,bool> Count;

			for (unsigned PathBBI = 0; PathBBI < PathBBList.size(); PathBBI++) {
					BasicBlock* BB = PathBBList[PathBBI];
					std::vector<unsigned>& MemBlocks = CacheTransform->GetMemBlockFromBB(BB);
					for (unsigned MEMI = 0; MEMI < MemBlocks.size(); MEMI++) {
							if (!Count.count(MemBlocks[MEMI])) {
									Count[MemBlocks[MEMI]] = true;
									TotalBlocks++;
							}
					}
			}

			return TotalBlocks;
}





