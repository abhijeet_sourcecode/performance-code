#ifndef __CODE_TRANSFORM_H_
#define __CODE_TRANSFORM_H_

#include "llvm/Pass.h"
#include "llvm/PassAnalysisSupport.h"
#include "llvm/Module.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Constants.h"
#include "llvm/Instructions.h"
#include "llvm/ModuleProvider.h"
#include "llvm/PassManager.h"
#include "llvm/PassManagers.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/FunctionUtils.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionAnalysis.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/Target/SubtargetFeature.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetRegistry.h"
#include "llvm/Target/TargetSelect.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/Support/Streams.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/ExecutionEngine/Interpreter.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/Target/TargetSelect.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Assembly/PrintModulePass.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/System/Host.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FormattedStream.h"
#include "llvm/Support/IRBuilder.h"
#include <memory>
#include <algorithm>
#include <fstream>

using namespace llvm;

//main class for cache performance instrumentation
class CodeTransform {
		
		private:		
    static char ID; // Pass identification

		//module on which transformation is carried out
		Module* M;

		//transformed LLVM bitcode filename
		char* OutBitcodeFileName;
		//original LLVM bitcode filename
		char* OrigOutBitcodeFileName;

		//cache related attributes
		unsigned NumCacheSets;
		unsigned Associativity;
		unsigned MemBlockSize;
		unsigned MemLatency;

		//specified machine architecture
		std::string TargetArch;

		//program start address, configurable through command line
		unsigned __start;

		//collects the map from basic block to accessed memory blocks
		DenseMap <const Value*, std::vector<unsigned> > MemBlockInBB;
		
		//collects the cache conflict map for a particular memory block
		DenseMap <unsigned, std::vector<unsigned> > CacheConflictMap;
		
		//global variables created for each memory block
		DenseMap<unsigned, GlobalVariable *> MemGlobal;		
		
		//global variables created for each memory block
		DenseMap<unsigned, GlobalVariable *> FlagMemGlobal;		
		
		//global variaables created for cache miss computation in 
		//different cache sets
		DenseMap<unsigned, GlobalVariable *> CacheMissGlobal;		
		
		//maintains a mapping between an instrumented conditional 
		//block and its corresponding instrumented computation 
		//block. according to our instrumentation technique, this
		//mapping is an one-to-one correspndence
		DenseMap<BasicBlock *, BasicBlock *> CondCompMap;		
		
		//keep a mapping of cache instrumented basic blocks with the 
		//original basic blocks of the module
		DenseMap<BasicBlock *, BasicBlock *> OriginalBBCondMap;		
		DenseMap<BasicBlock *, BasicBlock *> OriginalCoBBMap;		

		//cleekee: for LRU policy
		DenseMap<std::pair<unsigned,unsigned>, GlobalVariable*> LRUFlagGlobal;

		//keeps a record of all the instrumented basic blocks in the module
		std::vector<BasicBlock *> InstrumentedBBList;

		//keeps a record of all the memory blocks that are locked in cache
		std::vector<unsigned> LockedMemoryBlocks;

		//a global variable which counts all cache conflict misses
		GlobalVariable* GlobalCacheMiss;

		//a global variable which counts all cache cold misses
		GlobalVariable* GlobalCacheColdMiss;

		//a global varibale which counts the number of executed machine instructions
		GlobalVariable* MachineInstCount;

		//a return variable which returns the number of cache misses 
		//after interpretation through ExecutionEngine
		Value* CacheMissReturn;

		//true if cache locking is enabled
		bool CacheLockingEnabled;

		//file name providing the cache locked memory blocks
		std::string CacheLockFileName;

		//Target machine related data
		TargetMachine* TargetM;

		//basic block size in memory
		std::map<const BasicBlock *, unsigned> BBSizeInMemory;
		
		//basic block size in terms of number of machine instructions
		std::map<const BasicBlock *, unsigned> BBSizeInMBBInst;

		public:		
		enum CachePolicy {
				FIFO = 0,
				LRU
		} ReplacementPolicy;

		CodeTransform() {}
    
		CodeTransform(Module *Mod, char* OutFile, char* OrigOutFile, char* LockFileName) 
				: CacheLockingEnabled(false), TargetM(NULL) {
				MachineInstCount = NULL;
				DataRefCount = NULL;
				M = Mod; 
				OutBitcodeFileName = OutFile;
				OrigOutBitcodeFileName = OrigOutFile;
				if (LockFileName) {
					CacheLockFileName += LockFileName;
					CacheLockingEnabled = true;
				}	
		}

		char* GetOutputBitcodeFileName() { return OutBitcodeFileName;}
		
		char* GetOrigOutputBitcodeFileName() { return OrigOutBitcodeFileName;}

		std::vector<unsigned>& getLockedMemoryBlocks() { return LockedMemoryBlocks; }

		bool IsCacheLockingEnabled() { return CacheLockingEnabled; }

		unsigned getNumCacheSets();
		
		unsigned getCacheAssoc();

		unsigned getMemBlockSize();
		
		unsigned getMemLatency();

		std::string& getTargetArchitecture();

		unsigned getProgramStartAddress();	

		unsigned getMachineInstSize(MachineInstr* MI);
	
		unsigned getCacheSet(unsigned MemBlock);

		unsigned getLockedLines(unsigned MemBlock);

		bool IsMemBlockLockedInCache(unsigned MemBlock);

		void ReadConfigFromFile(std::ifstream &CacheIS);
		
		std::vector<unsigned>& GetMemBlockFromBB(BasicBlock* BB);

		void FindOrConstructCacheConflictMap(unsigned MemBB, Module &M);

		void ConstructGlobalCacheMissCounter(Module &M);

		void FindOrConstructGlobalVarForCacheMiss(unsigned SetNo, Module &M);
		
		void FindOrConstructGlobalVarForMem(unsigned MemBB, Module &M);
		
		void FindOrConstructFlagVarForMem(unsigned MemBB, Module &M);

		//cleekee: for LRU policy
		void FindOrConstructLRUFlagVarForMem(unsigned MemBB, Module &M);
		bool getLRUFlag(unsigned memBB1, unsigned memBB2);
		GlobalVariable* GetLRUFlag(unsigned memBB1, unsigned memBB2);

		void PrintMemoryBlockSequence(Module &M);

		void GenerateMemoryBlockSequence(Module &M);

		void GenerateMemoryBlockSequenceFromObj(Module &M);

		void UpdateCacheConflict(Module &M, BasicBlock &BB, Value &Conflict, unsigned MemBB, bool IsCold);
	
		void UpdateCacheConflictLRU(Module &M, BasicBlock &BB, Value &Conflict, unsigned MemBB, bool IsCold);
	
		void AnnotateBBWithCacheMiss(BasicBlock &BB, Module &M);

		//insert instrumented blocks between BB and successor SuccI of BB, with conflicting mem blocks LocalMem
		void InstrumentSuccessorOfBB(BasicBlock &BB, Module &M, unsigned SuccI, std::vector<unsigned> LocalMem);

		void AnnotateBBWithMachineInstrCount(BasicBlock& BB, Module &M);

		void TransformCodeForCacheTest(Module &M);

		BasicBlock* getRelatedBlock(BasicBlock* BB);

		bool IsInstrumentedBlock(BasicBlock* BB);

		BasicBlock* GetOriginalBasicBlock(BasicBlock* BB);
		
		BasicBlock* GetOriginalPredecessor(BasicBlock* BB);

		//schedule machine code generation pass for cache performance 
		//instrumentation 
		void ScheduleMachineCodeGenerationPass(Module& M);
		
		bool runCodeTransformOnModule(Module &M, std::ifstream& CacheIS);		

#if 0
		void TransformCodeForSymbolicExec(std::ifstream &ISYM);
#endif

		bool IsInjectedCode(Instruction* Inst);

		void UpdateLockedMemoryBlocks(unsigned MemBlock);
		
		void FetchLockedMemoryBlocks();

		unsigned getBBSizeInMachineInst(BasicBlock* BB) { return BBSizeInMBBInst[BB]; } 
	
		/** cleekee: everything below is added for data cache analysis **/

		//some existing functions modified for data cache analysis purpose
		bool dcache_runCodeTransformOnModule(Module &M, std::ifstream& CacheIS);
		void dcache_GenerateMemoryBlockSequence(Module &M);
		void dcache_TransformCodeForCacheTest(Module &M);

		//helper functions to map data to memory address
		int GetSizeOfType(const Type* type);
		int MapDataRef(Value* value, int curMemSize);
		int GetMemBlockFromDRef(Value* value, int memBlocks[2]);
		int GetMemBlockFromDRef(int memAddr, int size, int memBlocks[2]);

		//deals with static data reference of array element/struct member
		void AddAccessedMemBlock(User* gepo, std::vector<unsigned> *LocalMemBlock);

		//map array index bound to accessed mem block(in a loop)
		void MapArrayIndexBoundToMemBlock(std::vector<int> *arrayIndexBound, std::vector<int> *boundMemBlock, 
				int refSize, int stride, int firstRefPos, int lastRefPos);

		//map data reference to memory address
		DenseMap<const Value*, int> DRefMemMap;

		//Split BB just before an array access for conditional instrumentation
		void SplitBB(Module &M);		
		
		DenseMap<const BasicBlock*, bool> splittedBlocks;
		
		// Conditional instrumentation for splitted BB due to instruction containing array access.
		// (NOTE: There is tentative support for struct access, not guaranteed to work perfectly at the moment.)
		// Returns NULL if BB is not splitted, otherwise returns last inserted instrumented BB.
		BasicBlock* InstrumentArrayAccess(BasicBlock &BB, Module &M);

		//track data reference count
		GlobalVariable* DataRefCount;
		std::vector<BasicBlock *> DataRefBB;
		void AnnotateBBWithDataRefCount(Module& M);
};
#endif

