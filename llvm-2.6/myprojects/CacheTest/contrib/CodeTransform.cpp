//======CodeTransform.cpp -> Instrument a program with performance metric=======//
//======current instrumentation - Cache performance======//

//performance test related header files
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/PassManager.h"
#include "llvm/ModuleProvider.h"
#include "CodeTransform.h"

#define INTERNAL_FUNCTION_CALL "klee_make_symbolic"
//too cachyy ;-)
unsigned CodeTransform::getNumCacheSets() {
				return NumCacheSets;
}
		
unsigned CodeTransform::getCacheAssoc() {
				return Associativity;
}

unsigned CodeTransform::getMemBlockSize() {
				return MemBlockSize;
}

unsigned CodeTransform::getMemLatency() {
				return MemLatency;
}
		
unsigned CodeTransform::getCacheSet(unsigned MemBlock) {
				return MemBlock % getNumCacheSets();
}

//return the target machine architecture
std::string& CodeTransform::getTargetArchitecture() {
				return TargetArch;
}

//return the program start address, configurable through command line
unsigned CodeTransform::getProgramStartAddress() {
				return __start;
}

//return the machine instruction width (in bytes)
unsigned CodeTransform::getMachineInstSize(MachineInstr* MI) {
				return TargetM->getInstrInfo()->GetInstSizeInBytes(MI);
}

void CodeTransform::ReadConfigFromFile(std::ifstream &CacheIS)
{
			CacheIS >> NumCacheSets;
			cerr << "\nNumber of cache sets read = " << NumCacheSets << "\n";
			CacheIS >> Associativity;
			cerr << "\nAssociativity of cache  = " << Associativity << "\n";
			CacheIS >> MemBlockSize;
			cerr << "\nMemory block size or cache line size (multiple of instructions) = " \
				<< MemBlockSize << "\n";
			CacheIS >> MemLatency;
			cerr << "\nMemory latency = " << MemLatency << "\n";
			CacheIS >> TargetArch;
			cerr << "\nMachine architecture = " << TargetArch << "\n";
			CacheIS >> std::hex;
			CacheIS >> __start;
			cerr << "\nProgram loaded at " << __start << "\n";

			std::string ReplacementPolicyString;
			CacheIS >> ReplacementPolicyString;
			cerr << "\nCache replacement policy = " << ReplacementPolicyString << "\n";
			ReplacementPolicy = (ReplacementPolicyString == "lru")? LRU : FIFO;			
}

bool CodeTransform::runCodeTransformOnModule(Module &M, std::ifstream &CacheIS) {

			//read cache configuration	
			ReadConfigFromFile(CacheIS);

			//read memory block sequences from object file
			GenerateMemoryBlockSequenceFromObj(M);
				
			//debug purpose
			PrintMemoryBlockSequence(M);

			//fetch the set of memory blocks which are locked in cache [OPTIONAL]
			FetchLockedMemoryBlocks();
			
			//generate global cache miss counter
			ConstructGlobalCacheMissCounter(M);
			
			//transform the code for cache performance testing
			TransformCodeForCacheTest(M);

		  return false;
}

//schedule machine code generation pass. Code generation is required 
//to map the machine code related information to LLVM bitcode level 
//basic blocks. Such an information is used for cache performance 
//instrumentation
void CodeTransform::ScheduleMachineCodeGenerationPass(Module &M)
{
			std::string MArch = getTargetArchitecture();
			const Target *TheTarget = NULL;

			//first initialize and register all the target machine architectures
			InitializeAllTargets();
			
			cerr << "Target registry search starts.....\n";
			if (!MArch.empty()) {
    		for (TargetRegistry::iterator it = TargetRegistry::begin(), 
					ie = TargetRegistry::end(); it != ie; ++it) {
					cerr << "Target = " << it->getName() << "\n";
      		if (MArch == it->getName()) {
        		TheTarget = &*it;
						cerr << "Found the target registration for " << MArch << " architecture\n";
        		break;
      		}   
    		}  
			}
			cerr << "Target registry search ends.....\n";

			//sanity check.....
			assert(TheTarget && "Ouchhh.....unable to find the registration for the target machine");
			
			Triple TheTriple(M.getTargetTriple());
  		
			if (TheTriple.getTriple().empty())
    		TheTriple.setTriple(llvm::sys::getHostTriple());

			Triple::ArchType Type = Triple::getArchTypeForLLVMName(MArch);
    	if (Type != Triple::UnknownArch)
      	TheTriple.setArch(Type);

			std::string Feature;
			//keep the feature string empty, as it is optional
			std::auto_ptr<TargetMachine> 
					target(TheTarget->createTargetMachine(TheTriple.getTriple(), Feature));
  		assert(target.get() && "Could not allocate target machine!");
  		//TargetMachine &Target = *target.get();
  		TargetM = target.get();

			// Build up all of the passes that we want to do to the module.
    	ExistingModuleProvider* Provider = new ExistingModuleProvider(&M);
    	FunctionPassManager Passes(Provider);

    	// Add the target data from the target machine, if it exists, or the module.
    	if (const TargetData *TD = TargetM->getTargetData())
      	Passes.add(new TargetData(*TD));
    	else
      	Passes.add(new TargetData(&M));

			//*****IMP***** schedule code generation pass
			//Target.addPassesToEmitFile(Passes, *Out, TargetMachine::AssemblyFile, CodeGenOpt::None);
			TargetM->addCommonCodeGenPasses(Passes, CodeGenOpt::None);
	
			Passes.doInitialization();

			//run our queue of passes all at once now, effciently
    	for (Module::iterator I = M.begin(), E = M.end(); I != E; ++I) {
      	if (!I->isDeclaration()) {
					#if 0
					//sudiptac: what are these required for?
        	if (DisableRedZone)
          	I->addFnAttr(Attribute::NoRedZone);
        	if (NoImplicitFloats)
          	I->addFnAttr(Attribute::NoImplicitFloat);
					#endif
        	Passes.run(*I);
      	}
			}
    	
			Passes.doFinalization();

			//now debug the relationship between the generated machine code and the LLVM bitcode
			//moreover, save the basic block code size into a local class data structure so that 
			//the codesize is accessible even after the target machine class is destroyed
			cerr << "\nStart fetching machine code information.....\n";
    	for (Module::iterator I = M.begin(), E = M.end(); I != E; ++I) {
				Function* F = dyn_cast<Function>(I);
				for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; ++IF) {
						BasicBlock* BB = dyn_cast<BasicBlock>(IF);
						unsigned MBBSize = 0;
						unsigned MBBInstSize = 0;
						std::vector<MachineBasicBlock *> MBBV = (TargetM->getMachineCodeInfo())[BB];
						cerr << "<< " << BB->getName().data() << " >> continue.....\n";
						for (unsigned MBBVSize = 0; MBBVSize < MBBV.size(); MBBVSize++) {
							cerr << "Machine basic block size = " << MBBV[MBBVSize]->size() << "\n";
							MBBV[MBBVSize]->dump();
							//compute the size of basic block in memory
							for (MachineBasicBlock::iterator MII = MBBV[MBBVSize]->begin(), 
											MIE = MBBV[MBBVSize]->end(); MII != MIE; ++MII)
										MBBSize += getMachineInstSize(MII);		
							//size of basic block in terms of number of machine instructions
							MBBInstSize += MBBV[MBBVSize]->size();
						}
						//save the code size of the basic block 
						cerr << "Basic block size in memory = " << MBBSize << "\n\n";	
						BBSizeInMemory[BB] = MBBSize;
						BBSizeInMBBInst[BB] = MBBInstSize;
				}
			}
			cerr << "End fetching machine code information.....\n";
}

//generate memory block sequence from target machine code
void CodeTransform::GenerateMemoryBlockSequenceFromObj(Module &M)
{
			std::vector<Instruction *> EraseThis;
			Instruction* InsertBeforeThis = NULL;
			bool InjectedCodeFound = false;

			//go through the basic blocks and collect all the injected instructions machine 
			//code should not be genrated for injected instruction and therefore, they will 
			//not taken into account for the performance analysis of the program
			for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
					Function* F = dyn_cast<Function>(IM);
					for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
							BasicBlock* BB = dyn_cast<BasicBlock>(IF);
							for (BasicBlock::iterator IB = BB->begin(), EB = BB->end(); IB != EB; IB++) {
									Instruction* Inst = dyn_cast<Instruction>(IB);
									//collect the set of instructions which need to be erased from the 
									//basic block before code generation, as some instructions are injected 
									//due to the analysis purpose
									if (IsInjectedCode(Inst)) {
										InjectedCodeFound = true;
										EraseThis.push_back(Inst);
									}	else if (InjectedCodeFound) {
										//****IMP**** I currently assume that injected code are in a chunk 
										//(like the calls to klee_make_symbolic) and they are put together 
										//in exactly one place. this assumption holds for the current version 
										//of the work, but the following code need to be modified if the 
										//respective assumption is violated in future
										InsertBeforeThis = Inst;
										InjectedCodeFound = false;
									}
							}
					}					
			}

			//unlink the injected instructions from the parent basic block
			for (std::vector<Instruction *>::iterator I = EraseThis.begin(), E = EraseThis.end(); 
					I != E; ++I) {
					Instruction* Inst = *I;
					//unlink, but do not erase
					Inst->removeFromParent();
			}				

			//generate machine code through the pass manager
			ScheduleMachineCodeGenerationPass(M);
	
			//well, the machine code generation is done. Now need to put the injected 
			//code back in the module for the purpose of analysis 			

			for (std::vector<Instruction *>::iterator I = EraseThis.begin(), E = EraseThis.end(); 
					I != E; ++I) {
					Instruction* Inst = *I;
					Inst->insertBefore(InsertBeforeThis);
			}
				
			//generate memory block sequences for each basic block 
			GenerateMemoryBlockSequence(M);
}

//this function is for debug purpose
//print the memory block sequences collected by "GenerateMemoryBlockSequence"
void CodeTransform::PrintMemoryBlockSequence(Module &M)
{
			cerr << "\n\n************ <Printing memory blocks of pogram> ************\n";
			for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
					 Function *F = dyn_cast<Function>(IM);

					 for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								//get the local memory block map
								std::vector<unsigned> LocalMemBlock = MemBlockInBB[BB];
								//get the size of basic block (in terms of number of memory blocks)
								std::vector<unsigned>::size_type BBSize = LocalMemBlock.size();

								//iterate through all the memory blocks collected from BB a priori and 
								//print all of them out
								cerr << "=========begin a basic block (" << BB->getName().data() << ") [" << "size = " << BBSize << "]==========\n";
								for (unsigned BBSI = 0; BBSI < BBSize; BBSI++)
												cerr << " " << LocalMemBlock[BBSI];
								cerr << "\n=========end of basic block===========\n";
					 }
			}	
			cerr << "************ <End Printing> ************\n";
}

//sudiptac: this function collects the memory block sequences in a program. 
//output is a Map structure "MemBlockInBB", which is indexed by each basic 
//block in the program and the value is a sequence (vector) of different 
//memory blocks accessed in the particular basic block....
void CodeTransform::GenerateMemoryBlockSequence(Module &M)
{
			for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
					 //sudiptac: should be reset at the module level. but currently OK
					 //program start address is configured through the command line	
					 unsigned StartAddress = getProgramStartAddress(); 
					 unsigned CurrMachineInstAddr = StartAddress;
					 Function *F = dyn_cast<Function>(IM);

					 for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								//use this vector to collect all the memory blocks accessed in basic 
								//block BB
								std::vector<unsigned> LocalMemBlock;
								std::vector<unsigned>::iterator II;
												
								unsigned __startMachineInstAddr = CurrMachineInstAddr;
								unsigned __endMachineInstAddr = __startMachineInstAddr + BBSizeInMemory[BB] - 1;
								unsigned __startMemBlockAddr = __startMachineInstAddr / getMemBlockSize();
								unsigned __endMemBlockAddr = __endMachineInstAddr / getMemBlockSize();
												
								//map the machine memory blocks to basic blocks
								for (unsigned MemI = __startMemBlockAddr, MemE = __endMemBlockAddr + 1; 
												MemI != MemE; ++MemI) { 
										//check if the memory block already exists
										II = find(LocalMemBlock.begin(), LocalMemBlock.end(), MemI);
										//insert only unique memory blocks
										if (II == LocalMemBlock.end())
												LocalMemBlock.push_back(MemI);
								}	
								//for going to the next machine instruction
								CurrMachineInstAddr = __endMachineInstAddr + 1;
								
								#if 0
								//collect the memory block sequence from a single basic block
								for (BasicBlock::iterator IB = BB->begin(), EB = BB->end(); IB != EB; IB++) {
											//push unique memory blocks 
											if (LocalMemBlock.empty() || LocalMemBlock[BBSize - 1] != LastBlockVisited)
												LocalMemBlock.push_back(LastBlockVisited);	
											//increment instruction counter	
											InstructionCounter++;
											//if the upcoming instruction falls in the boundary of 
											//a new memory block, increment memory block counter
											if (InstructionCounter % getMemBlockSize() == 0) {
												LastBlockVisited++;
											}
											//recompute code size
											BBSize = LocalMemBlock.size();
								}
								#endif

								//insert the local memory block vector into the global memory block map
								MemBlockInBB[BB] = LocalMemBlock;
					 }
			}	
}

//check whether a particular instruction is part of injected code, in that case we skip 
//those instructions for cache performance instrumentation
bool CodeTransform::IsInjectedCode(Instruction* Inst)
{
			Value* VInst = dyn_cast<Value>(Inst);
			StringRef Compare(INTERNAL_FUNCTION_CALL);

			//if it is an internal function call (instrumented code) then 
			//we do not need to measure its performance
			if (Inst->getOpcode() == Instruction::Call) {
				if (Inst->getOperand(0)->getName().equals(Compare)) {
						cerr << "found internal call....ignoring it.....\n";	
						return true;
				}		
			}	
			//additionally, any defined value used only by the injected function 
			//calls are ignored
			for (Value::use_iterator UII = VInst->use_begin(), UIE = VInst->use_end(); 
				UII != UIE; ++UII) {		
					Instruction* UserInst = dyn_cast<Instruction>(*UII);
					if (UserInst->getOpcode() == Instruction::Call) {
						if (UserInst->getOperand(0)->getName().equals(Compare)) {
							cerr << "found definition used for internal calls....ignoring it.....\n";	
							return true;
						}
				}	
		}

		return false;
}

//transform code for cache performance testing --- entry level call
void CodeTransform::TransformCodeForCacheTest(Module &M)
{
			//Module* M = CloneModule(&Mx);

		  //iterate over all the functions in the module M
		  for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
					 Function* F = dyn_cast<Function>(IM);
					 std::string fname = F->getName();
					 EscapeString(fname);
					 cerr << "\n\n===========In Function (" << fname << ").....===========\n";
					 std::vector<BasicBlock *> OriginalBBList;
					 unsigned CodeSize = 0;
	
					 //iterate over all the basic blocks inside the function F 
					 for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								//dynamic cast and get the typed basic block pointer
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								OriginalBBList.push_back(BB);
								CodeSize += BBSizeInMemory[BB];
					 }

					 //number of basic blocks
					 cerr << "Number of basic blocks in the subject program = " << OriginalBBList.size() << ".....\n";
					 cerr << "Object code size = " << CodeSize << " bytes\n";

					 //begin code instrumentation			
					 for (unsigned BBI = 0; BBI < OriginalBBList.size(); BBI++) {
								//split the successor edges of basic block BB to annotate with cache 
								//information
								AnnotateBBWithCacheMiss(*(OriginalBBList[BBI]), M);
								//instrument each basic block at the end by incrementing the number of 
								//executed machine instructions
								AnnotateBBWithMachineInstrCount(*(OriginalBBList[BBI]), M);
					 }  
		  }
}

//find or construct a cache conflict map indexed by different memory blocks in the program
void CodeTransform::FindOrConstructCacheConflictMap(unsigned MemBB, Module &M) 
{
		std::vector<unsigned>::iterator II;

		//sudiptac: lazy create the cache conflict map, only when needed
		if (CacheConflictMap[MemBB].empty()) {
				for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
						Function *F = dyn_cast<Function>(IM);
						for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
								BasicBlock* BB = dyn_cast<BasicBlock>(IF);
								//get the local memory block map
								std::vector<unsigned> LocalMemBlock = MemBlockInBB[BB];
								//get the size of basic block (in terms of number of memory blocks)
								std::vector<unsigned>::size_type BBSize = LocalMemBlock.size();
								
								//go through the memory blocks and create the cache conflict 
								//mapping for memory block "MemBB"
								for (unsigned BBSI = 0; BBSI < BBSize; BBSI++) {
										if(getCacheSet(LocalMemBlock[BBSI]) == getCacheSet(MemBB) && 
												LocalMemBlock[BBSI] != MemBB) {
																//check if the conflict block already exists in the mapping
																II = find(CacheConflictMap[MemBB].begin(), CacheConflictMap[MemBB].end(), \
																				LocalMemBlock[BBSI]);
																//insert only unique cache conflict blocks				
																if (II == CacheConflictMap[MemBB].end())				
																				CacheConflictMap[MemBB].push_back(LocalMemBlock[BBSI]);
										}		
								}
						}
				}
		}	
}

//create a global variable for counting all cache misses (in all cache sets)
void CodeTransform::ConstructGlobalCacheMissCounter(Module &M)
{
			//create the global variable counting cache conflict miss
			GlobalCacheMiss = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
				false, GlobalValue::ExternalLinkage, 0, "__cache_miss_global", 0, false);
			//cleekee: create the global variable counting cache cold miss
			GlobalCacheColdMiss = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
				false, GlobalValue::ExternalLinkage, 0, "__cache_cold_miss_global", 0, false);
				
			 //sudiptac: **IMP** initialize each newly declared global variables 
			Constant* CacheMissInit = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), \
				0, true);
			GlobalCacheMiss->setInitializer(CacheMissInit);
			Constant* CacheColdMissInit = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), \
				0, true);
			GlobalCacheColdMiss->setInitializer(CacheColdMissInit);

			//now change the function return value to return the number of cache misses
			//----
			//This is how it works....
			//(1) find the basic block with the return instruction
			//(2) remove the previous return instruction
			//(3) create a new return instruction to send the value of global cache miss
			//
			//in the end, when the execution engine will work, we shall be able to get 
			//the value of cache miss, each time we run the function "main"
			Function *F = M.getFunction("main");

			//sanity check
			assert(F && "no \"main\" function found in test program");

			for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
				BasicBlock* BB = dyn_cast<BasicBlock>(IF);
				TerminatorInst* TI = BB->getTerminator();
				if (TI && TI->getOpcode() == Instruction::Ret) {
								BasicBlock::iterator IB = BB->end();
								IB--;

								Instruction* I = dyn_cast<Instruction>(IB);
								//load the value of global cache miss
								//cleekee: add up cache conflict and cold miss
								Value *ConflictMissVal = new LoadInst(GlobalCacheMiss, "__cache_conflict_miss_val", I);
								Value *ColdMissVal = new LoadInst(GlobalCacheColdMiss, "__cache_cold_miss_val", I);
								Value *GlobalCacheMissVal = BinaryOperator::CreateAdd(ConflictMissVal, ColdMissVal, "__global_cache_miss_val", I);
								I->eraseFromParent();
								ReturnInst::Create(TI->getContext(), GlobalCacheMissVal, BB);
				}
			}
}

//find or construct a global for the corresponding memory block (MemBB)
void CodeTransform::FindOrConstructGlobalVarForCacheMiss(unsigned CacheSet, Module &M) 
{
				if (! CacheMissGlobal.count(CacheSet)) {
								//create the global variable counting cache miss for cache set "CacheSet"
								GlobalVariable *CacheMiss = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
												false, GlobalValue::ExternalLinkage, 0, \
												"__cache_miss" + Twine("_") + Twine(CacheSet), 0, false);
				
								//sudiptac: **IMP** initialize each newly declared global variables 
								Constant* CacheMissInit = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), \
												0, true);
								CacheMiss->setInitializer(CacheMissInit);
												
								//store the cache miss global in a map, so that no duplicate is 
								//generated in future
								CacheMissGlobal[CacheSet] = CacheMiss;
				}
}

//find or construct a flag global for the corresponding memory block (MemBB)
//for tracking the first time misses
void CodeTransform::FindOrConstructFlagVarForMem(unsigned MemBB, Module &M) 
{
				if (! FlagMemGlobal.count(MemBB)) {
								//create the global variable conflict
								GlobalVariable *Conflict = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
												false, GlobalValue::ExternalLinkage, 0, \
												"__flag_cache_test" + Twine("_") + Twine(MemBB), 0, false);
				
								//sudiptac: **IMP** initialize each newly declared global variables 
								Constant* ConflictInit = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), \
												0, true);
								Conflict->setInitializer(ConflictInit);
												
								//store the conflict global in map, so that no duplicate is 
								//generated in future
								FlagMemGlobal[MemBB] = Conflict;
				}
}

//find or construct a global for the corresponding memory block (MemBB)
void CodeTransform::FindOrConstructGlobalVarForMem(unsigned MemBB, Module &M) 
{
				if (! MemGlobal.count(MemBB)) {
								//create the global variable conflict
								GlobalVariable *Conflict = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
												false, GlobalValue::ExternalLinkage, 0, \
												"__conflict_cache_test" + Twine("_") + Twine(MemBB), 0, false);
				
								//sudiptac: **IMP** initialize each newly declared global variables 
								Constant* ConflictInit = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), \
												0, true);
								Conflict->setInitializer(ConflictInit);
												
								//store the conflict global in map, so that no duplicate is 
								//generated in future
								MemGlobal[MemBB] = Conflict;
				}
}

void CodeTransform::FindOrConstructLRUFlagVarForMem(unsigned MemBB, Module &M) 
{
	//get the size of conflict
	std::vector<unsigned>::size_type ConflictSize = CacheConflictMap[MemBB].size();
	std::vector<unsigned> CacheConflict = CacheConflictMap[MemBB];

	//go through the cache conflict list and increment respective cache conflict count
	for (unsigned CCI = 0; CCI < ConflictSize; CCI++) {
		//fetch the conflicting memory block id
		unsigned ConflictMem = CacheConflict[CCI];

		if (!getLRUFlag(MemBB, ConflictMem)) {
			//create global var for LRU flag for memBB1-memBB2 pair
			GlobalVariable *LRUFlag;
			if (MemBB < ConflictMem)
				LRUFlag = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
												false, GlobalValue::ExternalLinkage, 0, \
												"__lruflag" + Twine("_") + Twine(MemBB) + Twine("_") + Twine(ConflictMem), \
												0, false);
			else
				LRUFlag = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
								false, GlobalValue::ExternalLinkage, 0, \
								"__lruflag" + Twine("_") + Twine(ConflictMem) + Twine("_") + Twine(MemBB), \
								0, false);

			//sudiptac: **IMP** initialize each newly declared global variables 
			Constant* LRUFlagInit = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0, true);
			LRUFlag->setInitializer(LRUFlagInit);
			
			//order the pair so that we don't have to create duplicate pair
			if (MemBB < ConflictMem)
				LRUFlagGlobal.insert(std::make_pair(std::make_pair(MemBB, ConflictMem), LRUFlag));
			else
				LRUFlagGlobal.insert(std::make_pair(std::make_pair(ConflictMem, MemBB), LRUFlag));
		}
	}
}

//cleekee: check if LRU flag for memBB1-memBB2 pair is already created
bool CodeTransform::getLRUFlag(unsigned memBB1, unsigned memBB2)
{
	//order the pair so that we don't have to create duplicate pair
	if (memBB1 < memBB2)
		return LRUFlagGlobal.count(std::make_pair(memBB1, memBB2));
	else
		return LRUFlagGlobal.count(std::make_pair(memBB2, memBB1));
}

//cleekee: check if LRU flag for memBB1-memBB2 pair is already created
GlobalVariable* CodeTransform::GetLRUFlag(unsigned memBB1, unsigned memBB2)
{
	//order the pair so that we don't have to create duplicate pair
	if (memBB1 < memBB2) {
		if (LRUFlagGlobal.count(std::make_pair(memBB1, memBB2)))
			return LRUFlagGlobal[std::make_pair(memBB1, memBB2)];
	} else {
		if (LRUFlagGlobal.count(std::make_pair(memBB2, memBB1)))
			return LRUFlagGlobal[std::make_pair(memBB2, memBB1)];
	}

	return NULL;
}

//instrument each basic block at the end by incrementing the total number of 
//executed machine instructions 
void CodeTransform::AnnotateBBWithMachineInstrCount(BasicBlock& BB, Module& M)
{
			//create the global variable counting the number of executed machine instructions
			if (!MachineInstCount) {
				MachineInstCount = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
					false, GlobalValue::ExternalLinkage, 0, "__machine_inst_global", 0, false);
				
				//sudiptac: **IMP** initialize each newly declared global variables 
				Constant* Zero = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0, true);
				MachineInstCount->setInitializer(Zero);
			}

			//load the current value of the executed machine instructions, just before the 
			//termination/branching of the respective basic block
			TerminatorInst* TI = BB.getTerminator();
			Value *MachineInstVal = new LoadInst(MachineInstCount, "__machine_inst_global_val", TI);
			//add the executed instructions in this basic block
			Value* ExeInst = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), \
						getBBSizeInMachineInst(&BB));
			Value *ExeAdd = BinaryOperator::CreateAdd(MachineInstVal, ExeInst, \
						"__machine_inst_incr_val", TI);
			//finally update the global variable which counts the number of executed 
			//machine instructions
			new StoreInst(ExeAdd, MachineInstCount, TI);
}

//annotate basic block (BB) successors with cache performance
void CodeTransform::AnnotateBBWithCacheMiss(BasicBlock &BB, Module &M)
{
			//get the terminator instruction of BB	
			TerminatorInst *TI = BB.getTerminator();

			for (unsigned SuccI = 0, SuccE = TI->getNumSuccessors(); SuccI != SuccE; SuccI++)	{
						//get the size of the original successor block (in terms of 
						//code memory blocks)
						BasicBlock* DestBB = TI->getSuccessor(SuccI);
						//std::vector<unsigned>::size_type BBSize = MemBlockInBB[DestBB].size();
						std::vector<unsigned> LocalMem = MemBlockInBB[DestBB];

						InstrumentSuccessorOfBB(BB, M, SuccI, LocalMem);
			}
}

void CodeTransform::InstrumentSuccessorOfBB(BasicBlock &BB, Module &M, unsigned SuccI, std::vector<unsigned> LocalMem)
{
			TerminatorInst *TI = BB.getTerminator();
			BasicBlock* DestBB = TI->getSuccessor(SuccI);

			//get the parent function of basic block "BB"
			Function *F = BB.getParent();
			Function::iterator IF = &BB;

			//get pointer to cache associativity constant
			Value* Assoc = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), getCacheAssoc());

			Value* Zero = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0);
			Value* One = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 1);

			//for each accessed memory block in DestBB, we shall need one conditional 
			//block and one computational block for instrumenting cache performance. 
			std::vector<BasicBlock*> NewBBCond;
			std::vector<BasicBlock*> NewBBComp;
			//for tracking first time conflicts
			std::vector<BasicBlock*> NewBBColdCond;
			std::vector<BasicBlock*> NewBBColdComp;
			
			//cleekee: for LRU policy
			//std::vector<BasicBlock**> BBLRUCond;
			//std::vector<BasicBlock**> BBLRUComp;
			BasicBlock* BBLRUCond[64][128];
			BasicBlock* BBLRUComp[64][128];
			std::vector<BasicBlock*> BBSetLRU;
			
			//use to track only the unlocked memory block size
			unsigned EffectiveSize = 0;
			std::map<unsigned,unsigned> EffectMemBlock;

			//create some basic blocks for cache performance instrumentation
			std::vector<unsigned>::size_type BBSize = LocalMem.size();
			for (unsigned BBSI = 0; BBSI < BBSize; BBSI++) {
							unsigned MemBB = LocalMem[BBSI];

							//get count of mem blocks conflicting with MemBB
							FindOrConstructCacheConflictMap(MemBB, M);
							std::vector<unsigned>::size_type ConflictSize = CacheConflictMap[MemBB].size();
							
							//handle cache locking. if the memory block has been locked in the cache, 
							//then do not create any instrumented basic block corresponding to it
							if (IsMemBlockLockedInCache(MemBB))
											continue;
							
							//create two empty basic blocks, first one is conditional 
							//and the second one computes cache misses for the original 
							//successor basic block of BB
							BasicBlock* BBCond = BasicBlock::Create(TI->getContext(), \
											BB.getName() + "." + DestBB->getName() + "_cache_test_cond" + "." + Twine(BBSI));				
							BasicBlock* BBComp = BasicBlock::Create(TI->getContext(), \
											BB.getName() + "." + DestBB->getName() + "_cache_test_comp" + "." + Twine(BBSI));
							//for tracking the first time conflicts				
							BasicBlock* BBColdCond = BasicBlock::Create(TI->getContext(), \
											BB.getName() + "." + DestBB->getName() + "_cache_test_cold_cond" + "." + Twine(BBSI));				
							BasicBlock* BBColdComp = BasicBlock::Create(TI->getContext(), \
											BB.getName() + "." + DestBB->getName() + "_cache_test_cold_comp" + "." + Twine(BBSI));

							NewBBCond.push_back(BBCond);
							NewBBComp.push_back(BBComp);
							NewBBColdCond.push_back(BBColdCond);
							NewBBColdComp.push_back(BBColdComp);

							//keep a record of all the instrumented basic blocks
							InstrumentedBBList.push_back(BBCond);
							InstrumentedBBList.push_back(BBComp);
							InstrumentedBBList.push_back(BBColdCond);
							InstrumentedBBList.push_back(BBColdComp);
							
							//we create this map to ensure we later find out which computation block is related to 
							//a specific conditional block. note that both the blocks are instrumented and therefore, 
							//such a nice mapping is possible
							CondCompMap[BBCond] = BBComp;
							CondCompMap[BBColdCond] = BBColdComp;

							//cleekee: for LRU policy
							BasicBlock* BBILRUCond[128];
							BasicBlock* BBILRUComp[128];
							if (ReplacementPolicy == LRU) {
								for (unsigned CCI = 0; CCI < ConflictSize; CCI++) {
									BBILRUCond[CCI] = BasicBlock::Create(TI->getContext(), \
													BB.getName() + "." + DestBB->getName() + "_lru_cond" + "." + Twine(BBSI));				
									BBILRUComp[CCI] = BasicBlock::Create(TI->getContext(), \
													BB.getName() + "." + DestBB->getName() + "_lru_comp" + "." + Twine(BBSI));
									
									//BBLRUCond.push_back(BBILRUCond);
									//BBLRUComp.push_back(BBILRUComp);
									BBLRUCond[BBSI][CCI] = BBILRUCond[CCI];
									BBLRUComp[BBSI][CCI] = BBILRUComp[CCI];
									InstrumentedBBList.push_back(BBILRUCond[CCI]);
									InstrumentedBBList.push_back(BBILRUComp[CCI]);
								}
							}

							//record the unlocked memory block
							EffectMemBlock[EffectiveSize] = MemBB;
							EffectiveSize++;
			}

			//instrument cache performance with the original code
			//iterate through the collected memory blocks and generate instrumented code
			for (int BBI = EffectiveSize - 1; BBI >= 0; BBI--) {
							unsigned BBSI = (unsigned)BBI;
							unsigned MemBB = EffectMemBlock[BBSI];

							//handle cache locking. if the memory block has been locked in the cache, 
							//then do not insert any instrument code
							if (IsMemBlockLockedInCache(MemBB)) 
											continue;

							//we also need to connect the first newly created conditional basic 
							//block or NewBBCond[0] with the original basic block "BB"
							if (BBI == 0)
											TI->setSuccessor(SuccI, NewBBColdCond[BBSI]);
		
							//create the global variable only if the global variable for the corresponding 
							//memory block is not created
							FindOrConstructGlobalVarForMem(MemBB, M);
							//for first time access
							FindOrConstructFlagVarForMem(MemBB, M);
							//cleekee: for LRU policy
							if (ReplacementPolicy == LRU)
								FindOrConstructLRUFlagVarForMem(MemBB, M);

							//cleekee: tracks data reference count
							DataRefBB.push_back(NewBBColdCond[BBSI]);

							//global value is always a pointer type. Therefore for a typed comparison, 
							//we need to first create a load instruction that would get the value of 
							//global variable
							Value *ConflictVal = new LoadInst(MemGlobal[MemBB], "__conflict_cache_test_val", NewBBCond[BBSI]);
							Value *FlagVal = new LoadInst(FlagMemGlobal[MemBB], "__conflict_cold_cache_test_val", \
											NewBBColdCond[BBSI]);

							//create conditional instruction
							//handle cache locking here.....in the presence of cache locking, the effective cache 
							//associativity is the original associativity subtracted by the number of locked cache 
							//lines

							//sanity check
							assert(getLockedLines(MemBB) <= getCacheAssoc() && "you are a SATAN.....you have locked too many.....");

							Value* LockedLines = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), getLockedLines(MemBB));
							Value *EffectConflictVal = BinaryOperator::CreateAdd(ConflictVal, LockedLines, "__effect_conflict_val", \
											NewBBCond[BBSI]);

							Value* CondInst = new ICmpInst(*(NewBBCond[BBSI]), ICmpInst::ICMP_SGE, EffectConflictVal, Assoc, \
											"cond");
							Value* ColdCondInst = new ICmpInst(*(NewBBColdCond[BBSI]), ICmpInst::ICMP_EQ, FlagVal, Zero, \
											"coldcond");

							//set the conditional targets of new basic blocks. NewBBCond[i] can branch to 
							//NewBBComp[i] or NewBBCond[i+1]. note that we need special hadling for the 
							//last basic block that is when i == BBSize-1. In that case, NewBBCond[i] can 
							//point to either NewBBComp[i] or DestBB (recall that "DestBB" was the original 
							//destination of basic block "BB" before any instrumentation)
							if (BBSI < EffectiveSize - 1) {
											BranchInst::Create(NewBBComp[BBSI], NewBBColdCond[BBSI+1], CondInst, NewBBCond[BBSI]);
											BranchInst::Create(NewBBColdComp[BBSI], NewBBCond[BBSI], ColdCondInst, NewBBColdCond[BBSI]);
							}				
							else {
											BranchInst::Create(NewBBComp[BBSI], DestBB, CondInst, NewBBCond[BBSI]);
											BranchInst::Create(NewBBColdComp[BBSI], NewBBCond[BBSI], ColdCondInst, NewBBColdCond[BBSI]);
											//record the original predecessor
											OriginalCoBBMap[NewBBCond[BBSI]] = &BB;
							}				
			
							//insert the instructions into NewBBComp to modify/update cache conflict count
							if (ReplacementPolicy == FIFO) {
								UpdateCacheConflict(M, *(NewBBComp[BBSI]), *(MemGlobal[MemBB]), MemBB, false);
								UpdateCacheConflict(M, *(NewBBColdComp[BBSI]), *(FlagMemGlobal[MemBB]), MemBB, true);
							} else {
								UpdateCacheConflictLRU(M, *(NewBBComp[BBSI]), *(MemGlobal[MemBB]), MemBB, false);
								UpdateCacheConflictLRU(M, *(NewBBColdComp[BBSI]), *(FlagMemGlobal[MemBB]), MemBB, true);
							}
							
							//set the successor of NewBBComp[i] to NewBBCond[i+1], note that 
							//we need special handling when "i" is the last created basic block, 
							//as NewBBComp[i] must unconditionally point to "DestBB" or the 
							//previous destination basic block of "BB"
							if (BBSI < EffectiveSize - 1) {
											BranchInst::Create(NewBBColdCond[BBSI+1], NewBBComp[BBSI]);
											BranchInst::Create(NewBBColdCond[BBSI+1], NewBBColdComp[BBSI]);
							}				
							else {
											//record the original predecessor
											OriginalCoBBMap[NewBBComp[BBSI]] = &BB;
											OriginalCoBBMap[NewBBColdComp[BBSI]] = &BB;
											BranchInst::Create(DestBB, NewBBComp[BBSI]);
											BranchInst::Create(DestBB, NewBBColdComp[BBSI]);
							}		
			
							//insert NewBBCond[i] and NewBBComp[i] into the function in sequence 
							//and just after BB
							IF = &BB;
							F->getBasicBlockList().insert(++IF, NewBBCond[BBSI]);
							IF = NewBBCond[BBSI];
							F->getBasicBlockList().insert(++IF, NewBBComp[BBSI]);
							IF = NewBBComp[BBSI];
							F->getBasicBlockList().insert(++IF, NewBBColdCond[BBSI]);
							IF = NewBBColdCond[BBSI];
							F->getBasicBlockList().insert(++IF, NewBBColdComp[BBSI]);
							IF = NewBBColdComp[BBSI];

							//cleekee: for LRU policy
#if 1
							if (ReplacementPolicy == LRU) {
								//get all mem blocks conflicting with MemBB
								std::vector<unsigned>::size_type ConflictSize = CacheConflictMap[MemBB].size();
								std::vector<unsigned> CacheConflict = CacheConflictMap[MemBB];

								for (unsigned CCI = 0; CCI < ConflictSize; CCI++) {		
									unsigned ConflictMem = CacheConflict[CCI];

									//relink to BBLRUCond
									if (CCI == 0) {
										//Odd but safe place to reset conflict count for MemBB to 0
										new StoreInst(Zero, MemGlobal[MemBB], BBLRUCond[BBSI][0]);

										NewBBCond[BBSI]->getTerminator()->removeFromParent();
										BranchInst::Create(NewBBComp[BBSI], BBLRUCond[BBSI][0], CondInst, NewBBCond[BBSI]);

										NewBBComp[BBSI]->getTerminator()->removeFromParent();
										BranchInst::Create(BBLRUCond[BBSI][0], NewBBComp[BBSI]);

										NewBBColdComp[BBSI]->getTerminator()->removeFromParent();
										BranchInst::Create(BBLRUCond[BBSI][0], NewBBColdComp[BBSI]);
									}

									//instrument BBLRUCond
									Value *LRUFlagVal = new LoadInst(GetLRUFlag(MemBB, ConflictMem),  \
													"__lru_flag_val", BBLRUCond[BBSI][CCI]);

									Value *LRUCondInst = new ICmpInst(*(BBLRUCond[BBSI][CCI]), ICmpInst::ICMP_EQ, 
													LRUFlagVal, (MemBB < ConflictMem)? Zero : One, "lru_flag_cond");
									
									//3 possible links from BBLRUCond to other BBs
									if (CCI < ConflictSize - 1) {
										BranchInst::Create(BBLRUComp[BBSI][CCI], BBLRUCond[BBSI][CCI+1], LRUCondInst, BBLRUCond[BBSI][CCI]);
									} else if (BBSI < EffectiveSize - 1) {
										BranchInst::Create(BBLRUComp[BBSI][CCI], NewBBColdCond[BBSI+1], LRUCondInst, BBLRUCond[BBSI][CCI]);
									} else {
										BranchInst::Create(BBLRUComp[BBSI][CCI], DestBB, LRUCondInst, BBLRUCond[BBSI][CCI]);
										//record the original predecessor?
										OriginalCoBBMap[BBLRUCond[BBSI][CCI]] = &BB;
									}

									//instrument BBLRUComp
									Value *ConflictVal = new LoadInst(MemGlobal[ConflictMem], "__conflict_load_val", BBLRUComp[BBSI][CCI]);
									Value *ConflictIncrement = BinaryOperator::CreateAdd(ConflictVal, One, \
													"__conflict_incr_val", BBLRUComp[BBSI][CCI]);
									new StoreInst(ConflictIncrement, MemGlobal[ConflictMem], BBLRUComp[BBSI][CCI]);
									
									//After this access, ConflictMem will be LRU than MemBB
									new StoreInst((MemBB < ConflictMem)? One : Zero, GetLRUFlag(MemBB, ConflictMem), BBLRUComp[BBSI][CCI]);

									//again, 3 possible links from BBLRUComp to other BBs
									if (CCI < ConflictSize - 1) {
										BranchInst::Create(BBLRUCond[BBSI][CCI+1], BBLRUComp[BBSI][CCI]);
									} else if (BBSI < EffectiveSize - 1) {
										BranchInst::Create(NewBBColdCond[BBSI+1], BBLRUComp[BBSI][CCI]);
									} else {
										BranchInst::Create(DestBB, BBLRUComp[BBSI][CCI]);
										//record the original predecessor?
										OriginalCoBBMap[BBLRUComp[BBSI][CCI]] = &BB;
									}

									//insert BBLRUCond[BBSI][CCI] and BBLRUComp[BBSI][CCI] into the function in sequence
									F->getBasicBlockList().insert(++IF, BBLRUCond[BBSI][CCI]);
									IF = BBLRUCond[BBSI][CCI];
									F->getBasicBlockList().insert(++IF, BBLRUComp[BBSI][CCI]);
									IF = BBLRUComp[BBSI][CCI];
								}
							}
#endif		

							//we do not have to update any other information as the transformation 
							//is merely done for our analysis purpose and is totally independent 
							//of any other transformation or analysis
							//-----end-----
			}

			if (EffectiveSize > 0) {
				//finally map the new destination with the original destination
				//this is used in some crucial parts of the path program exploration algorithm
				OriginalBBCondMap[NewBBColdCond[0]] = DestBB;
			}	
}

//update cache conflict count in the newly inserted basic block
void CodeTransform::UpdateCacheConflict(Module &M, BasicBlock &BB, Value &Conflict, unsigned MemBB, bool IsCold)
{
				//create a zero constant
				Value* Zero = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0);
				//create a one constant
				Value* One = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 1);
				
				//Step 1: reset the conflict count for the memory block which is just 
				//loaded into the cache
				if (!IsCold)
					new StoreInst(Zero, &Conflict, &BB);
				else	{
					new StoreInst(One, &Conflict, &BB);
					new StoreInst(Zero, MemGlobal[MemBB], &BB);
				}	

				//Step 2: increment the number of cache misses in the respective cache 
				//set
				
				//lazy create a global variable which computes the number of cache miss
				//in the respective cache set
				//FindOrConstructGlobalVarForCacheMiss(CacheSet, M);

				//increment cache miss count by creating a binary operator add
				//load-increment-store chain
				if (!IsCold) {
						Value *CacheMissVal = new LoadInst(GlobalCacheMiss, "__cache_miss_val", &BB);
						Value *MissIncrement = BinaryOperator::CreateAdd(CacheMissVal, One, "__cache_miss_incr", &BB);
						new StoreInst(MissIncrement, GlobalCacheMiss, &BB);
				} else {
						Value *CacheMissVal = new LoadInst(GlobalCacheColdMiss, "__cache_miss_val", &BB);
						Value *MissIncrement = BinaryOperator::CreateAdd(CacheMissVal, One, "__cache_miss_incr", &BB);
						new StoreInst(MissIncrement, GlobalCacheColdMiss, &BB);
				} 		

				//Step 3: increment the conflict count of all other memory blocks that 
				//may conflict in cache with memory block "MemBB"

				//first, lazy construct the cache conflict map for memory block "MemBB"
				FindOrConstructCacheConflictMap(MemBB, M);
				
				//get the size of conflict
				std::vector<unsigned>::size_type ConflictSize = CacheConflictMap[MemBB].size();
				std::vector<unsigned> CacheConflict = CacheConflictMap[MemBB];

				//go through the cache conflict list and increment respective cache conflict count
				for (unsigned CCI = 0; CCI < ConflictSize; CCI++) {
						//fetch the conflicting memory block id
						unsigned ConflictMem = CacheConflict[CCI];
						//sudiptac: ****CAUTION**** note that some global variables related 
						//to cache conflict may not have been created (as we do a lazy construction). 
						//therefore, you need to call the LAZY global variable allocator here 
						//as well
						FindOrConstructGlobalVarForMem(ConflictMem, M);
						//load-increment-store chain
						Value *ConflictVal = new LoadInst(MemGlobal[ConflictMem], "__conflict_load_val", &BB);
						Value *ConflictIncrement = BinaryOperator::CreateAdd(ConflictVal, One, \
										"__conflict_incr_val", &BB);
						new StoreInst(ConflictIncrement, MemGlobal[ConflictMem], &BB);
				}
				
				//====we are done here with instrumentation
}

//update cache conflict count in the newly inserted basic block (cleekee: for LRU policy)
void CodeTransform::UpdateCacheConflictLRU(Module &M, BasicBlock &BB, Value &Conflict, unsigned MemBB, bool IsCold)
{
				//create a zero constant
				Value* Zero = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0);
				//create a one constant
				Value* One = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 1);
				
				//lazy construct the cache conflict map for memory block "MemBB"
				FindOrConstructCacheConflictMap(MemBB, M);
				//get the size of conflict
				std::vector<unsigned>::size_type ConflictSize = CacheConflictMap[MemBB].size();
				std::vector<unsigned> CacheConflict = CacheConflictMap[MemBB];

				//Step 1: reset the conflict count for the memory block which is just 
				//loaded into the cache
				if (!IsCold)
					//reset LRU flag for all conflicting mem block pair with memBB
					//as MemBB (not in cache) is the least recently used
					for (unsigned CCI = 0; CCI < ConflictSize; CCI++) {
						unsigned ConflictMem = CacheConflict[CCI];
						FindOrConstructGlobalVarForMem(ConflictMem, M);
						new StoreInst((MemBB < ConflictMem)? Zero : One, GetLRUFlag(MemBB, ConflictMem), &BB);
					}
				else	{
					new StoreInst(One, &Conflict, &BB);

					for (unsigned CCI = 0; CCI < ConflictSize; CCI++) {
						unsigned ConflictMem = CacheConflict[CCI];
						FindOrConstructGlobalVarForMem(ConflictMem, M);
						new StoreInst((MemBB < ConflictMem)? Zero : One, GetLRUFlag(MemBB, ConflictMem), &BB);
					}
				}	

				//Step 2: increment the number of cache misses in the respective cache 
				//set
				
				//lazy create a global variable which computes the number of cache miss
				//in the respective cache set
				//FindOrConstructGlobalVarForCacheMiss(CacheSet, M);

				//increment cache miss count by creating a binary operator add
				//load-increment-store chain
				if (!IsCold) {
						Value *CacheMissVal = new LoadInst(GlobalCacheMiss, "__cache_miss_val", &BB);
						Value *MissIncrement = BinaryOperator::CreateAdd(CacheMissVal, One, "__cache_miss_incr", &BB);
						new StoreInst(MissIncrement, GlobalCacheMiss, &BB);
				} else {
						Value *CacheMissVal = new LoadInst(GlobalCacheColdMiss, "__cache_miss_val", &BB);
						Value *MissIncrement = BinaryOperator::CreateAdd(CacheMissVal, One, "__cache_miss_incr", &BB);
						new StoreInst(MissIncrement, GlobalCacheColdMiss, &BB);
				} 		
				
				//====we are done here with instrumentation
}

//sudiptac: returns the related instrumeneted computation block for 
//a specific instrumented conditional block. note that according to 
//our code instrumentation technique, this mapping is an one-to-one 
//correspondence
BasicBlock* CodeTransform::getRelatedBlock(BasicBlock* BB) 
{
				if (!CondCompMap.count(BB))
								return NULL;
				return CondCompMap[BB];				
}

//returns true if the given basic block is inserted as part of code 
//instrumentation to test performance
bool CodeTransform::IsInstrumentedBlock(BasicBlock* BB)
{
				std::vector<BasicBlock*>::iterator II;

				II = find(InstrumentedBBList.begin(), InstrumentedBBList.end(), BB);
				//return false if and only if the basic block was not inserted as 
				//part of cache performance instrumentation
				if (II == InstrumentedBBList.end())
								return false;

				return true;
}

//return the original target basic block with respect to a cache instrumented 
//basic block
BasicBlock* CodeTransform::GetOriginalBasicBlock(BasicBlock* BB)
{
				if (IsInstrumentedBlock(BB))
					return OriginalBBCondMap[BB];

				return BB;	
}

//return the original predecessor basic block with respect to a cache instrumented 
//basic block
BasicBlock* CodeTransform::GetOriginalPredecessor(BasicBlock* BB)
{
				if (IsInstrumentedBlock(BB))
					return OriginalCoBBMap[BB];

				return BB;	
}

//return the set of memory blocks accessed in a particular basic block
std::vector<unsigned>& CodeTransform::GetMemBlockFromBB(BasicBlock* BB)
{
				return MemBlockInBB[BB];
}

//returns true if the specified memory block is loked in the cache
bool CodeTransform::IsMemBlockLockedInCache(unsigned MemBlock)
{
				std::vector<unsigned>::iterator II;

				II = find(LockedMemoryBlocks.begin(), LockedMemoryBlocks.end(), MemBlock);
				//returns "true" if and only if the respective memory block is locked 
				//in the cache
				if (II == LockedMemoryBlocks.end())
								return false;
				
				return true;
}

//returns the number of locked cache lines in the cache set in which 
//the specified memory block is mapped
unsigned CodeTransform::getLockedLines(unsigned MemBlock)
{
			unsigned LockCount = 0;	
				
			for (unsigned LockI = 0; LockI < LockedMemoryBlocks.size(); LockI++) {
					if (getCacheSet(MemBlock) == getCacheSet(LockedMemoryBlocks[LockI]))
						LockCount++;		
			}
			
			return LockCount;
}

//insert the specified memory block in the cache lock database
void CodeTransform::UpdateLockedMemoryBlocks(unsigned MemBlock) 
{
			LockedMemoryBlocks.push_back(MemBlock);	
}

//record the set of memory blocks which are locked in cache
void CodeTransform::FetchLockedMemoryBlocks()
{
			if (!IsCacheLockingEnabled())	
				return;

			std::ifstream LockFd;
			LockFd.open(CacheLockFileName.c_str(), std::ios::in);
			unsigned MemBlock;
			
			//if no memory blocks are locked, we simply ignore this phase
			if (!LockFd) {
				cerr << "Found no cache locked memory blocks.....skipping this phase.....\n";
				return;
			}
			
			//build the locked memory data base here	
			while (!LockFd.eof()) {
				LockFd >> MemBlock;
				if (!IsMemBlockLockedInCache(MemBlock))
					LockedMemoryBlocks.push_back(MemBlock);
			}	
				
			LockFd.close();	
}

#if 0
//transform cache instrumented code for KLEE (note that the cache 
//instrumented code is not the same as the original code)
void CodeTransform::TransformCodeForSymbolicExec(std::ifstream &ISYM) {
		
		//first create the global string constants representing the symbolic 
		//input names.....never mind the same name "str", LLVM will do the 
		//renaming stuff for you. Pretty cool.....
		std::string line;
		GlobalVariable* SymName;
		while ( getline(ISYM, line) ) {
				Constant *StrConstant = ConstantArray::get(getGlobalContext(), line.c_str(), true);
				SymName = new GlobalVariable(*M, 
				                             StrConstant->getType(),		
				                             true, 
				                             GlobalValue::InternalLinkage, 
				                             StrConstant, 
				                             "str", 
				                             0, 
				                             false);
		}
		Function* F = M->getFunction("main");
		BasicBlock* BB = &(F->getEntryBlock());
		IRBuilder<>* Builder = new IRBuilder<>(BB);
		Value* Ptr = Builder->CreateGEP(SymName, ConstantInt::get(getGlobalContext(), APInt(32, 0)), "arrmax");
		Instruction* I = dyn_cast<Instruction>(Ptr);
		I->insertBefore(BB->begin());
}
#endif

bool CodeTransform::dcache_runCodeTransformOnModule(Module &M, std::ifstream& CacheIS)
{
	cerr << "\n\n************ START - DataCacheAnalysis::runCodeTransformOnModule() ************\n";

	//read cache configuration	
	ReadConfigFromFile(CacheIS);

	//split BB just before an array access
	SplitBB(M);

	//generate memory block sequences for each basic block 
	dcache_GenerateMemoryBlockSequence(M);
	
	//debug purpose
	PrintMemoryBlockSequence(M);
	
	//generate global cache miss counter
	ConstructGlobalCacheMissCounter(M);
	
	//transform the code for cache performance testing
	dcache_TransformCodeForCacheTest(M);

	cerr << "\n\n************ EXIT - DataCacheAnalysis::runCodeTransformOnModule() ************\n";
  return false;
}

void CodeTransform::SplitBB(Module &M)
{
	bool skipFirstIns = false; //skip first instruction of splitted BB

	BasicBlock::iterator firstGEPI;

  for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
		Function *F = dyn_cast<Function>(IM);

  	//iterate over all basic blocks
		for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
			BasicBlock* BB = dyn_cast<BasicBlock>(IF);

			//iterate over all instructions in BB
			for (BasicBlock::iterator IBB = BB->begin(), EBB = BB->end(); IBB != EBB; IBB++) {
				if (skipFirstIns) {
					skipFirstIns = false;
					continue;
				}

				if (IBB->getOpcode() == Instruction::Load && dyn_cast<GetElementPtrInst>(IBB->getOperand(0))) {
					splittedBlocks[BB->splitBasicBlock(IBB, BB->getName()+"-split")] = true;
					skipFirstIns = true;
					break;
				} else if (IBB->getOpcode() == Instruction::Store  && dyn_cast<GetElementPtrInst>(IBB->getOperand(1))) {
					splittedBlocks[BB->splitBasicBlock(IBB, BB->getName()+"-split")] = true;
					skipFirstIns = true;
					break;
				}
			}
		}
	}
}

void CodeTransform::dcache_GenerateMemoryBlockSequence(Module &M)
{
	int curMemSize = 0;

	//handle global variables' mapping to memory
	for (Module::global_iterator IGV = M.global_begin(), EGV = M.global_end(); IGV != EGV; IGV++) {
		GlobalVariable* GV = dyn_cast<GlobalVariable>(IGV);

		//ignore global constants introduced by KLEE
		if (GV->getName().startswith(".str")) continue;

		//maps the global variable data to a memory address
		int size = MapDataRef(GV, curMemSize);

		//points to next memory address
		curMemSize += size;
	}

	//for local stack allocation: align first memory address with cache set size
	int alignOffset = curMemSize % (NumCacheSets * MemBlockSize);
	if (alignOffset != 0)
		curMemSize += (NumCacheSets * MemBlockSize) - alignOffset;

	for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
		Function *F = dyn_cast<Function>(IM);
		if (F->empty()) continue;

		//handle alloc instructions in entry block
		BasicBlock* BBEntry = &(F->getEntryBlock());
		for (BasicBlock::iterator IBB = BBEntry->begin(), EBB = BBEntry->end(); IBB != EBB; IBB++) {
			AllocaInst* AI = dyn_cast<AllocaInst>(IBB);
			if (AI != NULL) {
				//get the allocated space size for each allocate instruction
				int size = MapDataRef(AI, curMemSize);

				curMemSize += size;
			}
		}

		//map each BB to their accessed memory blocks
		for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
			BasicBlock* BB = dyn_cast<BasicBlock>(IF);
			//collect all the memory blocks accessed in this BB
			std::vector<unsigned> LocalMemBlock;

			//collect the memory block sequence from a single basic block
			for (BasicBlock::iterator IB = BB->begin(), EB = BB->end(); IB != EB; IB++) {
				//check if not an injected code (e.g. additional code meant for KLEE 
				//symbolic execution engine)
				if (IsInjectedCode(IB))
					continue;			

				//analyze successor block for number of load/store instructions
				char dRefStr[16];

				Instruction* I = dyn_cast<Instruction>(IB);
				if (I == NULL) 
					continue;

				int operandPos;
				if (I->getOpcode() == Instruction::Load)
					operandPos = 0;
				else if (I->getOpcode() == Instruction::Store) 
					operandPos = 1;
				else 														
					continue;
					
				//ignore dynamic data reference
				if (splittedBlocks[BB] && IB == BB->begin())
					continue;		

				sprintf(dRefStr, "%p", (void*) I->getOperand(operandPos));

				//if data reference is accessing an array/struct
				GEPOperator* gepo = dyn_cast<GEPOperator>(I->getOperand(operandPos));
				if (gepo != NULL) {
					AddAccessedMemBlock(gepo, &LocalMemBlock);
				} 
				//if data ref is from scalar variable instead, just add the accessed memory block
				else {
					int memBlocks[2];
					for (int i = 0; i < GetMemBlockFromDRef(I->getOperand(operandPos), memBlocks); i++)
						LocalMemBlock.push_back(memBlocks[i]);
				}
			}

			//save all accessed memory blocks into their corresponding BB
			MemBlockInBB[BB] = LocalMemBlock;
		}
	}	
}

// This method uses a generic way of traversing array/struct to get memory address of a specific reference, when
// the array index is a variable, or a struct member is referenced (for a struct in an array).
// 
// refSize = size (in bytes) of referenced array/struct element
// stride = distance (in bytes) between any two consecutive possible references
// firstRefPos = address of first possible reference
// lastRefPos = address of last possible reference
void CodeTransform::MapArrayIndexBoundToMemBlock(std::vector<int> *arrayIndexBound, std::vector<int> *boundMemBlock,
                                             int refSize, int stride, int firstRefPos, int lastRefPos)
{
	int accessedMemBlock = -1;
	int addr;
	int arrayIndex = 0;
	
	addr = firstRefPos;

	do {
		int currentMemBlock = addr/MemBlockSize;
		if (accessedMemBlock < currentMemBlock) {
			accessedMemBlock = currentMemBlock;
			arrayIndexBound->push_back(arrayIndex);
			boundMemBlock->push_back(accessedMemBlock);
		}
		addr = addr + refSize + stride;
		arrayIndex++;
	} while (addr < lastRefPos);
}

//cleekee: currently only supports a single level of nested array/struct, code is a bit inflexible
BasicBlock* CodeTransform::InstrumentArrayAccess(BasicBlock &BB, Module &M)
{
	int refSize, stride, firstRefPos, lastRefPos;
	Value* arrayIndexVal;
	
	//mapping of array index bound to accessed mem block(in a loop)
	std::vector<int> arrayIndexBound;
	std::vector<int> boundMemBlock;
	
	TerminatorInst *TI = BB.getTerminator();
	Function *F = BB.getParent();

	//sanity check, splitted BB should be the only successor of original BB
	if (TI->getNumSuccessors() > 1 || TI->getNumSuccessors() == 0) return NULL;

	BasicBlock* DestBB = TI->getSuccessor(0);
	
	//proceed only if BB is splitted
	if (!splittedBlocks[DestBB]) return NULL;

	//get the first instruction, which should refer to the array/struct access
	Instruction* I = dyn_cast<Instruction>(DestBB->begin());

	int operandPos;
	if (I->getOpcode() == Instruction::Load) 
		operandPos = 0;
	else if (I->getOpcode() == Instruction::Store) 
		operandPos = 1;
	else
		assert(0 && "First instruction of splitted BB is not load/store!");

	//check whether there is nested array/struct
	User* GEP = dyn_cast<GEPOperator>(I->getOperand(operandPos));
	User* nestedGEP = dyn_cast<GEPOperator>(GEP->getOperand(0));
	if (nestedGEP == NULL) {
		//Checkpoint: data structure must be in the form of 'array[VARIABLE]'
		
		refSize = GEP->getOperand(0)->getType()->getContainedType(0)->getContainedType(0)->getPrimitiveSizeInBits() / 8;
		stride = 0;
		firstRefPos = DRefMemMap[GEP->getOperand(0)];
		lastRefPos = firstRefPos + GetSizeOfType(GEP->getOperand(0)->getType()->getContainedType(0));
		MapArrayIndexBoundToMemBlock(&arrayIndexBound, &boundMemBlock, refSize, stride, firstRefPos, lastRefPos);

		arrayIndexVal = GEP->getOperand(2);
	} else {
		//is array/struct accessed statically or index is a variable?
		ConstantInt* ci = dyn_cast<ConstantInt>(GEP->getOperand(2));
		if (ci == NULL) {
			//TODO: should be easily implementable along the same line with the other data structures implemented
			if (nestedGEP->getOperand(0)->getType()->getContainedType(0)->getTypeID() != llvm::Type::StructTyID) 
				assert(0 && "Sorry, data structure in the form of 'array[CONSTANT][VARIABLE]' is not supported yet!");
				
			//Checkpoint: data structure must be in the form of 'struct with the referenced array as struct member'

			refSize = GEP->getOperand(0)->getType()->getContainedType(0)->getContainedType(0)->getPrimitiveSizeInBits() / 8;
			stride = 0;
			//TODO: 2 lines below are wrong in some cases, keep at the moment
			firstRefPos = DRefMemMap[nestedGEP->getOperand(0)];
			lastRefPos = firstRefPos + GetSizeOfType(nestedGEP->getOperand(0)->getType()->getContainedType(0));
			MapArrayIndexBoundToMemBlock(&arrayIndexBound, &boundMemBlock, refSize, stride, firstRefPos, lastRefPos);

			arrayIndexVal = GEP->getOperand(2);
		} else {
			//Checkpoint: data structure must be in the form of 'array[VARIABLE][CONSTANT]'
			
			//calculate offset to first reference address
			unsigned startOffset = 0;
			for (unsigned i = 0; i < ci->getLimitedValue(); i++)
				startOffset += GEP->getOperand(0)->getType()->getContainedType(0)->getContainedType(i)->getPrimitiveSizeInBits() / 8;
			
			//refSize is the size of Value returned by GEP
			refSize = GEP->getType()->getContainedType(0)->getPrimitiveSizeInBits() / 8;
			stride = GetSizeOfType(nestedGEP->getType()->getContainedType(0)) - refSize;
			firstRefPos = DRefMemMap[nestedGEP->getOperand(0)] + startOffset;
			lastRefPos = firstRefPos + GetSizeOfType(nestedGEP->getOperand(0)->getType()->getContainedType(0));
			MapArrayIndexBoundToMemBlock(&arrayIndexBound, &boundMemBlock, refSize, stride, firstRefPos, lastRefPos);

			arrayIndexVal = nestedGEP->getOperand(2);
		}
	}

	//create instrumented BB to insert conditional check for array index bound
	std::vector<BasicBlock*> NewBBCond;
	for (unsigned i = 0; i < arrayIndexBound.size(); i++) {
		BasicBlock* BBCond = BasicBlock::Create(TI->getContext(), 
			BB.getName() + "." + DestBB->getName() + "_dref_cond");			

		NewBBCond.push_back(BBCond);

		InstrumentedBBList.push_back(BBCond);
	}

	//all instrumented BBs created for array access will point to this dummy BB, which simplifies
	//continuation of instrumentation for other data references afterwards
	BasicBlock* BBDummy = BasicBlock::Create(TI->getContext(), 
		BB.getName() + "." + DestBB->getName() + "_dref_dummy");	
	InstrumentedBBList.push_back(BBDummy);

	//instrument created BBs
	for (unsigned i = 0; i < arrayIndexBound.size(); i++) {
		//Value* rangeVal = ConstantInt::get(Type::getInt64Ty(getGlobalContext()), arrayIndexBound.at(i));
		Value* rangeVal = ConstantInt::get(arrayIndexVal->getType(), arrayIndexBound.at(i));

		Value* ArrCondInst = new ICmpInst(*NewBBCond[i], ICmpInst::ICMP_ULE, arrayIndexVal, rangeVal, "dref_cond");
	
		//inform parent function of the extra BBs
		Function::iterator IF = (i == 0)? &BB : NewBBCond[i-1];
		F->getBasicBlockList().insert(++IF, NewBBCond[i]);

		//link up all the new BBs
		if (i == 0)
			TI->setSuccessor(0, NewBBCond[i]);

		if (i < NewBBCond.size()-1)
			BranchInst::Create(BBDummy, NewBBCond[i+1], ArrCondInst, NewBBCond[i]);
		else
			BranchInst::Create(BBDummy, BBDummy, ArrCondInst, NewBBCond[i]);

		//add in dummy block at the end
		if (i == NewBBCond.size()-1) {			
			BranchInst::Create(DestBB, BBDummy);

			IF = NewBBCond[i];
			F->getBasicBlockList().insert(++IF, BBDummy);	
		}

		std::vector<unsigned> tempVector;
		tempVector.push_back(boundMemBlock.at(i));
		InstrumentSuccessorOfBB(*NewBBCond[i], M, 0, tempVector);
	}

	//finally map the new destination with the original destination
	//this is used in some crucial parts of the path program exploration algorithm
	OriginalBBCondMap[NewBBCond[0]] = DestBB;

	return BBDummy;
}

void CodeTransform::dcache_TransformCodeForCacheTest(Module &M)
{
  //iterate over all the functions in the module M
  for (Module::iterator IM = M.begin(), EM = M.end(); IM != EM; IM++) {
		Function* F = dyn_cast<Function>(IM);
		std::string fname = F->getName();
		//EscapeString(fname);
		cerr << "\n\n===========In Function (" << fname << ").....===========\n";
		std::vector<BasicBlock *> OriginalBBList;

		//iterate over all the basic blocks inside the function F 
		for (Function::iterator IF = F->begin(), EF = F->end(); IF != EF; IF++) {
			//dynamic cast and get the typed basic block pointer
			BasicBlock* BB = dyn_cast<BasicBlock>(IF);
			OriginalBBList.push_back(BB);
		}

		//number of basic blocks
		cerr << "Number of basic blocks in the subject program = " << OriginalBBList.size() << ".....\n";

		//add a virtual entry block as predecessor for the real entry block
		//(to allow AnnotateBBWithCacheMiss() to annotate entry block as well)
		if (fname == "main") {
			BasicBlock* BBRealEntry = &(F->getEntryBlock());
			BasicBlock* BBVirtualEntry = BasicBlock::Create(F->getContext(), "virtual_entry", F, BBRealEntry);
			BranchInst::Create(BBRealEntry, BBVirtualEntry);

			//begin code instrumentation
			AnnotateBBWithCacheMiss(*BBVirtualEntry, M);	
		}

		//proceed with the rest of BBs
		for (unsigned BBI = 0; BBI < OriginalBBList.size(); BBI++) {
			BasicBlock *lastInstrumentedBlock = InstrumentArrayAccess(*(OriginalBBList[BBI]), M);
			
			//if BB is instrumented for array access, insert the rest of instrumentation after the last inserted block
			if (lastInstrumentedBlock != NULL)
				AnnotateBBWithCacheMiss(*lastInstrumentedBlock, M);  
			else
				AnnotateBBWithCacheMiss(*(OriginalBBList[BBI]), M);  
		}
  }

	//Add instrumentation to track data reference count in certain BBs
	AnnotateBBWithDataRefCount(M);
}

int CodeTransform::GetSizeOfType(const Type* type)
{
	int size = 0;

	if (type->getTypeID() == llvm::Type::ArrayTyID) {
		size = cast<ArrayType>(type)->getNumElements() * GetSizeOfType(type->getContainedType(0));
	} else if (type->getTypeID() == llvm::Type::PointerTyID) {
		size = 4;
	} else if (type->getTypeID() == llvm::Type::StructTyID) {
		int numElem = cast<StructType>(type)->getNumElements();
		for (int i = 0; i < numElem; i++)
			size += GetSizeOfType(type->getContainedType(i));
	} else {
		size = type->getPrimitiveSizeInBits()/8;
	}

	return size;
}

int CodeTransform::MapDataRef(Value* value, int curMemSize)
{
	const Type* type = value->getType()->getContainedType(0);
	int size = 0;

	if (type->getTypeID() == llvm::Type::ArrayTyID) {
		size = cast<ArrayType>(type)->getNumElements() * GetSizeOfType(type->getContainedType(0));
		DRefMemMap[value] = curMemSize;
	} else if (type->getTypeID() == llvm::Type::PointerTyID) {
		size = 4;
	} else if (type->getTypeID() == llvm::Type::StructTyID) {
		for (unsigned i = 0; i < cast<StructType>(type)->getNumElements(); i++) 
			size += GetSizeOfType(type->getContainedType(i));
	} else {
		size = type->getPrimitiveSizeInBits()/8;
		DRefMemMap[value] = curMemSize;
	}

	return size;
}

int CodeTransform::GetMemBlockFromDRef(Value* value, int memBlocks[2])
{
	if (value->getType()->getNumContainedTypes() > 0)
		return GetMemBlockFromDRef(DRefMemMap[value], value->getType()->getContainedType(0)->getPrimitiveSizeInBits()/8, memBlocks);
	else
		return GetMemBlockFromDRef(DRefMemMap[value], value->getType()->getPrimitiveSizeInBits()/8, memBlocks);
}
			
int CodeTransform::GetMemBlockFromDRef(int memAddr, int size, int memBlocks[2])
{
	memBlocks[0] = memAddr / MemBlockSize;
	memBlocks[1] = ceil((double) (memAddr + size) / MemBlockSize) - 1;

	if (memBlocks[1] > memBlocks[0])
		return 2;
	else
		return 1;
}

void CodeTransform::AddAccessedMemBlock(User* gepo, std::vector<unsigned> *LocalMemBlock)
{
	if ((dyn_cast<GEPOperator>(gepo) == NULL) && (dyn_cast<GEPOperator>(gepo) == NULL)) 
		return;

	const Type* dRefType = gepo->getOperand(0)->getType()->getContainedType(0);

	int dimension = gepo->getNumOperands() - 2;
	int offset = 0;
	int dRefSize;

	Type* type = (Type*) dRefType;

	for (int i = 0; i < dimension; i++) {
		//return if is a data reference with variable index
		int index;
		ConstantInt* ci = dyn_cast<ConstantInt>(gepo->getOperand(2 + i));
		if (ci != NULL)
			index = ci->getLimitedValue();
		else
			return;

		//add up offset before object at index
		if (type->getTypeID() == llvm::Type::ArrayTyID) {
			int elementSize = GetSizeOfType(type->getContainedType(0));
			offset += index * elementSize;

			type = (Type*) type->getContainedType(0);
		} else if (type->getTypeID() == llvm::Type::StructTyID) {
			//iterate through struct members till member with index of structIndex
			for (int j = 0; j < index; j++)
				offset += GetSizeOfType(type->getContainedType(j));

			type = (Type*) type->getContainedType(index);
		}
		
		//get the size of the referred data
		if (i == dimension - 1)
			dRefSize = GetSizeOfType(type);	
	}

	int memBlocks[2];
	for (int i = 0; i < GetMemBlockFromDRef(DRefMemMap[gepo->getOperand(0)]+offset, dRefSize, memBlocks); i++)
		LocalMemBlock->push_back(memBlocks[i]);
}

//cleekee: instrument each data reference point once to count total data reference
void CodeTransform::AnnotateBBWithDataRefCount(Module& M)
{
	for (unsigned i = 0; i < DataRefBB.size(); i++) {
		if (!DataRefCount) {
			//NOTE: name of global variable is shared with machine instruction count, necessary 
			//with current implementation so that KLEE will collect the data, can be changed in future
			DataRefCount = new GlobalVariable(M, Type::getInt32Ty(getGlobalContext()), \
				false, GlobalValue::ExternalLinkage, 0, "__machine_inst_global", 0, false);
			Constant* Zero = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0, true);
			DataRefCount->setInitializer(Zero);
		}

		Instruction *firstI = DataRefBB.at(i)->getFirstNonPHI();

		Value *DataRefVal = new LoadInst(DataRefCount, "__machine_inst_global_val", firstI);
		Value *One = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 1);
		Value *AddOne = BinaryOperator::CreateAdd(DataRefVal, One, \
					"__machine_inst_incr_val", firstI);
		new StoreInst(AddOne, DataRefCount, firstI);
	}
}

char CodeTransform::ID = 0;
