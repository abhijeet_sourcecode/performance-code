#ifndef __SAT_ENCODER_H_
#define __SAT_ENCODER_H_

#include "llvm/Pass.h"
#include "llvm/Module.h"
#include "llvm/ModuleProvider.h"
#include "llvm/PassManager.h"
#include "llvm/PassManagers.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Constants.h"
#include "llvm/Instructions.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Transforms/IPO.h"
#include "llvm/Transforms/Utils/BasicInliner.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/FunctionUtils.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/Support/Streams.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/ExecutionEngine/Interpreter.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/Target/TargetSelect.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Assembly/PrintModulePass.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/raw_ostream.h"
#include <memory>
#include <algorithm>
#include <fstream>

using namespace llvm;

class DynamicSymExec;

//main class for SAT encoding control flow graph
class SATEncoder {
				friend class DynamicSymExec;

				public:
				typedef std::pair<BasicBlock*, BasicBlock*> PredType;
				typedef DenseMap <PredType, std::pair<std::string, unsigned> > PredMapType;
				typedef std::pair<PredType, unsigned> LiteralType;
				typedef std::vector<LiteralType> ClauseType;	
				typedef std::vector<ClauseType> FormulaType;
				typedef DenseMap <Loop*, std::vector<PredType> > MSCCMapType;

				private:
				
				//module for which SAT encoding is created
				Module* SATModule;

				//loop information for the module
				LoopInfo* LI;

				//keep a record of all the loops in the main module
				std::vector<Loop*> AllLoops;

				//MSCC entry node information
				MSCCMapType MSCCEntryInfo;

				//MSCC exit edge information
				MSCCMapType MSCCExitInfo;

				//Loop entry edge information
				MSCCMapType LoopEntryInfo;
		
				//Loop exit edge information
				MSCCMapType LoopExitInfo;
				
				//filename providing the SAT encoding to the solver
				StringRef* SATFileName;

				//filename providing the solved output from the SAT solver
				StringRef* SATOutFile;

				//define predicates on control flow edges
				PredMapType EdgePredicates;

				//number of literals constructed for encoding
				unsigned NumPredicates;

				//number of CNF clauses constructed for encoding
				unsigned NumClauses;

				//CNF Formula holding the SAT encoding of the module
				FormulaType CNFFormula;

				public:

				SATEncoder(Module *M, const char* InFile, const char* OutFile) : NumPredicates(0), NumClauses(0) {
								SATModule = M;
								SATFileName = new StringRef(InFile);
								SATOutFile = new StringRef(OutFile);
				}

				//return number of literals
				unsigned getNumPredicates() { return NumPredicates; }
				
				//return number of CNF clauses
				unsigned getNumClauses() { return NumClauses; }

				//construct an MSCC decomposition of the given function
				void ConstructMSCCDecomposition(Function *F);
				
				//construct a SAT encoding of the MSCC decomposition 
				//of control flow graph
				void ConstructSATEncoding();

				//construct the clauses for the edge defined by pair <BB,DestBB> 
				void ConstructCNFClausesForEdge(BasicBlock* BB, BasicBlock* DestBB);
				
				//construct CNF clauses to identify incoming edge constraints
				void ConstructClausesForInCons(BasicBlock* BB, BasicBlock* DestBB);
				
				//construct CNF clauses to identify incoming edge constraints
				//this function is used only when the source of the edge belong 
				//to some MSCC (here specified through "SCC")
				void ConstructClausesForInConsMSCC(BasicBlock* SCC, BasicBlock* DestBB);

				//construct CNF clauses to identify outgoing edge constraints
				//this function is used only when the destination of the edge 
				//belong to some MSCC (here specified through "SCC")
				void ConstructClausesForOutConsMSCC(BasicBlock* BB, BasicBlock* SCC);

				//construct CNF clauses to identify outgoing edge constraints
				void ConstructClausesForOutCons(BasicBlock* BB, BasicBlock* DestBB);

				//construct special CNF clauses for program entry node
				void ConstructClausesForEntryNode(BasicBlock* Entry);
				
				//construct special CNF clauses for program exit node
				void ConstructClausesForExitNode(BasicBlock* Exit);

				//construct the predicate for an control flow edge defined by 
				//(BB,DestBB)
				void ConstructEdgePredicates(BasicBlock* BB, BasicBlock* DestBB);

				//construct CNF clauses to distinguish edges inside an MSCC
				void ConstructCNFClausesForMSCCEdges();

				//construct blocking clauses for a set of edges and add them to the 
				//overall SAT encoding
				void AddBlockingClauses(std::vector<PredType> BlockingEdges, std::vector<PredType> IntfEdges);

				//prints the SAT encoding in a text file, which will be processed 
				//by the solver (current format used: MINISAT solver)
				void PrintSATEncoding(unsigned int ExploreCount = 0);

				//call the SAT solver to solve the current SAT encoding
				bool Solve(unsigned int ExploreCount = 0);
				
				//get the MSCC identity of a basic block aka the outermost loop level
				Loop* getMSCCFor(BasicBlock* BB) 
				{
								Loop* LBB = LI->getLoopFor(BB);
								if (!LBB) return NULL;
								while (LBB->getLoopDepth() > 1)
												LBB = LBB->getParentLoop();
				
								return LBB;
				}

				//get the LoopInfo
				LoopInfo* getLoopInfoFromSAT() {
								return LI;
				}
		
				//get number of paths
				void computePathProgramNumbers(Module &M);
};

#endif
