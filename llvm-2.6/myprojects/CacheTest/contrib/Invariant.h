#ifndef __INVARIANT_H_
#define __INVARIANT_H_

#include "llvm/Pass.h"
#include "llvm/Module.h"
#include "llvm/ModuleProvider.h"
#include "llvm/PassManager.h"
#include "llvm/PassManagers.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Constants.h"
#include "llvm/Instructions.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/FunctionUtils.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/Support/Streams.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/ExecutionEngine/Interpreter.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/Target/TargetSelect.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Assembly/PrintModulePass.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/raw_ostream.h"
#include <memory>
#include <algorithm>
#include <fstream>
//sudiptac: header file related to STP, SMT solver libraries
#include "../../../stp/src/c_interface/c_interface_llvm.h"
#include "CodeTransform.h"
#include "PathProgram.h"
#include "SATEncoder.h"

using namespace llvm;

//following headers and namespaces are related to the Parma 
//Polyhedra library. note that we need to link the libraries 
//peoperly with our driver
//#include <ppl.hh>
//using namespace Parma_Polyhedra_Library;
//using namespace Parma_Polyhedra_Library::IO_Operators;
//#include "ppl/ppl_util.hh"

class InvariantMap {
				public:
				typedef std::pair<BasicBlock*, BasicBlock*> EdgeType;
				typedef std::vector<EdgeType> EdgeList;
				typedef std::vector<std::pair<BasicBlock*, bool> > TraceType;

				private:
				//module for which the inductive invariant map 
				//is generated
				Module* M;

				//cache instrumented module
				CodeTransform* CTModule;

				//Path program for which the inductive invariant 
				//map is generated
				PathProgram* CachePathProgram;

				//execution trace corresponding to one of the path in this path program
				TraceType* ExecTrace;

				//set of edges for which blocking clauses need to be 
				//added to the SAT encoder
				EdgeList BlockEdges;
				
				//set of edges for which interference clauses need to be 
				//added to the SAT encoder
				EdgeList InterferenceEdges;

				//maximum number of cache miss
				long MaxCacheMiss;

				//minimum number of cache miss
				long MinCacheMiss;

				//maximum execution cycles
				long MaxExeCycles;

				//minimum execution cycles
				long MinExeCycles;

				//bucket condition
				Expr BucketCond;

				//SAT encoding database
				SATEncoder* SATFormula;


				public:
				InvariantMap() {}

				InvariantMap(Module* Mo, PathProgram* CPP, CodeTransform* CTM, SATEncoder* SATF, TraceType* ET) 
								: MaxCacheMiss(-1), MinCacheMiss(-1), MaxExeCycles(-1), MinExeCycles(-1) {
								
								M = Mo; 
								CachePathProgram = CPP;
								CTModule = CTM;
								SATFormula = SATF;
								ExecTrace = ET;
				}
				
				#if 0
				//test module for PPL library (polyhedra + octagon) 
				//=====remove this (courtesy: test suite PPL)
				void test() {
				#if 0
								Variable A(0);
								Variable B(1);

								C_Polyhedron ph(2);
								ph.add_constraint(A >= 0);
								ph.add_constraint(A <= 2);
								ph.add_constraint(A >= -1);

								std::cerr << "*** ph ***" << "\n" << ph.constraints() << "\n";			

								ph.add_constraint(B >= 1);

								C_Polyhedron known_result(2);
								known_result.add_constraint(A >= 0);
								known_result.add_constraint(A <= 2);
								known_result.add_constraint(B >= 1);

								std::cerr << "*** after ph.add_constraint(B >= 1) ***" << "\n"\
												<< ph.constraints() << "\n";
				#endif
								int m = 0;
								m++;m++;m++;
								Variable k(0);
								Variable l(1);
								Linear_Expression y(l);
								Linear_Expression z;
								Linear_Expression x(2);
								Linear_Expression w(k+l);
								//Variable A(0);
								//Variable B(1);
								//Variable C(0);
								Constraint C = x <= 0;
								Constraint CC = w >= x;
								TOctagonal_Shape bds(3);
								//bds.add_constraint(A == 0);
								//bds.add_constraint(B == 0);
								//bds.add_constraint(A + C == 0);
								//bds.add_constraint(w == 0);
								//bds.unconstrain(A);
								CC.print();
								cerr << "\n";
								C.print();
								std::filebuf fb;
								std::filebuf fb1;
								fb.open ("test.txt",std::ios::out);
								std::ostream os(&fb);
								cerr << "\n";
								cerr << "ascii dump (CC)=====\n";
								CC.ascii_dump();
								os << "size " << m << " -2 1 1 f -RPI_V +RPI  -NNC_V -NNC";
								//CC.ascii_dump(os);
								fb.close();
								cerr << "\n";
								fb1.open ("test.txt",std::ios::in);
								std::istream is(&fb1);
								Constraint CCC(C);
								cerr << "[before] ascii dump (CCC)=====\n";
								CCC.ascii_dump();
								CCC.ascii_load(is);
								cerr << "\n";
								cerr << "[after] ascii dump (CCC)=====\n";
								CCC.ascii_dump();
								bds.add_constraint(CCC);
								//bds.unconstrain(C);
 
							//	bool ok; 
							//	TEST_PREDICATE_TRUE(bds.constrains(A));
							//	TEST_PREDICATE_TRUE(bds.constrains(B));
							//	TEST_PREDICATE_TRUE(bds.constrains(C));
				}
				#endif
				
				//input dependent branch list
				EdgeList SymBranchList;

				//get the set of blocking edges for the corresponding path program
				EdgeList& getBlockingEdges() {return BlockEdges;}
				
				//get the set of interference edges for the corresponding path program
				EdgeList& getIntfEdges() {return InterferenceEdges;}

				//get the path program for the corresponding invariant map
				PathProgram* getPathProgram() {return CachePathProgram;}

				//build an inductive invariant map over a particular path program
				void ConstructInvariantOverPathProgram();

				//build/update invariants over basic blocks, changed variable is 
				//set if any change happens in the invocation
				void GenerateBasicBlockInvariants(BasicBlock* BB, bool &changed);

				//extract sufficient control flow edges from a path program and 
				//this invariant map
				void ExtractEdgesFromPathProgram();

				//Analyze cache misses along a particular path program
				void AnalyzeCacheMissesOverPathProgram();

				//create a selective fork file to execute/analyze a path program
				void CreatePathProgramBranches(PathProgram* CPP);

				//run KLEE selectively over a path program
				void runSelectiveKLEE();

				//read and set the maximum and minimum number of cache misses
				void SetMaxMinCacheMisses();

				//get maximum number of cache misses
				long GetMaxCacheMisses() {return MaxCacheMiss;}
				
				//get maximum number of execution cycles
				long GetMaxExeCycles() {return MaxExeCycles;}

				//Debug path program cache performance
				void DebugPathProgramCachePerformance();

				//get minimum number of cache misses
				long GetMinCacheMisses() {return MinCacheMiss;}
				
				//get minimum number of execution cycles
				long GetMinExeCycles() { return MinExeCycles; }

				//get path program condition
				Expr GetPPCond() { return BucketCond; }

				#if 0
				
				//returns true if there is conflict between the memory blocks accessed 
				//in BB and the current path program
				bool CheckCacheConflictWithCurrentPathProgram(BasicBlock* BB);

				//update interference edge information for a path program, used by the 
				//SAT encoder
				void UpdateInterferenceEdges(BasicBlock* BB);

				//check reachability of two basic blocks BB->DestBB
				bool CheckReachability(BasicBlock* BB, BasicBlock* DestBB);
				
				#endif				
};

#endif
