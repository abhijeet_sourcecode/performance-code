#include "DynamicSymExecution.h"
#define SMT_SOLVER_TYPE "stp"
#define SMALLSKIP "  "
#define EXE_TYPE "klee"
#define KLEE_DIR "kleedir/"
#define KLEE_TEST_DIR "klee-last/"
#define KLEE_PATH_FILE_EXT "path"
#define KLEE_FILE_EXT "bc"
#define KLEE_PATH_FILENAME "replay"
#define KLEE_CVC_FILENAME "test000001"
#define KLEE_ASSERT_FAIL "assertion_failure"
#define CVC_EXT ".cvc"
#define PATH_EXT ".path"

//following options are related to KLEE executable options
#define REPLAY_PATH " -replay-path "
#define KLEE_OTHER_OPTIONS " -write-cvcs "
#define EMIT_ALL_ERRORS "-emit-all-errors"
#define POSITIVE 1
#define NEGATED 0
#define SIZE_OF_BYTE 8
//for safety ;-)
#define TIMEOUT 4096
#define MAGIC_NUMBER 99999
//holds the set of locked memory blocks
//usage in the feedback driven optimization
#define LOCK_FILENAME "main.mem.lock"
#define INTERNAL_CALL "klee_make_symbolic"

//store the number of assertions, per basic block
std::map<std::string, int> assertions_per_block;
std::map<std::string, std::string> assertion_marker;//<block,assertion_child>
//tracks the number of violated assertions, by one KLEE run
std::map<std::string, std::pair<int,bool> > violatedAssertsTracker;
std::vector<std::string> uniqueViolatedTracker;
std::map<std::string, std::string> uniqueAssertionsRead;//from the symboltabel <symbol,assert>
std::map<std::string, std::string> block_to_assertions_map;//<blocklabel,assert>
std::map<std::string, int> assertions_touched;

//tracks the brnaches (true and false), per basic block
std::map<std::string, std::pair<std::string,std::string> >  branches_per_block;
//stores the total number of unchecked assertions, unchecked assertions on the true (and false) branches, per basic block 
std::map<std::string, std::pair<int,std::pair<int, int> > >  assert_count;

int total_assertions = 0;
int total_violated = 0;
int ua_count = 0;
//int total_touched = 0; 
int assertions_tested = 0;
std::ofstream time_file;
std::ofstream touch_file;
std::ofstream test_file;
//std::ofstream max_file;

std::string valueString;
char bigString[256];

struct timeval analysis_start_time;

int DynamicSymExec::getBranches(std::string task) {

  //std::string postfileread = "__converted__KLEE.ll";
  std::string postfileread = ".ll";
  std::string postfilewrite = "_record";
  std::string temp = task.substr(0,task.length()-3);
  std::string input_file = temp.append(postfileread);
  const char* input_filename = input_file.c_str();

  std::ifstream iFile(input_filename);
  if(!iFile.is_open()){
    cerr<<"Could not open "<<input_filename<< std::endl;
    exit(0);
  }

  std::string line;
  std::string assertCalls("__assert_fail");
  std::string entry("entry:");
  std::string block("bb");
  std::string colon(":");
  std::string label("label");
  std::string comma(",");

  int start = 0;

  int pos,pos2;
  std::string last_label;
  std::string branch_label_1;
  std::string branch_label_2;

    start = 1;
  while(getline(iFile, line)) {
    if(line.find(entry)!=-1){
  //    start = 1;
      last_label = "entry";
    }
    if(start){
      pos = line.find(colon);
      if(pos!=-1 && start){
      //  assertions_per_block.insert(std::make_pair(last_label,total));
        last_label = line.substr(0,pos);
        branch_label_1 = "NOLABEL";
        branch_label_2 = "NOLABEL";
      }
      pos = line.find(label);
      //cerr << line << "\n";
      pos2 = line.find(label,pos+6);
      if(pos2!=-1 && pos!=-1 && start){
        
        branch_label_1 = line.substr(pos+7,pos2-pos-9);
        branch_label_2 = line.substr(pos2+7,line.length());
      }
      if(pos2==-1 && pos!=-1 && start){
        branch_label_1 = line.substr(pos+7,line.length());
      }
      if(pos!=std::string::npos){
      //  cerr << "\n"<<last_label ;
      //  cerr << ":" << branch_label_1 ;//<< branch_label_1.length();
      //  cerr << " + " << branch_label_2 ;//<< branch_label_2.length();
       
  
        branches_per_block.insert(std::make_pair(last_label,std::make_pair(branch_label_1,branch_label_2)));
        pos = -1;
      }

      
    }
  }
  /*
  std::map<std::string, std::pair<std::string,std::string> >::iterator ii;
  for(ii = branches_per_block.begin(); ii!=branches_per_block.end(); ++ii)
  {
    cout << (*ii).first << ": " << ((*ii).second).first << "&" << ((*ii).second).second << std::endl;
  }
*/
  cerr << "\nBranches read";
  iFile.close();
  return 0;
}
//find out which basic block computes the arguments for which assertion
void DynamicSymExec::assertion_computation(){

  std::map<std::string, std::pair<std::string,std::string> >::iterator ii;
  std::map<std::string, std::string>::iterator btami;//block_to_assertions_map;//<blocklabel,assert>

  for(ii = branches_per_block.begin(); ii!=branches_per_block.end(); ++ii)
  {
    int branch_count = 0;
//    cout << "\n" <<(*ii).first << ": " << ((*ii).second).first << "&" << ((*ii).second).second ;//<< std::endl;
    std::string thisblock = (*ii).first;
    std::string child1 = ((*ii).second).first;
    std::string child2 = ((*ii).second).second;

    if(child1.compare("NOLABEL")!=0)
      branch_count++;
    if(child2.compare("NOLABEL")!=0)
      branch_count++;

    if(branch_count>1){
         btami = block_to_assertions_map.find(child1); 
         if(btami != block_to_assertions_map.end()){
            std::string tested_assertion = (*btami).second;
            assertion_marker.insert(std::make_pair(thisblock,tested_assertion));
         }
    }

  }
}

void DynamicSymExec::processedCDG(){


  /*std::map<std::string, std::pair<std::string,std::string> >::iterator ii;
  for(ii = branches_per_block.begin(); ii!=branches_per_block.end(); ++ii)
  {
    cout << (*ii).first << ": " << ((*ii).second).first << "&" << ((*ii).second).second << std::endl;
  }*/

  std::map<std::string, std::pair<int,std::pair<int, int> > > ::iterator di = assert_count.begin();
  for(di = assert_count.begin();di!=assert_count.end();++di){

    cout << (*di).first <<":" <<(((*di).second).second).first << ":"<< (((*di).second).second).second << "\n";
  }
}


//Reads how many unique assertions in the block
//Also intializes the tracker for the number of assertions touched
int DynamicSymExec::readUnique(std::string task) {

  std::string postfileread = ".ll";
  std::string temp = task.substr(0,task.length()-3);
  std::string input_file = temp.append(postfileread);
  const char* input_filename = input_file.c_str();

  std::ifstream iFile(input_filename);
  if(!iFile.is_open()){
    cerr<<"Could not open "<<input_filename<< std::endl;
    exit(0);
  }

  uniqueAssertionsRead.clear();
  assertions_touched.clear();

  std::string line;
  std::string pconstant("= private constant");
  std::string cstart("C__");

  int pos1,pos2;
  std::string last_label;
  while(getline(iFile, line)) {
    pos1 = line.find(cstart);
    pos2 = line.find(pconstant);
    if(pos1 != -1 && pos2 !=-1){
        //cout << "\n" << line;
        std::string slabel = line.substr(2,pos2-3);
        int pos3 = line.find("align 1");
        if(pos3<=0){
            cout << "\nerror";
            exit(0);
        }
        std::string assert = line.substr(pos1,(pos3-pos1-6));
        uniqueAssertionsRead.insert(std::make_pair(slabel,assert));//<label for the assertion in the .ll file , assertion string>
        assertions_touched.insert(std::make_pair(assert,0));
        //cout << "\n" << slabel << "," << assert;
        ua_count++;
    }
  }
  iFile.close();
  cerr << "\nUnique assertions read " << ua_count;
  return 0;
}
//assertions mapped to the basic block
//in the <filename>.ll file, basic blocks do not simply contain the assertion string
//instead it contains a unique identifiers, which has been declared in the begining of the file
//the identifier to assertion mapping has been done in function readUnique 
int DynamicSymExec::map_block_to_assertion(std::string task) {

  std::string postfileread = ".ll";
  std::string postfilewrite = "_record";
  std::string temp = task.substr(0,task.length()-3);
  std::string input_file = temp.append(postfileread);
  const char* input_filename = input_file.c_str();

  std::ifstream iFile(input_filename);
  if(!iFile.is_open()){
    cerr<<"Could not open "<<input_filename<< std::endl;
    exit(0);
  }

  std::map<std::string, std::string>::iterator uari;// uniqueAssertionsRead;
  std::map<std::string, int>::iterator btami; // block_to_assertions_map;
  std::string line;
  std::string assertCalls("__assert_fail");  //this symbols marks the place, where klee_assert(...) is called
  std::string entry("entry:");
  std::string block("bb");
  std::string colon(":");

  int start = 0;

  int total = 0;
  int pos;
  int total_mapped = 0;
  std::string last_label;
  std::string assertlabel;

  while(getline(iFile, line)) {
    if(start){
      pos = line.find(colon);
      if(pos!=-1 && start){
        if(total){
          //cout << "\n add " << last_label << " "  << assertlabel;
          block_to_assertions_map.insert(std::make_pair(last_label,assertlabel));
        }
        else{
          //  cout << "\n add " << last_label << " "  << "NOLABEL";
          block_to_assertions_map.insert(std::make_pair(last_label,"NOLABEL"));
        }
        last_label = line.substr(0,pos);
        total_mapped += total;
        total = 0;
      }
      //find how many there are
      for(uari = uniqueAssertionsRead.begin(); uari != uniqueAssertionsRead.end(); ++uari){
        int found = line.find((*uari).first);
        if(found!=-1){
          total = 1;
          assertlabel = (*uari).second;
          //cout << "\t" << assertlabel;
        }
      }
    }
    if(line.find(entry)!=std::string::npos){
      start = 1;
      last_label = "entry";
    }
  }

  iFile.close();
  cerr << "\nAssertions mapped : " << total_mapped;
  return 0;
}
int DynamicSymExec::makeRecord(std::string task) {

  //std::string postfileread = "__converted__KLEE.ll";
  std::string postfileread = ".ll";
  std::string postfilewrite = "_record";
  std::string temp = task.substr(0,task.length()-3);
  std::string input_file = temp.append(postfileread);
  const char* input_filename = input_file.c_str();

  std::ifstream iFile(input_filename);
  if(!iFile.is_open()){
    cerr<<"Could not open "<<input_filename<< std::endl;
    exit(0);
  }

  std::string line;
  std::string assertCalls("__assert_fail");  //this symbols marks the place, where klee_assert(...) is called
  std::string entry("entry:");
  std::string block("bb");
  std::string colon(":");

  int assert_len = assertCalls.length();
  int start = 0;

  int total = 0;
  int pos;
  std::string last_label;
  while(getline(iFile, line)) {
    if(start){
      pos = line.find(colon);
      if(pos!=-1 && start){
        assertions_per_block.insert(std::make_pair(last_label,total));
        assert_count.insert(std::make_pair(last_label,std::make_pair(total,std::make_pair(0,0))));
        last_label = line.substr(0,pos);
        total = 0;
      }
      pos = line.find(assertCalls);
      while(pos!=-1){
        total++;
        total_assertions++;
        pos = line.find(assertCalls,pos+assert_len);
      }
    }
    if(line.find(entry)!=std::string::npos){
      start = 1;
      last_label = "entry";
    }
  }

  /*std::map<std::string, std::pair<int,std::pair<int, int> > > ::iterator ii = assert_count.begin();
  cout << (*ii).first << ((*ii).second).first;
  ((*ii).second).first = 2;
*/
 // cout << (*ii).first << ((*ii).second).first;
  /*for(ii = assert_count.begin();ii!=assert_count.end();++ii){

    cout << (*ii).first << ((*ii).second).first;
  }*/
  iFile.close();
  //cerr << "\nTotal assertions read " << (total_assertions-1);
  return 0;
}
int DynamicSymExec::read_assertions(std::string task){
  //TODO : presently the <filename>.ll is generated from the <filename>.bc, using script (rundis)
  //It can be integrated in the program, as an LLVM pass, just for elegance 
  
  //This functions make the actual record
  makeRecord(task);

  /*  for( std::map<std::string, int>::iterator ii=assertions_per_block.begin(); ii!=assertions_per_block.end(); ++ii)
      {
      std::cout << (*ii).first << ": " << (*ii).second << std::endl;
      }*/
  return 0;
}

void DynamicSymExec::report_error(int er){

  cout << "\n error happened [code : %d]\n" << er ;
  exit(0);
}
//print the basic block execution trace
void DynamicSymExec::PrintBBTrace() 
{
  /*  unsigned TraceSize = BBList.size();

      for (unsigned TBBI = 0; TBBI < TraceSize; TBBI++) {
      const char* BBName = (BBList[TBBI]->getName()).data();
      cerr << prefix << " " << BBName << "\n";
      }
   */
  for (unsigned TI = 0; TI < ExecTrace->size(); TI++) {
    bool BBOut = (*ExecTrace)[TI].second;
    if(1)cerr << BBOut;

  }
}

void DynamicSymExec::display_CDG(){

  controlGraph_t* itr = CDG;
  cout << "\nControl Dependence Graph\n";
  while(itr!=NULL){
    BasicBlock *bblock = itr->bblock;
    cout << "\n" <<(bblock->getName()).data();
    BasicBlockSet_t *dSet = itr->dSet;
    cout << "[ ";
    while(dSet!=NULL){
      BasicBlock *block= dSet->bblock;
      cout << " " <<(block->getName()).data();
      dSet = dSet->next;
    }
    cout << " ]";


    itr = itr->next;
  }
  return;
}

DynamicSymExec::controlGraph_t* DynamicSymExec::search(BasicBlock *block){

  controlGraph_t* Itr = CDG;
  while(Itr!=NULL){
    if(Itr->bblock==block){
      return Itr;
    }
    Itr = Itr->next;
  }

  return Itr;
}


//replay a path file using KLEE
int DynamicSymExec::runKLEE() {
  cerr << "\nRun KLEE";
  std::string KLEECommand(EXE_TYPE);
  KLEECommand += SMALLSKIP;
  KLEECommand += REPLAY_PATH;
  KLEECommand += SMALLSKIP;
  KLEECommand += KLEEPathFileName;
 // KLEECommand += SMALLSKIP;
 // KLEECommand += EMIT_ALL_ERRORS;
 // KLEECommand += SMALLSKIP;
  KLEECommand += KLEE_OTHER_OPTIONS;
  KLEECommand += SMALLSKIP;
  //the actual LLVM bitcode file to replay
  KLEECommand += KLEEFileName;

  //call KLEE now for building the path condition
  cerr << "\ncalling KLEE with the following command.....\n";
  cerr << KLEECommand.c_str() << "\n";
  unsigned retcode = system(KLEECommand.c_str());

  cerr << "\nKLEE has returned " << retcode << ".....\n";

  return retcode;
}

//initialize KLEE path file
void DynamicSymExec::InitKLEEFileName() {
  //initialize the klee path file and Klee bitcode file
  KLEEPathFileName.clear();
  KLEEFileName.clear();
  KLEEPathFileName += KLEE_DIR;
  KLEEPathFileName += EXE_TYPE;
  KLEEPathFileName += ".main.";
  KLEEPathFileName += KLEE_PATH_FILE_EXT;		
  //get the bitcode filename to be used for replaying by KLEE
  const char* TestF = GetTestFileName();	
  //KLEEFileName += OutFile;
  KLEEFileName += TestF;
}

//deviate from the currently executed path by negating the last 
//branch of a partial path condition

int DynamicSymExec::getAffectedAsserts(std::vector<bool> blvector){

  std::map<std::string, std::pair<int,std::pair<int, int> > > ::iterator acbi;//assert_count.begin();
  int retAssert = 0;
  if(blvector.size()>(*ExecTrace).size()){
    cout << "vector bigger than trace";
    exit(0);

  }else{
    int inversionIndex = blvector.size()-1;
    bool BBOut = (*ExecTrace)[inversionIndex].second;
    BasicBlock* BB = (*ExecTrace)[inversionIndex].first;
    //cerr << "\nGot Basic Block " << BB->getName().data() << "presently " << BBOut <<"\n";
    std::string searchLabel = BB->getName().data();
    std::string child_label;

    acbi = assert_count.find(searchLabel);

    if(!BBOut){
      retAssert = (((*acbi).second).second).first;
    }else{
      retAssert = (((*acbi).second).second).second;
    }
    /////////////////////////////
#if 0
    incrementTouched(BB);


    controlGraph_t* temp = search(BB);
    BasicBlockSet_t *dSet = temp->dSet;
    //  std::vector<string> blockList;
    std::vector<BasicBlock* > blockList;
    blockList.clear();
    while(dSet!=NULL){
      BasicBlock *block= dSet->bblock;
//      cout << "\n" <<(block->getName()).data();
      //std::string chlabel = (block->getName()).data();
      blockList.push_back(block);
      dSet = dSet->next;
    }

    elements = blockList.size();
    //cout << "\n at this point " << blockList.size();
    while(!blockList.empty()){
      controlGraph_t* tempi = search(blockList.back());
      blockList.pop_back();
      incrementTouched(tempi->bblock);

      //assume tempi is not null
      BasicBlockSet_t *dSeti = tempi->dSet;
      while(dSeti!=NULL){
        BasicBlock *blocki= dSeti->bblock;
  //      cout << " " <<(blocki->getName()).data();
        //std::string chlabeli = (block->getName()).data();
        blockList.push_back(blocki);
        elements++;
        dSeti = dSeti->next;
      }
    }
#endif
    //exit(0);
  }
  //cout << "\n total elements : " << elements;

  //cerr << "\t count " << retAssert << " \n " ;
  return retAssert;
}
void DynamicSymExec::uniqueTouched(){

  std::map<std::string, int>:: iterator ati;// assertions_touched;
  std::map<std::string, std::string>:: iterator ami;// assertion_marker;
  std::map<std::string, std::string>::iterator btami;// block_to_assertions_map;//<blocklabel,assert>
  std::string KleeTOUCHED("assertions_touched");
  std::ifstream KleeAT;
  cerr << "\nOpening file : " + KleeTOUCHED;

  //open the KLEE path file (modified in replay mode)
  KleeAT.open(KleeTOUCHED.c_str(), std::ios::in);

  //sanity check file descriptor
  if (!KleeAT.is_open())
    cerr << "KLEE assert file " << KleeTOUCHED.c_str() << " cannot be opened.....\n";
  
  std::string line;
  std::string assert_string;
  std::string lastline = "NOLINE";
  while (KleeAT.good()) {
    getline(KleeAT,line);
    if(line.compare(lastline)!=0){ 
      lastline = line;
      //  std::string bblabel = BB->getName().data();
      ami = assertion_marker.find(line); 
      if(ami!=assertion_marker.end())
        assert_string = (*ami).second;
        ati = assertions_touched.find(assert_string);
        if(ati != assertions_touched.end()){
          if((*ati).second == 0){
            (*ati).second = 1;
            assertions_tested++;
          }
        }

    }
  }
  KleeAT.close();
  //cout << "\n touched " << total_touched;
  system("rm assertions_touched");

}

void DynamicSymExec::dumpViolatedBlocks(std::string line){

  std::string conF = "C__";
  int position = line.find(conF);
  if(position != std::string::npos){
      std::string vBlock = line.substr(position+3,6);
      test_file << "\n" << vBlock;
      cout << "\n" << vBlock;
      position = line.find(conF,position+9);
      while(position != std::string::npos){
        vBlock = line.substr(position+3,6);
        test_file << "\n" << vBlock;
        cout << "\n" << vBlock;
        position = line.find(conF,position+9);
      }
  }
  test_file << "\n---";

}

void DynamicSymExec::uniqueViolations(){

  cerr << "\nKeeping track of unique assertions violated";
  std::string KleeASSERT(KLEE_TEST_DIR);
  KleeASSERT += KLEE_ASSERT_FAIL;
  std::ifstream KleeAfd;
  cerr << "\nOpening file : " + KleeASSERT;

  //open the KLEE path file (modified in replay mode)
  KleeAfd.open(KleeASSERT.c_str(), std::ios::in);

  //sanity check file descriptor
  if (!KleeAfd.is_open())
    cerr << "KLEE assert file " << KleeASSERT.c_str() << " cannot be opened.....\n";

  std::vector<std::string>:: iterator uvi;//uniqueViolatedTracker;
  std::vector<std::string>:: iterator itr;//uniqueViolatedTracker;
  std::map<std::string, int>:: iterator ati;// assertions_touched;

  int once = 0;
  int i;
  int uniqueViolatedAsserts_count = 0;
  std::string line;
  std::string AFail = "ASSERTION FAIL:";
  while (KleeAfd.good()) {
    getline(KleeAfd,line);
    int position = line.find(AFail);
    if(position != std::string::npos){
      std::string nAssert = line.substr(position+16,line.length());
      uvi = std::find(uniqueViolatedTracker.begin(), uniqueViolatedTracker.end(), nAssert);

      if(uvi == uniqueViolatedTracker.end()){ 
      if(!once){
        test_file << "\n#=@========================\n";
        for(i=0;i<256;i++){
          if(bigString[i]=='\0')
            break;
          else if(bigString[i]=='#')
            test_file << "\n";
          else
            test_file << bigString[i];
        }
        once = 1;
      }
        uniqueViolatedTracker.push_back(nAssert);
        uniqueViolatedAsserts_count++;
        //test_file << "\n"<<line;
        dumpViolatedBlocks(line);
/*
        ati = assertions_touched.find(nAssert);
        if(ati != assertions_touched.end()){
          if((*ati).second == 0){
            (*ati).second = 1;
            total_touched++;
          }
        }
        */
      }else{ 
        //  cout << "\nexsits";
      }

    }
  }
  total_violated += uniqueViolatedAsserts_count;
  //time_file << ((total_violated*100/ua_count)) << "\n"; 
  time_file << total_violated << "\n"; 

  KleeAfd.close();
}

int DynamicSymExec::violatedAsserts(){

  cerr << "\nChecking how many assertions got violated, in last run of KLEE";
  std::string KleePATH(KLEE_PATH_FILENAME);
  KleePATH += PATH_EXT;
  std::ifstream KleePfd;

  //open the KLEE path file (modified in replay mode)
  KleePfd.open(KleePATH.c_str(), std::ios::in);

  //sanity check file descriptor
  if (!KleePfd.is_open())
    cerr << "KLEE modified path file " << KleePATH.c_str() << " cannot be opened.....\n";

  std::map<std::string, std::pair<int,bool> > :: iterator vi;//violatedAssertsTracker;

  violatedAssertsTracker.clear();
  int violatedAsserts_count = 0;

  bool Bout, BSym, BAssert;
  int index = 0;
  int max_size = (*ExecTrace).size();
  //cout << "\n max size " << max_size ;
  std::string lastBranch = "nobranch";
  bool lastDir;
  while(index<max_size){
    KleePfd >> Bout;
    KleePfd >> BSym;
    KleePfd >> BAssert;

    if(BAssert && lastBranch.compare("nobranch")!=0){
      if((violatedAssertsTracker.insert(std::make_pair(lastBranch,std::make_pair(1,lastDir))).second)==false){
        vi = violatedAssertsTracker.find(lastBranch);
        ((*vi).second).first = ((*vi).second).first + 1;
        //cout << "\n please test if it comes over here";
        //exit(0);
      }

    }else{
      BasicBlock* BB = (*ExecTrace)[index].first;
      lastBranch = BB->getName().data();
      lastDir = (*ExecTrace)[index].second;
      //  cout << "\nGot Basic Block " << BB->getName().data()<< "presently " << BBOut ;//<<"\n";

      if(BAssert)
        violatedAsserts_count++;

    }

    index++;
  }
  //cerr << "\nViolated assertion on this path: " << violatedAsserts_count;
  KleePfd.close();
  return violatedAsserts_count;
  //  exit(0);
}


void DynamicSymExec::adjustAssertionVectors(){

  std::map<std::string, std::pair<int,std::pair<int, int> > > :: iterator aci;//assert_count;

  //processedCDG();

  cerr << "\nAdjusting the assertion tracker accordingly...";

  std::map<std::string, std::pair<int,bool> > :: iterator vi;//violatedAssertsTracker;

  for(vi = violatedAssertsTracker.begin(); vi != violatedAssertsTracker.end(); ++vi){
    std::string label = (*vi).first;
    aci = assert_count.find(label);
    if(aci != assert_count.end()){
      //check
      if(((*aci).second).first>0)
        ((*aci).second).first = ((*aci).second).first - 1;
      bool outcome = ((*vi).second).second;
      if(outcome){
        if((((*aci).second).second).first>0)
          (((*aci).second).second).first--;
      }else{
        if((((*aci).second).second).second>0)
          (((*aci).second).second).second--;
      }
    }
  }
  //////////////////////////

  std::map<std::string, std::pair<int,std::pair<int, int> > > ::iterator ii = assert_count.begin();
  std::map<std::string, std::pair<std::string,std::string> >  ::iterator bi;//branches_per_block;


  for(ii = assert_count.begin();ii!=assert_count.end();++ii){


    //  cout <<"\n" <<(*ii).first <<":" <<(((*ii).second).second).first << ":"<< (((*ii).second).second).second ;//<< "\n";
    std::string label = (*ii).first;
    bi = branches_per_block.find(label);
    std::string child_label1 = ((*bi).second).first;   
    std::string child_label2 = ((*bi).second).second;

    vi = violatedAssertsTracker.find(child_label1);
    if(vi!=violatedAssertsTracker.end()){
      if((((*ii).second).second).first > 0){
        //          cout << "\nlabel " << label << " child 1" << child_label1 <<" " << (((*ii).second).second).first << " reduce by " << ((*vi).second).first;
        (((*ii).second).second).first = (((*ii).second).second).first - ((*vi).second).first;
      }
    }

    vi = violatedAssertsTracker.find(child_label2);
    if(vi!=violatedAssertsTracker.end()){
      if((((*ii).second).second).second > 0){
        //        cout << "\nlabel " << label << " child 2" << child_label2 <<" " << (((*ii).second).second).second << " reduce by " << ((*vi).second).first;
        (((*ii).second).second).second = (((*ii).second).second).second - ((*vi).second).first;
      }

    }
  }
  cerr << "\nChange complete, good to go for next round";
  //exit(0);
}

float TimeLimit ;//= 300.0;

//Attn: Abhijeet (****THIS FUNCTION IS THE KEY FOR YOU****)
void DynamicSymExec::TryToDeviateFromCurrentPath()
{ 
  cerr << "\nTrying to deviate from current path";
  Function *F = M->getFunction("main");
  int iteration = 0;

  //sanity check
  assert(F && "you are a SATAN.....no \"main\" function found");

  //***** THE CRUNCH LOOP ***** go through all the partial path 
  //conditions......
  cerr << "\n size of unexpanded " << UnexpandedPathConditionTracker.size();
  while (!UnexpandedPathConditionTracker.empty()  && TimeLimit <= 300) {


    //Check the last one without considering any priority
    /*PCTrackerType::iterator II = UnexpandedPathConditionTracker.end();
    --II;
    Expr PPCond = II->second;*/

    PCTrackerType::iterator ppci ;
    PCTrackerType::iterator ppc_max;
    int max_assert = -1;

    //use the path condition, which could lead to maximum number of unchecked assertions
    for(ppci = UnexpandedPathConditionTracker.begin(); ppci!= UnexpandedPathConditionTracker.end();++ppci){
      std::vector<bool> blvector = ppci->first;
      //get the vector, which can lead to maximum number of unchecked assertions
      int affected_asserts = getAffectedAsserts(blvector);  
      if(affected_asserts>max_assert){
        ppc_max = ppci;
        max_assert = affected_asserts;
      }
    }


    Expr PPCond = ppc_max->second;

    //create the validity checker to solve a partial path formula
    VC vc = vc_createValidityChecker();
    //vc_printExpr(vc, PPCond);
    vc_Destroy(vc);						


    //Attn: Abhijeet (check whether any assertion is reachable)
    //bool BlockPath = CheckReachabilityofAssertion(...Use CDG...);

    //erase the partial path from tracker database.....
    //cerr << "\nRemoving partial path condtion from unexplored set";
    UnexpandedPathConditionTracker.erase(ppc_max);
    //UnexpandedPathConditionTracker.erase(II);


    //get all the concrete values of the symbolic inputs
    uint32_t NSym = GetAndSetAllConcreteValues(PPCond, F);

    //path condition is infeasible, so continue;
    if (NSym == 0){
      cerr << "\nNot a single symbolic variable, skip this one";
      continue;			
    }
    ++iteration;

    //cerr << "\nIteration count in explore = " << iteration ;

    //*****CAUTION***** it is extremely important that you create the JIT here 
    //and then run the function. As our module gets on modified the LLVM JIT 
    //does not track the module changes and may give spurious results if EE 
    //is not properly initialized
    EngineBuilder EBuilder(M);
    EBuilder.setEngineKind(EngineKind::Interpreter);
    ExecutionEngine* EE = EBuilder.create();

    //run the program using LLVM interpreter
    std::vector<GenericValue> noargs;
    //initialize the BB trace collector
    EE->InitBBTraceCollector();
    //initialize the execution trace collector
    EE->InitExecTraceCollector();

    GenericValue OutputVal = EE->runFunction(F, noargs);

    //now destroy the concrete value setup.....concrete values were 
    //stored only to run the program. we can now delete all of the 
    //additional store instructions which were used to setup the 
    //concrete values of inputs
    DestroyAllConcreteValues(F, NSym);

    //collect basic block trace
    cerr << "\nGenerating basic block trace.....";
    std::vector<BasicBlock *> BBTrace = EE->getBBTraceCollector();

#ifdef _DEBUG_PP
    //debug trace
    PrintBBList(BBTrace, "[EX]");
#endif

    //collect the execution trace with concrete values --- this will 
    //be used for dynamic symbolic execution of the respective trace
    TraceType &ExecTrace = EE->getExecTraceCollector();

    //set the new basic block and execution traces for the symbolic engine
    SetBBTrace(&BBTrace);
    SetExeTrace(&ExecTrace);
    //PrintBBTrace();
    //cerr << "\n";
    //for debug purpose
    //PrintExecTrace();

    //build the new branch outcome vector to be replayed by KLEE
    BuildBranchOutcomeList();

    //run KLEE to get the path condition
    int ret = runKLEE();

    //check KLEE output, be careful here
    if (ret) {
      cerr << "[ERROR] KLEE returned bad.....currently ignoring the error.....\n";
      continue;	
    }

    struct timeval time;
    double time_elapsed;
    gettimeofday(&time, NULL);
    time_elapsed = (time.tv_sec - analysis_start_time.tv_sec)+((time.tv_usec - analysis_start_time.tv_usec)/1000000.0);
    time_file << time_elapsed << "\t"; 
    TimeLimit = time_elapsed;
    cerr << "\nRemaining budget : "<< (300 - TimeLimit);

    uniqueViolations();
    uniqueTouched();

    //touch_file << time_elapsed << "\t" << ((assertions_tested*100)/ua_count) << "\n";
    cerr << "\nviolated : " << total_violated ;//<< "touched " << assertions_touched;
    cerr << "touched " << assertions_tested;
    if(assertions_tested < total_violated)
      assertions_tested = total_violated;

    touch_file << time_elapsed << "\t" << assertions_tested << "\n";
    if(violatedAsserts()!=0)
      adjustAssertionVectors();

    //build the new partial path conditions
    BuildPartialPathConditions();

    //cleanup A potential memory leak.....however, cannot delete 
    //due to some dangling pointers in market. Investigate the 
    //issue further
    //delete EE;

  }
}

//remove the code which were used to set up the concrete values of input variables
void DynamicSymExec::DestroyAllConcreteValues(Function* F, uint32_t NSym)
{
  //entry block of the "main" function
  BasicBlock* Entry = &(F->getEntryBlock());
  unsigned SymI = 0;

  for (BasicBlock::iterator II = Entry->begin(); SymI < NSym; SymI++) {
    Instruction* Inst = dyn_cast<Instruction>(II);
    ++II;
    //remove the concrete value setup from the parent basic block
    Inst->removeFromParent();
  }
}

//inject code to set concrete values of input variable "Input"
//current support: arbitrary width integer or an array of the same 
void DynamicSymExec::SetConcreteValue(Value* Input, uint64_t InputValue, Function* F, 
    long* ArrayVector, long InputSize, unsigned* NSym)
{
  //entry block of the "main" function		
  BasicBlock* Entry = &(F->getEntryBlock());		

  //*****CAUTION***** sudiptac: LLVM is strongly typed
  //we currently assume that the inputs are global pointers and the concrete values 
  //to all the inputs are assigned by checking the type of "first" contained type 
  //of global input value
  if (Input->getType()->getContainedType(0)->getTypeID() == Type::ArrayTyID) {
    cerr << "Array input found\n";
    BasicBlock::iterator II = Entry->begin();
    //get the array first element pointer
    Constant *ConsValue = static_cast<Constant *>(Input);
    //zero (0) value
    Value* Zero = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0);
    //get the type and size of the array element
    const ArrayType* ATy = dyn_cast<ArrayType>(Input->getType()->getContainedType(0));
    unsigned ElSize = ATy->getElementType()->getPrimitiveSizeInBits() / SIZE_OF_BYTE;
    //construct the element-by-element value and store it into the array
    for (unsigned StepI = 0; StepI < InputSize; StepI += ElSize) {
      long IVal = 0;
      for (unsigned EI = StepI; EI < StepI + ElSize; EI++) {
        if (EI < InputSize) {
          //carefully construct the array element value, depends on the 
          //element size (ElSize)
          long IByte = ArrayVector[EI] << ((EI % ElSize) * SIZE_OF_BYTE);
          IVal |= IByte;
        }
      }
      //zero value is default, so we do not reassign zero.....skip
      if (!IVal) continue;
      //get the pointer specific to the array element need to be assigned	
      Value* Idx = ConstantInt::get(Type::getInt32Ty(getGlobalContext()), StepI / ElSize);
      Value *gep_params[] = {Zero, Idx};
      Constant *ArrPtr = ConstantExpr::getInBoundsGetElementPtr(ConsValue, gep_params, 2);
      Value* CValue = ConstantInt::get(ATy->getElementType(), IVal, true);
      //store the array element with the value got from solver
      new StoreInst(CValue, ArrPtr, II);
      (*NSym)++;
    }
  } else if (Input->getType()->getContainedType(0)->getTypeID() == Type::IntegerTyID) { 
    //simpler case when the input is scalar
    Value* CValue = ConstantInt::get(Input->getType()->getContainedType(0), InputValue, true);
    BasicBlock::iterator II = Entry->begin();
    //create a store instruction to set the value.....the store instruction 
    //must be before any of the original code
    new StoreInst(CValue, Input, II);
    (*NSym)++;
  } else {
    assert(0 && "Ouchhh.....input type not supported for building cache spectra");
  }
}

//fetch the concrete values produced by the SMT solver
uint32_t DynamicSymExec::GetAndSetAllConcreteValues(Expr PPCond, Function* F) 
{
  unsigned NSym = 0;		
  //for array input, holds the array element values and index values, respectively
  long* ArrayVector = NULL;
  bool* Index = NULL;

  //create the validity checker to solve a partial path formula
  VC vc = vc_createValidityChecker();
  //vc_setFlags(vc, 'v');
  //vc_setFlags(vc, 's');
  //vc_setFlags(vc, 'n');

  //check the validity of the partial path condition
  cout << "\n@@@";
  cout << "\n===================================================\n";
  cerr << "\n checking the validity of the following partial path condition.....\n";
  vc_printExpr(vc, PPCond);
  Expr NotPPCond = vc_notExpr(vc, PPCond);
  vc_push(vc);
  int ret = vc_query(vc, NotPPCond);

  if (ret == 1) {
    cerr << "\npath condition is infeasible, returning zero to skip.....\n";
    vc_pop(vc);
    vc_Destroy(vc);						
    return 0;
  }	
  else if (ret == 2) {
    cerr << "\nsome error occured in the SMT solver.....\n";
    vc_pop(vc);
    vc_Destroy(vc);						
    return 0;
  }	
  cerr << "\na feasible path condition found.....\n";
  cerr << "\nsymbolic input vector size : "<<SymbolicInputVector.size() << "\n";

  int i;
  for(i=0;i<256;i++)
    bigString[i] = '\0';

  //walk through the symbolic inputs and obtain their concrete values
  for (unsigned SymI = 0; SymI < SymbolicInputVector.size(); SymI++) {
    const char* Name = SymbolicInputVector[SymI]->getName().data();
    bool Found = false;

    //call to STP API to get counterexample value
    long NameSize = vc_getValueFromCounterExample(vc, Name, &Found, \
        &ArrayVector, &Index);

    //solver output debug
    cerr << "counter example size = " << NameSize << "\n";
    for (unsigned i = 0; i < NameSize; i++)
      cerr << ArrayVector[i] << " ";
    cerr << "\n";
    for (unsigned i = 0; i < NameSize; i++)
      cerr << Index[i] << " ";
    cerr << "\n";

    if (Found) {
      //if the input is not an array, the following computed "Value" will 
      //be used as a concrete value to the respective scalar input variable
      long Value = 0;
      for (int size = NameSize - 1; size >= 0; size--) {
        Value <<= SIZE_OF_BYTE;
        Value |= ArrayVector[size]; 
      }
      //cerr << "Input " << Name << " = " << Value << "\n";
      cout << "Input " << Name << " = " << Value << "\n";
      //test_file << "Input " << Name << " = " << Value << "\n";
      char tempS[80];
      sprintf(tempS,"%s=%ld#",Name,Value);
      strcat(bigString,tempS);
      //set the value of symbolic input
      SetConcreteValue(SymbolicInputVector[SymI], Value, F, ArrayVector, NameSize, &NSym);
    }	
    //free the counter example value holder, as the memory might have 
    //been allocated in the STP library
    if (ArrayVector) {
      delete ArrayVector;
      ArrayVector = NULL;
    }
    if(Index) {
      delete Index;
      Index = NULL;
    }
  }

  //sudiptac: number of generated concrete values
  cerr << "Number of concrete values set = " << NSym << "\n";

  vc_pop(vc);
  vc_Destroy(vc);						

//  printf("\n stopped here");
//  exit(0);
  return NSym;
}

DynamicSymExec::controlGraph_t* DynamicSymExec::searchByLabel(std::string label){

  controlGraph_t* Itr = CDG;
  while(Itr!=NULL){
    BasicBlock *bblock = Itr->bblock;
    std::string this_label = (bblock->getName()).data();
    if(label.compare(this_label)==0){
      return Itr;
    }
    Itr = Itr->next;
  }

  return Itr;
}

void DynamicSymExec::preProcess_step2(){

  std::map<std::string, std::pair<int,std::pair<int, int> > > ::iterator ii = assert_count.begin();
  std::map<std::string, std::pair<int,std::pair<int, int> > > ::iterator ai = assert_count.begin();
  std::map<std::string, std::pair<std::string,std::string> >  ::iterator bi;//branches_per_block;
  std::map<std::string, int> ::iterator apb;
  int assert1, assert2;
  int assert1_temp, assert2_temp;

  for(ii = assert_count.begin();ii!=assert_count.end();++ii){

    (((*ii).second).second).first = 0;
    (((*ii).second).second).second = 0;

    std::string label = (*ii).first;
    bi = branches_per_block.find(label);
    if(bi != branches_per_block.end()){
      std::string child_label1 = ((*bi).second).first;   
      std::string child_label2 = ((*bi).second).second;

      apb = assertions_per_block.find(child_label1);
      if(apb!=assertions_per_block.end()){
        assert1 = (*apb).second;
      }
      apb = assertions_per_block.find(child_label2);
      if(apb!=assertions_per_block.end()){
        assert2 = (*apb).second;
      }

      ai = assert_count.find(child_label1);
      if(ai == assert_count.end())
        assert1_temp = 0;
      else
        assert1_temp = ((*ii).second).first;

      ai = assert_count.find(child_label2);
      if(ai == assert_count.end())
        assert2_temp = 0;
      else
        assert2_temp = ((*ii).second).first;


      controlGraph_t *temp = searchByLabel(label);
      if(temp!=NULL){
        BasicBlockSet_t *dSet = temp->dSet;
        while(dSet!=NULL){
          BasicBlock *block= dSet->bblock;
          std::string temp_string = (block->getName()).data();
          if(temp_string.compare(child_label1)==0){
            assert1 = assert1_temp; 
          }
          if(temp_string.compare(child_label2)==0){
            assert2 = assert2_temp; 
          }
          dSet = dSet->next;
        }
      }

      (((*ii).second).second).first = assert1;
      (((*ii).second).second).second = assert2;
      //cout << "\nblock : "<< (*ii).first << " l " << assert1 << "r" << assert2 ;
    }
  }
}

int DynamicSymExec::preProcess_step1(){

  std::map<std::string, std::pair<int,std::pair<int, int> > > ::iterator ii = assert_count.begin();
  std::map<std::string, std::pair<int,std::pair<int, int> > > ::iterator childI;
  std::map<std::string, int> :: iterator omi;
  //  cout << (*ii).first << ((*ii).second).first;
  //  ((*ii).second).first = 2;

  int change = 0;

  for(ii = assert_count.begin();ii!=assert_count.end();++ii){

    std::string label = (*ii).first;

    //  if(print_flag)cout << "\nblock : "<< (*ii).first << " total : " <<((*ii).second).first<<"\n";
    omi = assertions_per_block.find(label);
    int org_val = (*omi).second;

    controlGraph_t *temp = searchByLabel(label);
    if(temp!=NULL){

      BasicBlockSet_t *dSet = temp->dSet;
      //cout << "[ ";
      while(dSet!=NULL){
        BasicBlock *block= dSet->bblock;
        // cout << " " <<(block->getName()).data();
        std::string child = (block->getName()).data();
        childI = assert_count.find(child);
        int child_assert_count;
        if((child.compare(label))==0){
          child_assert_count = (*omi).second;
        }
        else{
          child_assert_count = ((*childI).second).first;
        }
        //if(print_flag)cout << "\tchild" << (*childI).first << ((*childI).second).first;
        org_val += child_assert_count;

        dSet = dSet->next;
      }
      if(org_val > ((*ii).second).first){
        ((*ii).second).first = org_val;
        change = 1;
        //            cout << "\nchanged due "  ;
        //            cout << "after block : "<< (*ii).first << " total : " <<((*ii).second).first;
      }
    }
  }
  return change;
}


void DynamicSymExec::preProcess(){
  int change = 1;
  while(change){
    //count the total number of unchecked assertions, per basic block, untill fix-point in achieved
    change = preProcess_step1();
  }
  //assign the number of unchecked assertions for true and false branches, per basic block
  preProcess_step2();

  //now we are ready for path exploration
  cerr << "\nPre-processing complete";
}
//MAIN
//this is the top level call of dynamic path program exploration
//(1) we explore path programs instead of paths
//(2) however, we guarantee that there must be at least one 
//feasible path in the explored path program
//
//Technically, the exploration follows the process of SAGE, 
//however, before exploring a path, it is checked whether 
//the path is already reasoned by some path program before. 
//Such a check is done by adding blocking clauses in the 
//Path Program SAT encoder database
void DynamicSymExec::runDynamicPathProgramExploration(const char* inputF)
{
  struct timeval time;
  system("rm record.time"); 
  time_file.open ("record.time", std::ios::out);
  if(!time_file.is_open()){
    cerr << "\nFile for recording time could not be opened, exiting";
    exit(0);
  }
  system("rm touch.time"); 
  touch_file.open ("touch.time", std::ios::out);
  if(!touch_file.is_open()){
    cerr << "\nFile for recording time could not be opened, exiting";
    exit(0);
  }
  test_file.open ("testsuite", std::ios::out);
  if(!test_file.is_open()){
    cerr << "\nFile for recording test suites could not be opened, exiting";
    exit(0);
  }
  double time_elapsed;


  //set up environment for KLEE
  InitKLEEFileName();
  uniqueViolatedTracker.clear();
  violatedAssertsTracker.clear();

  //Genrate the control dependency graph
  GenerateCDG();

  std::string provided_filename = inputF;
  //reads the assertion (per basic block), from the <filename>.ll
  read_assertions(provided_filename);
  //reads the unique assertion (per basic block)
  readUnique(provided_filename);
  //reads the (true and false)branches of a basic block, from the <filename>.ll
  getBranches(provided_filename);
  //map the assertions to basic block
  map_block_to_assertion(provided_filename);
  //find out which basic block computes the arguments for which assertion
  assertion_computation();

  //processes the CDG, to find how many assertions can be reached through the (left and right) branch condition, of a basic block
  preProcess();
  //processedCDG();exit(0);

  //before coming here, we have already run the program for a 
  //random input. therefore, use KLEE to collect the initial 
  //path condition
  gettimeofday(&analysis_start_time, NULL);
  gettimeofday(&time, NULL);
  time_elapsed = (time.tv_sec - analysis_start_time.tv_sec)+((time.tv_usec - analysis_start_time.tv_usec)/1000000.0);
  time_file << time_elapsed << "\t" << "0" << "\n"; 
  touch_file << time_elapsed << "\t" << "0" << "\n";
  BuildBranchOutcomeList();

  runKLEE();		
  //exit(0);


  //how many unique assertions where touched in this run of KLEE
  uniqueTouched();
  gettimeofday(&time, NULL);
  time_elapsed = (time.tv_sec - analysis_start_time.tv_sec)+((time.tv_usec - analysis_start_time.tv_usec)/1000000.0);
  //touch_file << time_elapsed << "\t" << ((assertions_tested*100)/ua_count)<< "\n";
  //how many unique assertions where violated in this run of KLEE
  gettimeofday(&time, NULL);
  time_elapsed = (time.tv_sec - analysis_start_time.tv_sec)+((time.tv_usec - analysis_start_time.tv_usec)/1000000.0);
  time_file << time_elapsed << "\t"; 
  uniqueViolations();
  if(violatedAsserts()!=0)
    adjustAssertionVectors();

  if(assertions_tested < total_violated)
    assertions_tested = total_violated;
  touch_file << time_elapsed << "\t" << assertions_tested<< "\n";
  //the following code will now use several API-s from the 
  //SMT library to collect the conditional expressions, 
  //negating one condition at a time to deviate the path 
  //and calling the SMT solver to solve new path conditions
  BuildPartialPathConditions();				

  //try to negate the last branch to follow a different path
  //however, the path is only explored if it is not already 
  //blocked by an already visited path program
  TryToDeviateFromCurrentPath();			

  cerr << "\nTotal assertions violated " << total_violated;
  cerr << "\nTotal assertions touched " << assertions_tested;


  gettimeofday(&time, NULL);
  time_elapsed = (time.tv_sec - analysis_start_time.tv_sec)+((time.tv_usec - analysis_start_time.tv_usec)/1000000.0);


  time_file.close();
  touch_file.close();
  test_file.close();

  cerr << "\n";
}

//Build all partial path conditions from current path condition
void DynamicSymExec::BuildPartialPathConditions()
{
  cerr << "\nBuild partial path conditions";
  VC vc = vc_createValidityChecker();

  //vc_setFlags(vc,'v');
  //vc_setFlags(vc,'s');
  //vc_setFlags(vc,'n');

  std::string KleeCVC(KLEE_TEST_DIR);
  KleeCVC += KLEE_CVC_FILENAME;
  KleeCVC += CVC_EXT;

  cerr << "\treading KLEE generated .cvc file.....";

  //pretty cool stuff, there are API-s which can read the CVC 
  //file generated by KLEE.....
  vc_parseExpr(vc, KleeCVC.c_str());

  int NoAssertions = 0;

  //the following stuff i.e. the API vc_getAllAsserts is written 
  //by "sudiptac" (at the "stp" code base).....so better be careful 
  //if some bug is hit here
  Expr* AllConditions = vc_getAllAsserts(vc, &NoAssertions);


  //  cerr << "\n no of assertions : " << NoAssertions;

  /*  //AllConditions is just the path condition.....
      cerr << "\nprinting path condition of current execution.....\n";
      for (int AI = 0; AI < NoAssertions; AI++){
      vc_printExpr(vc, AllConditions[AI]);
      }
      cerr << "\n";*/

  //build the partial path conditions and put them onto the database
  for (int CondI = 0; CondI < NoAssertions; CondI++) {
    Expr PartialCond = vc_trueExpr(vc);
    for (int PartialI = 0; PartialI <= CondI; PartialI++) {
      Expr Prev = PartialCond;
      if (PartialI < CondI) 		
        PartialCond = vc_andExpr(vc, PartialCond, AllConditions[PartialI]);
      else {
        Expr notLastCond = vc_notExpr(vc, AllConditions[PartialI]);
        PartialCond = vc_andExpr(vc, PartialCond, notLastCond);
      }
      //free up the previous memory here.....please do not disable it
      free(Prev);
    }

    //    cerr << "\nPrinting partial path conditions, for condI: "<<CondI<<"\n";
    //    vc_printExpr(vc, PartialCond);
    //    cerr << "\n";

    //builds the partial branch identity vector, which is used for the 
    //fast check of duplicate removal
    cerr <<"\nPartial path condition for condition number "<< (CondI+1) ;
    std::vector<bool>* BBVector = BuildPartialBranchIdentityVector(CondI, true);

    //insert the partial path conditions only if they are not visited before
    if (!PartialPathConditionTracker.count(*BBVector)) {
      cerr << "\tadded to UnexpandedPathConditionTracker";
      UnexpandedPathConditionTracker[*BBVector] = PartialCond;
      PartialPathConditionTracker[*BBVector] = PartialCond;
    }	else {//clean up
      cerr << "\tduplication detected, partial path condition not added.....";
      delete BBVector;
    }		
  }

  //cleanup the STP allocated memory in vc_getAllAsserts
  delete AllConditions;

  cerr << "\nFinished building all partial path conditions.....";		

  //clean up the validator
  vc_Destroy(vc);
}

//build partial branch identity vector for fast duplicate checks
//ARG0 : the argument specifies the sequence number of the first 
//input dependent branch (yes, not just any branch)
std::vector<bool>* DynamicSymExec::BuildPartialBranchIdentityVector(int EndBranchId, bool negate)
{
  std::vector<bool>* PartialBranchVector = new std::vector<bool>;		
  PartialBranchVector->clear();
  //std::string KleePATH(KLEE_TEST_DIR);
  std::string KleePATH(KLEE_PATH_FILENAME);
  //KleePATH += KLEE_PATH_FILENAME;
  KleePATH += PATH_EXT;
  std::ifstream KleePfd;

  //open the KLEE path file (modified in replay mode)
  KleePfd.open(KleePATH.c_str(), std::ios::in);

  //sanity check file descriptor
  if (!KleePfd.is_open())
    cerr << "KLEE modified path file " << KleePATH.c_str() << " cannot be opened.....\n";

  //*****CAUTION*****this portion is tricky, depends on how 
  //KLEE generates the *.path file
  //now build the partial branch outcome vector (note that 
  //this branch outcome vector may be infeasible, so should 
  //not be replayed back to KLEE)
  //  cerr << "file : " << KleePATH.c_str() << "\n";
  //  cerr << "Insdie buildpartialpathidentityvector\n";

  for (int SymCondI = -1, CondI = 0;  ; CondI++) {
    //the first entry specifies the branch and the next whether 
    //it is symbolic
    bool Bout, BSym, BAssert;
    KleePfd >> Bout;
    KleePfd >> BSym;
    KleePfd >> BAssert;

    //since we provide the last symbolic branch id, the increment 
    //is conditional
    if (BSym == 1) 
      SymCondI++;			

    //cerr << Bout;
    /*
    //build branch vector by negating the last branch
    if (SymCondI == EndBranchId) {
    if (negate) {
    if (Bout)
    PartialBranchVector->push_back(0);
    else
    PartialBranchVector->push_back(1);
    } else {
    if (Bout)
    PartialBranchVector->push_back(1);
    else
    PartialBranchVector->push_back(0);
    }
    //this was the last branch which was negated.....exit the loop here
    break;
    } else 
     */
    //{		
    if (Bout){
      PartialBranchVector->push_back(1);
    }
    else
      PartialBranchVector->push_back(0);


    // }		
    if (SymCondI == EndBranchId) 
      break;
  }
  cerr << "\nBout = ";
  for (unsigned sizeI = 0; sizeI < PartialBranchVector->size(); sizeI++) {
    cerr << (*PartialBranchVector)[sizeI];
  }
  cerr << "\n";

  KleePfd.close();

  return PartialBranchVector;
}

/*void DynamicSymExec::howmanytouched()
  {		
  std::ofstream KleeFd;

  std::map<std::string, std::string>::iterator ami;// assertion_marker;

  KleeFd.open(KLEEPathFileName.c_str(), std::ios_base::out|std::ios::trunc);		

  std::string child_label;

  int touch_count = 0;
  for (unsigned TI = 0; TI < ExecTrace->size(); TI++) {
  BasicBlock* BB = (*ExecTrace)[TI].first;

  std::string bblabel = BB->getName().data();
  ami = assertion_marker.find(bblabel); 
  if(ami!=assertion_marker.end())
  assertions_tested++;

  }
  KleeFd.close();
  }
 */
//build path (list of branch outcomes 0/1) to be processed by KLEE
void DynamicSymExec::BuildBranchOutcomeList()
{		
  std::ofstream KleeFd;


  KleeFd.open(KLEEPathFileName.c_str(), std::ios_base::out|std::ios::trunc);		

  //clear the memory
  BranchOutcomeVector.clear();
  std::string child_label;

  cerr << "\nBuildBranchOutcomeList ("<< KLEEPathFileName.c_str() << ") ";
  //build branch outcome trace from each conditional branch instructions
  for (unsigned TI = 0; TI < ExecTrace->size(); TI++) {
    bool BBOut = (*ExecTrace)[TI].second;
    BasicBlock* BB = (*ExecTrace)[TI].first;

    std::string bblabel = BB->getName().data();

    if (true) {
      BranchOutcomeVector.push_back(BBOut);
      KleeFd << BBOut << "\n";
    }
  }
  KleeFd.close();
  cerr << "\nBranch outcome vector made";
}

//create symbolic input names -- the input filename is given 
//through command line
void DynamicSymExec::CreateSymbolicInputs(std::ifstream &ISYM)
{
  std::string line;
  ValueSymbolTable &VSym = M->getValueSymbolTable();

  //builds the symbolic input name array by reading through 
  //the file
  while ( getline(ISYM,line) ) {
    StringRef SymbolicInput(line);
    //sudiptac:
    cerr << "line sym = " << line.c_str() << "\n";
    for (ValueSymbolTable::iterator VI = VSym.begin(), VE = VSym.end(); 
        VI != VE; ++VI) {
      Value* V = VI->getValue();
      if (SymbolicInput.equals(V->getName()))
        SymbolicInputVector.push_back(V);
    }
  }
}

//forcefully inline all function calls (recursive calls are not handled currently)
//Attn: Abhijeet (currently not used, unsure whether you will need it or not)
void DynamicSymExec::ForceInlining()
{
  //iterate over all the basic blocks of "main" and produce the SAT encoding
  Function* F = M->getFunction("main");
  //sanity check
  assert(F && "you are a SATAN.....no \"main\" function found in test program");
  std::vector<Function*> ToBeRemovedFromParent;

  //before doing everything, inline the functions using the basic inline 
  //implementor class. I am not sure what it does when you have recursive 
  //calls. with the current implementation, I manually write an iteartive 
  //version of the recursive function. Overall, I assume that there are 
  //no recursive function in your test program

  BasicInliner InlineF;
  //it's a whole sale inlining dude, therefore insert all the functions inside 
  //the queue to be inlined
  for (Module::iterator IM = M->begin(), EM = M->end(); IM != EM; IM++) {
    Function* FCall = dyn_cast<Function>(IM);
    if (!(FCall->getName().equals(INTERNAL_CALL)))
      InlineF.addFunction(FCall);
    if (FCall != F && !(FCall->getName().equals(INTERNAL_CALL)))
      ToBeRemovedFromParent.push_back(FCall);
  }
  //call the inliner, "true" argument force the inliner to always inline irrespective of 
  //the cost.....because note that we do not care about the cost of inlining, as it is 
  //entirely virtual for the purpose of quantitative analysis
  InlineF.inlineFunctions(true);

  //now erase all the functions from the parent module except "main", phew.....
  //just like that, everybody is gone ;-)
  for (unsigned RI = 0; RI < ToBeRemovedFromParent.size(); RI++) {
    Function* FCall = ToBeRemovedFromParent[RI];
    FCall->dropAllReferences();
  }		
}

//Generate loop information
//Attn: Abhijeet (currently not used, but you may need it)
void DynamicSymExec::GenerateLoopInfo() 
{
  //create a function pass manager to add existing loop analysis. note that 
  //to create the SAT encoding of MSCC decomposition, we need loop informat
  //-ion, and therefore, we first schedule the "LoopInfo" pass
  FunctionPassManager* OurFPM;
  OurFPM = new FunctionPassManager(new ExistingModuleProvider(M));
  //create the loop info generation pass
  LI = createLoopInfoPass();
  OurFPM->add(LI);
  OurFPM->doInitialization();

  //iterate over all the basic blocks of "main" and produce the SAT encoding
  Function* F = M->getFunction("main");
  //sanity check
  assert(F && "you are a SATAN.....no \"main\" function found in test program");

  //run the LoopInfo Pass to get the informations abour program loops
  OurFPM->run(*F);

  //Attn: Abhijeet after this pass finished, LI contains the loop information. 
  //Refer to the respective class.
}


//Generate control dependency graph
void DynamicSymExec::GenerateCDG() 
{
  cerr << "\nGenerate control dependence graph ";


  //create a function pass manager to add existing post dominator analysis 
  FunctionPassManager* ourFPM;
  ourFPM = new FunctionPassManager(new ExistingModuleProvider(M));

  //create the postdominator tree generation pass
  PostDominanceFrontier* PDF = createPostDominanceFrontier();
  ourFPM->add(PDF);
  ourFPM->doInitialization();


  //iterate over all the basic blocks of "main" and produce the SAT encoding
  Function* F = M->getFunction("main");
  assert(F && "you are a SATAN.....no \"main\" function found in test program");
  //F->viewCFG();

  ourFPM->run(*F);
  //PDF->dump();

  for (DominanceFrontierBase::iterator I = PDF->begin(),E = PDF->end(); I != E;++I ) {

    //an element in the structure CDG, is made for each basic block
    controlGraph_t *temp = (controlGraph_t *)malloc(sizeof(controlGraph_t *)); 
    temp->bblock = I->first;
    //cout << "\n Node "<< ((temp->bblock)->getName()).data() << " created";
    temp->dSet = NULL;
    temp->next = CDG;
    CDG = temp;
  }

  for (DominanceFrontierBase::iterator I = PDF->begin(),E = PDF->end(); I != E;++I ) {

    BasicBlock *Node = I->first;
    //second element contains the post-dominance frontier
    DominanceFrontier::DomSetType BBSet = I->second;

    //n is control dependent on c iff,
    //c belongs to DF(n) but computed on the reverse CFG
    //For more details see Cryton et al., 1991 section 6
    for (DominanceFrontier::DomSetType::iterator BBSetI = BBSet.begin(),BBSetE = BBSet.end(); BBSetI != BBSetE; ++BBSetI) {
      BasicBlock *DFMember = *BBSetI;
      controlGraph_t *parent = search(DFMember);
      if(parent==NULL){
        report_error(1);
      }
      BasicBlockSet_t *dSet = (BasicBlockSet_t *)malloc(sizeof(BasicBlockSet_t *));
      dSet->next = parent->dSet;
      dSet->bblock = Node;
      parent->dSet = dSet;
    }

  }

  cerr << "\nCreated control dependence graph ";
  //At this point CDG is complete, following command can be used to print it
//    display_CDG();
//    exit(0);
}
