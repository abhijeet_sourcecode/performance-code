; ModuleID = 'example1.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32"
target triple = "i386-pc-linux-gnu"

@N = global i32 3                                 ; <i32*> [#uses=1]
@x = common global i32 0                          ; <i32*> [#uses=2]
@y = common global i32 0                          ; <i32*> [#uses=1]

define i32 @main() nounwind {
entry:
  %retval = alloca i32                            ; <i32*> [#uses=1]
  %i = alloca i32                                 ; <i32*> [#uses=4]
  %d = alloca i32                                 ; <i32*> [#uses=1]
  %c = alloca i32                                 ; <i32*> [#uses=1]
  %b = alloca i32                                 ; <i32*> [#uses=2]
  %a = alloca i32                                 ; <i32*> [#uses=2]
  %"alloca point" = bitcast i32 0 to i32          ; <i32> [#uses=0]
  store i32 0, i32* %i, align 4
  br label %bb

bb:                                               ; preds = %bb6, %entry
  %0 = load i32* @x, align 4                      ; <i32> [#uses=1]
  %1 = icmp sle i32 %0, 3                         ; <i1> [#uses=1]
  br i1 %1, label %bb1, label %bb2

bb1:                                              ; preds = %bb
  store i32 1, i32* %a, align 4
  br label %bb3

bb2:                                              ; preds = %bb
  store i32 1, i32* %b, align 4
  br label %bb3

bb3:                                              ; preds = %bb2, %bb1
  %2 = load i32* @x, align 4                      ; <i32> [#uses=1]
  %3 = icmp sle i32 %2, 0                         ; <i1> [#uses=1]
  br i1 %3, label %bb4, label %bb5

bb4:                                              ; preds = %bb3
  store i32 1, i32* %c, align 4
  br label %bb6

bb5:                                              ; preds = %bb3
  store i32 1, i32* %d, align 4
  br label %bb6

bb6:                                              ; preds = %bb5, %bb4
  %4 = load i32* %i, align 4                      ; <i32> [#uses=1]
  %5 = add nsw i32 %4, 1                          ; <i32> [#uses=1]
  store i32 %5, i32* %i, align 4
  %6 = load i32* @N, align 4                      ; <i32> [#uses=1]
  %7 = load i32* %i, align 4                      ; <i32> [#uses=1]
  %8 = icmp slt i32 %7, %6                        ; <i1> [#uses=1]
  br i1 %8, label %bb, label %bb7

bb7:                                              ; preds = %bb6
  %9 = load i32* @y, align 4                      ; <i32> [#uses=1]
  %10 = icmp sgt i32 %9, 9                        ; <i1> [#uses=1]
  br i1 %10, label %bb8, label %bb9

bb8:                                              ; preds = %bb7
  store i32 2, i32* %a, align 4
  br label %bb9

bb9:                                              ; preds = %bb8, %bb7
  store i32 3, i32* %b, align 4
  br label %return

return:                                           ; preds = %bb9
  %retval10 = load i32* %retval                   ; <i32> [#uses=1]
  ret i32 %retval10
}
