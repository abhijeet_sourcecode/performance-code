long a[30];

#define NO_OF_ITERATIONS 7

int main()
{
				int x, i;
				klee_make_symbolic(a, sizeof(a), "a");	
				
				for (i = 0; i < NO_OF_ITERATIONS; i++) {
						if (a[i] < 3)		
								x += a[i];	
						else
								x += a[i] + 12;	
				}
}
				
