; ModuleID = 'insertsort__converted__KLEE.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32"
target triple = "i386-pc-linux-gnu"

@C__400340 = global i32 0                         ; <i32*> [#uses=6]
@C__400540 = global i32 0                         ; <i32*> [#uses=9]
@flag__0X400340__0X400540 = global i8 0           ; <i8*> [#uses=6]
@flag__0X400540__0X400340 = global i8 0           ; <i8*> [#uses=3]
@C__400328 = global i32 0                         ; <i32*> [#uses=7]
@C__400520 = global i32 0                         ; <i32*> [#uses=8]
@flag__0X400328__0X400520 = global i8 0           ; <i8*> [#uses=5]
@flag__0X400520__0X400328 = global i8 0           ; <i8*> [#uses=4]
@a = common global [11 x i32] zeroinitializer, align 32 ; <[11 x i32]*> [#uses=17]
@.str = private constant [32 x i8] c"C__400340 <= 0 & C__400540 <= 0\00", align 1 ; <[32 x i8]*> [#uses=1]
@.str1 = private constant [30 x i8] c"insertsort__converted__KLEE.c\00", align 1 ; <[30 x i8]*> [#uses=1]
@__PRETTY_FUNCTION__.1520 = internal constant [5 x i8] c"main\00" ; <[5 x i8]*> [#uses=1]
@.str2 = private constant [32 x i8] c"C__400328 <= 0 & C__400520 <= 0\00", align 1 ; <[32 x i8]*> [#uses=1]

define i32 @main() nounwind {
entry:
  %retval = alloca i32                            ; <i32*> [#uses=2]
  %cnt2 = alloca i32                              ; <i32*> [#uses=3]
  %cnt1 = alloca i32                              ; <i32*> [#uses=3]
  %temp = alloca i32                              ; <i32*> [#uses=2]
  %j = alloca i32                                 ; <i32*> [#uses=9]
  %i = alloca i32                                 ; <i32*> [#uses=5]
  %0 = alloca i32                                 ; <i32*> [#uses=2]
  %"alloca point" = bitcast i32 0 to i32          ; <i32> [#uses=0]
  store i32 0, i32* %cnt1, align 4
  store i32 0, i32* %cnt2, align 4
  store i32 0, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 0), align 4
  store i32 11, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 1), align 4
  store i32 10, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 2), align 4
  store i32 9, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 3), align 4
  store i32 8, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 4), align 4
  store i32 7, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 5), align 4
  store i32 6, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 6), align 4
  store i32 5, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 7), align 4
  store i32 4, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 8), align 4
  store i32 3, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 9), align 4
  store i32 2, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 10), align 4
  %1 = load i8* @flag__0X400340__0X400540, align 1 ; <i8> [#uses=1]
  %2 = icmp eq i8 %1, 0                           ; <i1> [#uses=1]
  %3 = zext i1 %2 to i32                          ; <i32> [#uses=1]
  %4 = load i32* @C__400540, align 4              ; <i32> [#uses=1]
  %5 = add nsw i32 %3, %4                         ; <i32> [#uses=1]
  store i32 %5, i32* @C__400540, align 4
  store i8 1, i8* @flag__0X400340__0X400540, align 1
  %6 = load i32* @C__400340, align 4              ; <i32> [#uses=1]
  %7 = icmp sgt i32 %6, 0                         ; <i1> [#uses=1]
  %8 = zext i1 %7 to i8                           ; <i8> [#uses=1]
  %9 = load i32* @C__400540, align 4              ; <i32> [#uses=1]
  %10 = icmp sgt i32 %9, 0                        ; <i1> [#uses=1]
  %11 = zext i1 %10 to i8                         ; <i8> [#uses=1]
  %toBool = icmp ne i8 %8, 0                      ; <i1> [#uses=1]
  %toBool1 = icmp ne i8 %11, 0                    ; <i1> [#uses=1]
  %12 = or i1 %toBool, %toBool1                   ; <i1> [#uses=1]
  %13 = zext i1 %12 to i8                         ; <i8> [#uses=1]
  %toBool2 = icmp ne i8 %13, 0                    ; <i1> [#uses=1]
  br i1 %toBool2, label %bb, label %bb3

bb:                                               ; preds = %entry
  %14 = call i32 (...)* @__assert_fail(i8* getelementptr inbounds ([32 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([30 x i8]* @.str1, i32 0, i32 0), i32 22, i8* getelementptr inbounds ([5 x i8]* @__PRETTY_FUNCTION__.1520, i32 0, i32 0)) nounwind ; <i32> [#uses=0]
  br label %bb3

bb3:                                              ; preds = %bb, %entry
  store i8 0, i8* @flag__0X400540__0X400340, align 1
  store i32 0, i32* @C__400340, align 4
  %15 = load i8* @flag__0X400340__0X400540, align 1 ; <i8> [#uses=1]
  %16 = icmp eq i8 %15, 0                         ; <i1> [#uses=1]
  %17 = zext i1 %16 to i32                        ; <i32> [#uses=1]
  %18 = load i32* @C__400540, align 4             ; <i32> [#uses=1]
  %19 = add nsw i32 %17, %18                      ; <i32> [#uses=1]
  store i32 %19, i32* @C__400540, align 4
  store i8 1, i8* @flag__0X400340__0X400540, align 1
  %20 = load i32* @C__400340, align 4             ; <i32> [#uses=1]
  %21 = icmp sgt i32 %20, 0                       ; <i1> [#uses=1]
  %22 = zext i1 %21 to i8                         ; <i8> [#uses=1]
  %23 = load i32* @C__400540, align 4             ; <i32> [#uses=1]
  %24 = icmp sgt i32 %23, 0                       ; <i1> [#uses=1]
  %25 = zext i1 %24 to i8                         ; <i8> [#uses=1]
  %toBool4 = icmp ne i8 %22, 0                    ; <i1> [#uses=1]
  %toBool5 = icmp ne i8 %25, 0                    ; <i1> [#uses=1]
  %26 = or i1 %toBool4, %toBool5                  ; <i1> [#uses=1]
  %27 = zext i1 %26 to i8                         ; <i8> [#uses=1]
  %toBool6 = icmp ne i8 %27, 0                    ; <i1> [#uses=1]
  br i1 %toBool6, label %bb7, label %bb8

bb7:                                              ; preds = %bb3
  %28 = call i32 (...)* @__assert_fail(i8* getelementptr inbounds ([32 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([30 x i8]* @.str1, i32 0, i32 0), i32 28, i8* getelementptr inbounds ([5 x i8]* @__PRETTY_FUNCTION__.1520, i32 0, i32 0)) nounwind ; <i32> [#uses=0]
  br label %bb8

bb8:                                              ; preds = %bb7, %bb3
  store i8 0, i8* @flag__0X400540__0X400340, align 1
  store i32 0, i32* @C__400340, align 4
  %29 = load i8* @flag__0X400328__0X400520, align 1 ; <i8> [#uses=1]
  %30 = icmp eq i8 %29, 0                         ; <i1> [#uses=1]
  %31 = zext i1 %30 to i32                        ; <i32> [#uses=1]
  %32 = load i32* @C__400520, align 4             ; <i32> [#uses=1]
  %33 = add nsw i32 %31, %32                      ; <i32> [#uses=1]
  store i32 %33, i32* @C__400520, align 4
  store i8 1, i8* @flag__0X400328__0X400520, align 1
  %34 = load i32* @C__400328, align 4             ; <i32> [#uses=1]
  %35 = icmp sgt i32 %34, 0                       ; <i1> [#uses=1]
  %36 = zext i1 %35 to i8                         ; <i8> [#uses=1]
  %37 = load i32* @C__400520, align 4             ; <i32> [#uses=1]
  %38 = icmp sgt i32 %37, 0                       ; <i1> [#uses=1]
  %39 = zext i1 %38 to i8                         ; <i8> [#uses=1]
  %toBool9 = icmp ne i8 %36, 0                    ; <i1> [#uses=1]
  %toBool10 = icmp ne i8 %39, 0                   ; <i1> [#uses=1]
  %40 = or i1 %toBool9, %toBool10                 ; <i1> [#uses=1]
  %41 = zext i1 %40 to i8                         ; <i8> [#uses=1]
  %toBool11 = icmp ne i8 %41, 0                   ; <i1> [#uses=1]
  br i1 %toBool11, label %bb12, label %bb13

bb12:                                             ; preds = %bb8
  %42 = call i32 (...)* @__assert_fail(i8* getelementptr inbounds ([32 x i8]* @.str2, i32 0, i32 0), i8* getelementptr inbounds ([30 x i8]* @.str1, i32 0, i32 0), i32 34, i8* getelementptr inbounds ([5 x i8]* @__PRETTY_FUNCTION__.1520, i32 0, i32 0)) nounwind ; <i32> [#uses=0]
  br label %bb13

bb13:                                             ; preds = %bb12, %bb8
  store i8 0, i8* @flag__0X400520__0X400328, align 1
  store i32 0, i32* @C__400328, align 4
  %43 = load i8* @flag__0X400520__0X400328, align 1 ; <i8> [#uses=1]
  %44 = icmp eq i8 %43, 0                         ; <i1> [#uses=1]
  %45 = zext i1 %44 to i32                        ; <i32> [#uses=1]
  %46 = load i32* @C__400328, align 4             ; <i32> [#uses=1]
  %47 = add nsw i32 %45, %46                      ; <i32> [#uses=1]
  store i32 %47, i32* @C__400328, align 4
  store i8 1, i8* @flag__0X400520__0X400328, align 1
  %48 = load i32* @C__400328, align 4             ; <i32> [#uses=1]
  %49 = icmp sgt i32 %48, 0                       ; <i1> [#uses=1]
  %50 = zext i1 %49 to i8                         ; <i8> [#uses=1]
  %51 = load i32* @C__400520, align 4             ; <i32> [#uses=1]
  %52 = icmp sgt i32 %51, 0                       ; <i1> [#uses=1]
  %53 = zext i1 %52 to i8                         ; <i8> [#uses=1]
  %toBool14 = icmp ne i8 %50, 0                   ; <i1> [#uses=1]
  %toBool15 = icmp ne i8 %53, 0                   ; <i1> [#uses=1]
  %54 = or i1 %toBool14, %toBool15                ; <i1> [#uses=1]
  %55 = zext i1 %54 to i8                         ; <i8> [#uses=1]
  %toBool16 = icmp ne i8 %55, 0                   ; <i1> [#uses=1]
  br i1 %toBool16, label %bb17, label %bb18

bb17:                                             ; preds = %bb13
  %56 = call i32 (...)* @__assert_fail(i8* getelementptr inbounds ([32 x i8]* @.str2, i32 0, i32 0), i8* getelementptr inbounds ([30 x i8]* @.str1, i32 0, i32 0), i32 40, i8* getelementptr inbounds ([5 x i8]* @__PRETTY_FUNCTION__.1520, i32 0, i32 0)) nounwind ; <i32> [#uses=0]
  br label %bb18

bb18:                                             ; preds = %bb17, %bb13
  store i8 0, i8* @flag__0X400328__0X400520, align 1
  store i32 0, i32* @C__400520, align 4
  store i32 2, i32* %i, align 4
  br label %bb33

bb19:                                             ; preds = %bb33
  %57 = load i8* @flag__0X400340__0X400540, align 1 ; <i8> [#uses=1]
  %58 = icmp eq i8 %57, 0                         ; <i1> [#uses=1]
  %59 = zext i1 %58 to i32                        ; <i32> [#uses=1]
  %60 = load i32* @C__400540, align 4             ; <i32> [#uses=1]
  %61 = add nsw i32 %59, %60                      ; <i32> [#uses=1]
  store i32 %61, i32* @C__400540, align 4
  store i8 1, i8* @flag__0X400340__0X400540, align 1
  %62 = load i32* @C__400340, align 4             ; <i32> [#uses=1]
  %63 = icmp sgt i32 %62, 0                       ; <i1> [#uses=1]
  %64 = zext i1 %63 to i8                         ; <i8> [#uses=1]
  %65 = load i32* @C__400540, align 4             ; <i32> [#uses=1]
  %66 = icmp sgt i32 %65, 0                       ; <i1> [#uses=1]
  %67 = zext i1 %66 to i8                         ; <i8> [#uses=1]
  %toBool20 = icmp ne i8 %64, 0                   ; <i1> [#uses=1]
  %toBool21 = icmp ne i8 %67, 0                   ; <i1> [#uses=1]
  %68 = or i1 %toBool20, %toBool21                ; <i1> [#uses=1]
  %69 = zext i1 %68 to i8                         ; <i8> [#uses=1]
  %toBool22 = icmp ne i8 %69, 0                   ; <i1> [#uses=1]
  br i1 %toBool22, label %bb23, label %bb24

bb23:                                             ; preds = %bb19
  %70 = call i32 (...)* @__assert_fail(i8* getelementptr inbounds ([32 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([30 x i8]* @.str1, i32 0, i32 0), i32 47, i8* getelementptr inbounds ([5 x i8]* @__PRETTY_FUNCTION__.1520, i32 0, i32 0)) nounwind ; <i32> [#uses=0]
  br label %bb24

bb24:                                             ; preds = %bb23, %bb19
  store i8 0, i8* @flag__0X400540__0X400340, align 1
  store i32 0, i32* @C__400340, align 4
  %71 = load i32* %i, align 4                     ; <i32> [#uses=1]
  store i32 %71, i32* %j, align 4
  %72 = load i32* %cnt1, align 4                  ; <i32> [#uses=1]
  %73 = add nsw i32 %72, 1                        ; <i32> [#uses=1]
  store i32 %73, i32* %cnt1, align 4
  br label %bb31

bb25:                                             ; preds = %bb31
  %74 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %75 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %74 ; <i32*> [#uses=1]
  %76 = load i32* %75, align 4                    ; <i32> [#uses=1]
  store i32 %76, i32* %temp, align 4
  %77 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %78 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %79 = sub i32 %78, 1                            ; <i32> [#uses=1]
  %80 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %79 ; <i32*> [#uses=1]
  %81 = load i32* %80, align 4                    ; <i32> [#uses=1]
  %82 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %77 ; <i32*> [#uses=1]
  store i32 %81, i32* %82, align 4
  %83 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %84 = sub i32 %83, 1                            ; <i32> [#uses=1]
  %85 = load i32* %temp, align 4                  ; <i32> [#uses=1]
  %86 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %84 ; <i32*> [#uses=1]
  store i32 %85, i32* %86, align 4
  %87 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %88 = sub i32 %87, 1                            ; <i32> [#uses=1]
  store i32 %88, i32* %j, align 4
  %89 = load i8* @flag__0X400328__0X400520, align 1 ; <i8> [#uses=1]
  %90 = icmp eq i8 %89, 0                         ; <i1> [#uses=1]
  %91 = zext i1 %90 to i32                        ; <i32> [#uses=1]
  %92 = load i32* @C__400520, align 4             ; <i32> [#uses=1]
  %93 = add nsw i32 %91, %92                      ; <i32> [#uses=1]
  store i32 %93, i32* @C__400520, align 4
  store i8 1, i8* @flag__0X400328__0X400520, align 1
  %94 = load i32* @C__400328, align 4             ; <i32> [#uses=1]
  %95 = icmp sgt i32 %94, 0                       ; <i1> [#uses=1]
  %96 = zext i1 %95 to i8                         ; <i8> [#uses=1]
  %97 = load i32* @C__400520, align 4             ; <i32> [#uses=1]
  %98 = icmp sgt i32 %97, 0                       ; <i1> [#uses=1]
  %99 = zext i1 %98 to i8                         ; <i8> [#uses=1]
  %toBool26 = icmp ne i8 %96, 0                   ; <i1> [#uses=1]
  %toBool27 = icmp ne i8 %99, 0                   ; <i1> [#uses=1]
  %100 = or i1 %toBool26, %toBool27               ; <i1> [#uses=1]
  %101 = zext i1 %100 to i8                       ; <i8> [#uses=1]
  %toBool28 = icmp ne i8 %101, 0                  ; <i1> [#uses=1]
  br i1 %toBool28, label %bb29, label %bb30

bb29:                                             ; preds = %bb25
  %102 = call i32 (...)* @__assert_fail(i8* getelementptr inbounds ([32 x i8]* @.str2, i32 0, i32 0), i8* getelementptr inbounds ([30 x i8]* @.str1, i32 0, i32 0), i32 61, i8* getelementptr inbounds ([5 x i8]* @__PRETTY_FUNCTION__.1520, i32 0, i32 0)) nounwind ; <i32> [#uses=0]
  br label %bb30

bb30:                                             ; preds = %bb29, %bb25
  store i8 0, i8* @flag__0X400520__0X400328, align 1
  store i32 0, i32* @C__400328, align 4
  %103 = load i32* %cnt2, align 4                 ; <i32> [#uses=1]
  %104 = add nsw i32 %103, 1                      ; <i32> [#uses=1]
  store i32 %104, i32* %cnt2, align 4
  br label %bb31

bb31:                                             ; preds = %bb30, %bb24
  %105 = load i32* %j, align 4                    ; <i32> [#uses=1]
  %106 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %105 ; <i32*> [#uses=1]
  %107 = load i32* %106, align 4                  ; <i32> [#uses=1]
  %108 = load i32* %j, align 4                    ; <i32> [#uses=1]
  %109 = sub i32 %108, 1                          ; <i32> [#uses=1]
  %110 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %109 ; <i32*> [#uses=1]
  %111 = load i32* %110, align 4                  ; <i32> [#uses=1]
  %112 = icmp ult i32 %107, %111                  ; <i1> [#uses=1]
  br i1 %112, label %bb25, label %bb32

bb32:                                             ; preds = %bb31
  %113 = load i32* %i, align 4                    ; <i32> [#uses=1]
  %114 = add nsw i32 %113, 1                      ; <i32> [#uses=1]
  store i32 %114, i32* %i, align 4
  br label %bb33

bb33:                                             ; preds = %bb32, %bb18
  %115 = load i32* %i, align 4                    ; <i32> [#uses=1]
  %116 = icmp sle i32 %115, 9                     ; <i1> [#uses=1]
  br i1 %116, label %bb19, label %bb34

bb34:                                             ; preds = %bb33
  store i32 1, i32* %0, align 4
  %117 = load i32* %0, align 4                    ; <i32> [#uses=1]
  store i32 %117, i32* %retval, align 4
  br label %return

return:                                           ; preds = %bb34
  %retval35 = load i32* %retval                   ; <i32> [#uses=1]
  ret i32 %retval35
}

declare i32 @__assert_fail(...)
