#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <map>

using namespace std;

map<string, int> assertions_per_block;
int makeRecord(string task) {

  int task_len = task.length();
  string postfileread = "__converted__KLEE.ll";
  string postfilewrite = "_record";
  string temp = task.substr(0,task_len-3);
  string input_file = temp.append(postfileread);
  const char* input_filename = input_file.c_str();
  
  ifstream iFile(input_filename);
  if(!iFile.is_open()){
    std::cout<<"Could not open "<<input_filename;
    exit(0);
  }

  string line;
  string assertCalls("PRETTY_FUNCTION");
  string entry("entry:");
  string block("bb");
  string colon(":");

  int assert_len = assertCalls.length();
  int start = 0;
  int total_assertions = 0;

  int total = 0;
  int pos;
  string last_label;
  while(getline(iFile, line)) {
    if(start){
      pos = line.find(colon);
      if(pos!=-1 && start){
        assertions_per_block.insert(std::make_pair(last_label,total));
        last_label = line.substr(0,pos);
        total = 0;
      }
      pos = line.find(assertCalls);
      while(pos!=-1){
        total++;
        total_assertions++;
        pos = line.find(assertCalls,pos+assert_len);
      }
    }
    if(line.find(entry)!=std::string::npos){
      start = 1;
      last_label = "entry";
    }
  }

  iFile.close();
}

int main(){
  string task = "test_short.bc";
  makeRecord(task);
  for( map<string, int>::iterator ii=assertions_per_block.begin(); ii!=assertions_per_block.end(); ++ii)
  {
    cout << (*ii).first << ": " << (*ii).second << endl;
  }
}
