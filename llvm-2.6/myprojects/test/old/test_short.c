#include <klee/klee.h>
int a;
int a1,a2;
#define NO_OF_ITERATIONS 4

int main()
{
				int x, i;
				klee_make_symbolic(&a1, sizeof(a1), "a1");	
				klee_make_symbolic(&a2, sizeof(a2), "a2");	
                if(a1==1)
                  klee_assert(0);

				for (i = 0; i < NO_OF_ITERATIONS; i++) {
						if (i == 0)
								a = a1;
						if (i == 1)
								a = a2;
						if (a < 5)
								x += a - 5;
						else
								x += a + 12;	
				}
}
				
