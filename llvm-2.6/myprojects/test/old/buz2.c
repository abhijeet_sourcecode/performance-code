#include <klee.h>
#include <assert.h>

/* fibonacci_ref... */
int fibonacci_ref(int n)
{
  if      (n == 0) return 0;
  else if (n == 1) return 1;
  else             return fibonacci_ref(n-1) + fibonacci_ref(n-2);
}
/* fibonacci_opt... */
int fibonacci_opt(int n)
{
  if (n == 0)      return 0;
  else if (n == 1) return 1;
  else
  {
    int a, b, c;
    int i;
    a = 0; b = 1; c = 1;
    for (i = 1; i < n; i++)
    {
      a = b; b = c; c = a + b;
    }
    return c;
  }
}

int main(int argc, char** argv)
{
  int n, a, b;
  klee_make_symbolic(&n, sizeof(n), "n");
  n &= 0x3F;
  a = fibonacci_ref(n);
  b = fibonacci_opt(n);
  klee_assert(a == b);
  return 0;
}
