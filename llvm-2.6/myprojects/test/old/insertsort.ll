; ModuleID = 'insertsort.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32"
target triple = "i386-pc-linux-gnu"

@a = common global [11 x i32] zeroinitializer, align 32 ; <[11 x i32]*> [#uses=17]

define i32 @main() nounwind {
entry:
  %retval = alloca i32                            ; <i32*> [#uses=2]
  %cnt2 = alloca i32                              ; <i32*> [#uses=3]
  %cnt1 = alloca i32                              ; <i32*> [#uses=3]
  %temp = alloca i32                              ; <i32*> [#uses=2]
  %j = alloca i32                                 ; <i32*> [#uses=9]
  %i = alloca i32                                 ; <i32*> [#uses=5]
  %0 = alloca i32                                 ; <i32*> [#uses=2]
  %"alloca point" = bitcast i32 0 to i32          ; <i32> [#uses=0]
  store i32 0, i32* %cnt1, align 4
  store i32 0, i32* %cnt2, align 4
  store i32 0, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 0), align 4
  store i32 11, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 1), align 4
  store i32 10, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 2), align 4
  store i32 9, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 3), align 4
  store i32 8, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 4), align 4
  store i32 7, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 5), align 4
  store i32 6, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 6), align 4
  store i32 5, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 7), align 4
  store i32 4, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 8), align 4
  store i32 3, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 9), align 4
  store i32 2, i32* getelementptr inbounds ([11 x i32]* @a, i32 0, i32 10), align 4
  store i32 2, i32* %i, align 4
  br label %bb4

bb:                                               ; preds = %bb4
  %1 = load i32* %i, align 4                      ; <i32> [#uses=1]
  store i32 %1, i32* %j, align 4
  %2 = load i32* %cnt1, align 4                   ; <i32> [#uses=1]
  %3 = add nsw i32 %2, 1                          ; <i32> [#uses=1]
  store i32 %3, i32* %cnt1, align 4
  br label %bb2

bb1:                                              ; preds = %bb2
  %4 = load i32* %j, align 4                      ; <i32> [#uses=1]
  %5 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %4 ; <i32*> [#uses=1]
  %6 = load i32* %5, align 4                      ; <i32> [#uses=1]
  store i32 %6, i32* %temp, align 4
  %7 = load i32* %j, align 4                      ; <i32> [#uses=1]
  %8 = load i32* %j, align 4                      ; <i32> [#uses=1]
  %9 = sub i32 %8, 1                              ; <i32> [#uses=1]
  %10 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %9 ; <i32*> [#uses=1]
  %11 = load i32* %10, align 4                    ; <i32> [#uses=1]
  %12 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %7 ; <i32*> [#uses=1]
  store i32 %11, i32* %12, align 4
  %13 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %14 = sub i32 %13, 1                            ; <i32> [#uses=1]
  %15 = load i32* %temp, align 4                  ; <i32> [#uses=1]
  %16 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %14 ; <i32*> [#uses=1]
  store i32 %15, i32* %16, align 4
  %17 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %18 = sub i32 %17, 1                            ; <i32> [#uses=1]
  store i32 %18, i32* %j, align 4
  %19 = load i32* %cnt2, align 4                  ; <i32> [#uses=1]
  %20 = add nsw i32 %19, 1                        ; <i32> [#uses=1]
  store i32 %20, i32* %cnt2, align 4
  br label %bb2

bb2:                                              ; preds = %bb1, %bb
  %21 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %22 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %21 ; <i32*> [#uses=1]
  %23 = load i32* %22, align 4                    ; <i32> [#uses=1]
  %24 = load i32* %j, align 4                     ; <i32> [#uses=1]
  %25 = sub i32 %24, 1                            ; <i32> [#uses=1]
  %26 = getelementptr inbounds [11 x i32]* @a, i32 0, i32 %25 ; <i32*> [#uses=1]
  %27 = load i32* %26, align 4                    ; <i32> [#uses=1]
  %28 = icmp ult i32 %23, %27                     ; <i1> [#uses=1]
  br i1 %28, label %bb1, label %bb3

bb3:                                              ; preds = %bb2
  %29 = load i32* %i, align 4                     ; <i32> [#uses=1]
  %30 = add nsw i32 %29, 1                        ; <i32> [#uses=1]
  store i32 %30, i32* %i, align 4
  br label %bb4

bb4:                                              ; preds = %bb3, %entry
  %31 = load i32* %i, align 4                     ; <i32> [#uses=1]
  %32 = icmp sle i32 %31, 9                       ; <i1> [#uses=1]
  br i1 %32, label %bb, label %bb5

bb5:                                              ; preds = %bb4
  store i32 1, i32* %0, align 4
  %33 = load i32* %0, align 4                     ; <i32> [#uses=1]
  store i32 %33, i32* %retval, align 4
  br label %return

return:                                           ; preds = %bb5
  %retval6 = load i32* %retval                    ; <i32> [#uses=1]
  ret i32 %retval6
}
