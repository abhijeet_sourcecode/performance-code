#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int makeRecord(string task) {

  string postfileread = "__converted__KLEE.ll";
  string postfilewrite = "_record";
  string temp = task;
  string input_file = task.append(postfileread);
  string output_file = temp.append(postfilewrite);
  const char* input_filename = input_file.c_str();
  const char* output_filename = output_file.c_str();
  
  ifstream iFile(input_filename);

  ofstream myfile;
  myfile.open (output_filename);
  string line;
  string assertCalls("PRETTY_FUNCTION");
  string entry("entry:");
  string block("bb");
  string colon(":");

  int assert_len = assertCalls.length();
  int start = 0;
  int total_assertions = 0;

  int total = 0;
  int pos;
  while(getline(iFile, line)) {
    if(start){
      pos = line.find(colon);
      if(pos!=-1 && start){
        string label = line.substr(0,pos);
        myfile<< total << endl;
        myfile << label << endl;
        total = 0;
      }
      pos = line.find(assertCalls);
      while(pos!=-1){
        total++;
        total_assertions++;
        pos = line.find(assertCalls,pos+assert_len);
        //      myfile << line << endl;
      }
    }
    if(line.find(entry)!=-1){
      start = 1;
      myfile<<"entry"<<endl;
    }
  }
  myfile<<"Total"<<endl;
  myfile<<total_assertions;

  iFile.close();
  myfile.close();
}

int main(){
  string task = "insertsort";
  makeRecord(task);
}
