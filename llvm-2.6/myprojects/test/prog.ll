; ModuleID = 'prog.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32"
target triple = "i386-pc-linux-gnu"

@.str = private constant [2 x i8] c"a\00", align 1 ; <[2 x i8]*> [#uses=1]

define i32 @main() nounwind {
entry:
  %retval = alloca i32                            ; <i32*> [#uses=1]
  %d = alloca i32                                 ; <i32*> [#uses=4]
  %c = alloca i32                                 ; <i32*> [#uses=0]
  %b = alloca i32                                 ; <i32*> [#uses=2]
  %a = alloca i32                                 ; <i32*> [#uses=2]
  %"alloca point" = bitcast i32 0 to i32          ; <i32> [#uses=0]
  %a1 = bitcast i32* %a to i8*                    ; <i8*> [#uses=1]
  call void @klee_make_symbolic(i8* %a1, i32 4, i8* getelementptr inbounds ([2 x i8]* @.str, i32 0, i32 0)) nounwind
  store i32 0, i32* %d, align 4
  br label %bb5

bb:                                               ; preds = %bb5
  %0 = load i32* %a, align 4                      ; <i32> [#uses=1]
  %1 = icmp sgt i32 %0, 10                        ; <i1> [#uses=1]
  br i1 %1, label %bb2, label %bb3

bb2:                                              ; preds = %bb
  store i32 10, i32* %b, align 4
  br label %bb4

bb3:                                              ; preds = %bb
  store i32 11, i32* %b, align 4
  br label %bb4

bb4:                                              ; preds = %bb3, %bb2
  %2 = load i32* %d, align 4                      ; <i32> [#uses=1]
  %3 = add nsw i32 %2, 1                          ; <i32> [#uses=1]
  store i32 %3, i32* %d, align 4
  br label %bb5

bb5:                                              ; preds = %bb4, %entry
  %4 = load i32* %d, align 4                      ; <i32> [#uses=1]
  %5 = icmp sle i32 %4, 99                        ; <i1> [#uses=1]
  br i1 %5, label %bb, label %bb6

bb6:                                              ; preds = %bb5
  br label %return

return:                                           ; preds = %bb6
  %retval7 = load i32* %retval                    ; <i32> [#uses=1]
  ret i32 %retval7
}

declare void @klee_make_symbolic(i8*, i32, i8*)
