#include "klee.h"
#define __COUNT_CODE(flag_mem_blk, conflict) {conflict+=(flag_mem_blk==0)?1:0; flag_mem_blk=1;}
#define __COUNT_MACRO(flag_mem_blk, conflict)	__COUNT_CODE(flag_mem_blk, conflict)
/* __KLEE__ */ int C__800003=0;
/* __KLEE__ */ int C__7FFC03=0;
/* __KLEE__ */ char flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ char flag__0X7FFC03__0X800003=0;
/* __KLEE__ */ int C__800004=0;
/* __KLEE__ */ int C__7FFC04=0;
/* __KLEE__ */ char flag__0X800004__0X7FFC04=0;
/* __KLEE__ */ char flag__0X7FFC04__0X800004=0;
/* __KLEE__ */ int C__7FFC05=0;
/* __KLEE__ */ int C__800005=0;
/* __KLEE__ */ char flag__0X7FFC05__0X800005=0;
/* __KLEE__ */ char flag__0X800005__0X7FFC05=0;
/* __KLEE__ */ int C__7FFC0E=0;
/* __KLEE__ */ int C__80002E=0;
/* __KLEE__ */ char flag__0X7FFC0E__0X80002E=0;
/* __KLEE__ */ char flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ int C__7FFC00=0;
/* __KLEE__ */ int C__3FFFE00=0;
/* __KLEE__ */ char flag__0X7FFC00__0X3FFFE00=0;
/* __KLEE__ */ char flag__0X3FFFE00__0X7FFC00=0;
/* __KLEE__ */ int SCP_1=0;
typedef unsigned char uint8_t;

//typedef unsigned short uint16_t;

//typedef unsigned long uint32_t;

typedef signed char int8_t;

typedef signed short int16_t;

//typedef signed long int32_t;

typedef uint8_t bool_t;

struct point {
  float x;

  float y;

  float a;

};

static float alpha, leg;

typedef int16_t pprz_t;

struct inter_mcu_msg {
  int16_t channels[9];

  uint8_t ppm_cpt;

  uint8_t status;

  uint8_t nb_err;

  uint8_t vsupply;

};

float estimator_x;

float estimator_y;

float estimator_z;

float estimator_phi;

float estimator_psi;

float estimator_theta;

float estimator_x_dot;

float estimator_y_dot;

float estimator_z_dot;

float estimator_phi_dot;

float estimator_psi_dot;

float estimator_theta_dot;

uint16_t estimator_flight_time;

float estimator_t;

float estimator_hspeed_mod;

float estimator_hspeed_dir;

float estimator_rad_of_ir, estimator_ir, estimator_rad;

uint8_t pprz_mode = 0;

uint8_t vertical_mode = 0;

uint8_t lateral_mode = 0;

bool_t auto_pitch = 0;

bool_t rc_event_1, rc_event_2;

struct inter_mcu_msg from_fbw;

float desired_climb = 0., pre_climb = 0.;

float desired_roll = 0.;

float course_pgain = -0.2;

float desired_course = 0.;

float max_roll = 0.35;

uint8_t excpt_stage;

static float last_x, last_y;

static uint8_t last_wp;

float rc_pitch;

uint16_t stage_time, block_time;

float stage_time_ds;


bool_t approaching_static(uint8_t);

void fly_to_xy_static(float x, float y);

void fly_to_static(uint8_t wp);

void route_to_static(uint8_t last_wp, uint8_t wp);


static float qdr;

const int32_t nav_east0 = 605530;

const int32_t nav_north0 = 5797350;


float desired_altitude = 125. + 50.;

float desired_x, desired_y;

uint16_t nav_desired_gaz;

float nav_pitch = 0.;

float nav_desired_roll;

float dist2_to_wp, dist2_to_home;

bool_t too_far_from_home;

const uint8_t nb_waypoint = 7;

struct point waypoints[8] = { {0.0, 0.0, 200},{0.0, 0.0, 200},{115.0, -75.0, 200},{156.7, -41.7, 200},{115.0, 0.0, 200},{0.0, -75.0, 200},{-51.7, -36.7, 200},};


static float carrot;

uint32_t EIMSK;

uint32_t EIMSK_1;

uint32_t EIMSK_2;

uint32_t EIMSK_3;

uint8_t Iteration;

uint8_t tx_buf[1024];

uint8_t tx_byte;

uint8_t tx_byte_idx;

uint8_t ck_a, ck_b;

uint8_t modem_nb_ovrn;

uint8_t tx_head;

volatile uint8_t tx_tail;

uint32_t nav_stage, nav_block;

uint8_t excpt_stage;

static float last_x, last_y;

static uint8_t last_wp;

float rc_pitch;

uint16_t stage_time, block_time;

float stage_time_ds;



void fly_to_xy_static(float x, float y){
  desired_x = x;

  desired_y = y;



}void fly_to_static(uint8_t wp) {

  fly_to_xy_static(waypoints[wp].x, waypoints[wp].y);




}void route_to_static(uint8_t _last_wp, uint8_t wp) {

  float last_wp_x = waypoints[_last_wp].x;

  float last_wp_y = waypoints[_last_wp].y;

  float leg_x = waypoints[wp].x - last_wp_x;

  float leg_y = waypoints[wp].y - last_wp_y;

  float leg2 = leg_x * leg_x + leg_y * leg_y;

  alpha = ((estimator_x - last_wp_x) * leg_x + (estimator_y - last_wp_y) * leg_y) / leg2;

  alpha = (alpha > 0. ? alpha : 0.);


  alpha += (carrot / leg > 0. ? carrot / leg : 0.);

  alpha = (1. < alpha ? 1. : alpha);

  fly_to_xy_static(last_wp_x + alpha*leg_x, last_wp_y + alpha*leg_y);

}

void MODEM_LOAD_NEXT_BYTE(){

  tx_byte = tx_buf[tx_tail];

  tx_byte_idx = 0;

  tx_tail++;

  if( tx_tail >= 255 )tx_tail = 0;

}

void MODEM_CHECK_RUNNING(){

  if (Iteration == 1){ EIMSK = EIMSK_1;

  }else if (Iteration == 2){ EIMSK = EIMSK_2;

  }else  EIMSK = EIMSK_3;


  if (!(EIMSK & (1 << (0)))){

    MODEM_LOAD_NEXT_BYTE();

  }


}void DOWNLINK_SEND_NAVIGATION(void* cur_block, void* cur_stage, void* pos_x, void* pos_y, void* desired_course,void* dist2_wp, void* course_pgain, void* dist2_home) {
/* __KLEE__ */__COUNT_MACRO(flag__0X3FFFE00__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X3FFFE00=0;
/* __KLEE__ */ C__3FFFE00=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X3FFFE00__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X3FFFE00=0;
/* __KLEE__ */ C__3FFFE00=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X3FFFE00__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X3FFFE00=0;
/* __KLEE__ */ C__3FFFE00=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X3FFFE00__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X3FFFE00=0;
/* __KLEE__ */ C__3FFFE00=0;
  if ((tx_head>=tx_tail? 30 < (255 - (tx_head - tx_tail)) : 30 < (tx_tail - tx_head))) {
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;
    { 
      { 
        tx_buf[tx_head] = 0x05;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) 
          tx_head = 0;

      };

      { 
        tx_buf[tx_head] = 10;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };
      ck_a = 10;
      ck_b = 10;
    }
    { 
      tx_buf[tx_head] = *((uint8_t*)(cur_block));
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

      ck_a += *((uint8_t*)(cur_block));

      ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

      tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

      if (tx_head >= 255) tx_head = 0;

    };

    { 
      tx_buf[tx_head] = *((uint8_t*)(cur_stage));
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

      ck_a += *((uint8_t*)(cur_stage));

      ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

      tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

      if (tx_head >= 255) tx_head = 0;

    };

    { 
      { 
        tx_buf[tx_head] = *((uint8_t*)(pos_x));
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(pos_x));

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(pos_x)+1);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(pos_x)+1);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(pos_x)+2);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(pos_x)+2);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(pos_x)+3);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(pos_x)+3);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

    };

    { 
      { 
        tx_buf[tx_head] = *((uint8_t*)(pos_y));
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(pos_y));

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(pos_y)+1);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(pos_y)+1);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(pos_y)+2);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(pos_y)+2);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(pos_y)+3);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(pos_y)+3);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

    };

    { 
      { 
        tx_buf[tx_head] = *((uint8_t*)(desired_course));
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(desired_course));

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(desired_course)+1);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(desired_course)+1);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(desired_course)+2);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(desired_course)+2);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(desired_course)+3);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(desired_course)+3);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

    };

    { 
      { 
        tx_buf[tx_head] = *((uint8_t*)(dist2_wp));
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(dist2_wp));

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(dist2_wp)+1);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(dist2_wp)+1);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(dist2_wp)+2);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(dist2_wp)+2);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

      {
        tx_buf[tx_head] = *((uint8_t*)(dist2_wp)+3);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(dist2_wp)+3);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;


        if (tx_head >= 255) tx_head = 0;

      };

    };

    { 
      { 
        tx_buf[tx_head] = *((uint8_t*)(course_pgain));
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(course_pgain));

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(course_pgain)+1);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(course_pgain)+1);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(course_pgain)+2);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(course_pgain)+2);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(course_pgain)+3);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(course_pgain)+3);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

    };

    { 
      { 
        tx_buf[tx_head] = *((uint8_t*)(dist2_home));
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(dist2_home));

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(dist2_home)+1);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(dist2_home)+1);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(dist2_home)+2);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(dist2_home)+2);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = *((uint8_t*)(dist2_home)+3);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        ck_a += *((uint8_t*)(dist2_home)+3);

        ck_b += ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

    };

    { 
      { 
        tx_buf[tx_head] = ck_a;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      { 
        tx_buf[tx_head] = ck_b;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        tx_head++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

        if (tx_head >= 255) tx_head = 0;

      };

      MODEM_CHECK_RUNNING();

    
  
  }}else modem_nb_ovrn++;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC0E__0X80002E,C__80002E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X80002E__0X7FFC0E=0;
/* __KLEE__ */ C__7FFC0E=0;

}



void course_pid_run( void ) {

  float err = estimator_hspeed_dir - desired_course;


  //   { 
  //   while (err > 3.14159265358979323846) 
  //   err -= 2 * 3.14159265358979323846;

  //   while (err < -3.14159265358979323846) 
  //   err += 2 * 3.14159265358979323846;

  //   };


  nav_desired_roll = course_pgain * err;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 9){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X3FFFE00,C__3FFFE00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X3FFFE00__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

  if (nav_desired_roll > max_roll) nav_desired_roll = max_roll;

  else if (nav_desired_roll < -max_roll) nav_desired_roll = -max_roll;

}


void auto_nav_lib(void) {

  if(nav_block == 0) { 

    nav_block=0;

    if(nav_stage == 0) { 
      //label_while_1: 
      nav_stage=0;

      if (! (!(estimator_flight_time))) { 
        //goto label_endwhile_2;

        //label_endwhile_2: 
        nav_stage=2;


        if ((estimator_flight_time>8)) { 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        } 
      }
    }

    if (nav_stage == 1) { 
      nav_stage=1;

      { 
        //goto label_while_1;

        nav_stage=0;

        if (! (!(estimator_flight_time))) 
        { 
          //goto label_endwhile_2;

          //label_endwhile_2: 
          nav_stage=2;


          if ((estimator_flight_time>8)) { 
            nav_stage++;

            { 
              last_x = estimator_x;

              last_y = estimator_y;

              stage_time = 0;

              stage_time_ds = 0;

              return;

            } 
          }
        }
      }
    }


    if(nav_stage == 2) { 
      //label_endwhile_2: 
      nav_stage=2;


      if ((estimator_flight_time>8)) { 
        nav_stage++;

        { 
          last_x = estimator_x;

          last_y = estimator_y;

          stage_time = 0;

          stage_time_ds = 0;

          return;

        } 
      }
      if(estimator_flight_time<=8){
        desired_course = ((270.0)/180. * 3.14159265358979323846);

        auto_pitch = 0;

        nav_pitch = 0.150000;

        vertical_mode = 1;

        nav_desired_gaz = (0.800000*(600 * 16) < 0 ? 0 : (0.800000*(600 * 16) > (600 * 16) ? (600 * 16) : 0.800000*(600 * 16)));

      }
      return;

    }	

    if(nav_stage == 3) { 

      nav_stage=3;


      if ((estimator_z>150.)) { 

        nav_stage++;


        last_x = estimator_x;

        last_y = estimator_y;

        stage_time = 0;

        stage_time_ds = 0;

        return;

      } 
      //if ((estimator_z<=150.)) { 
      desired_course = ((270.0)/180. * 3.14159265358979323846);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 2;

      desired_climb = 8.000000;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;

      // }
      return;

    }	

    if(nav_stage == 4) { 
      nav_stage=4;

      { 
        nav_block++;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      }
    }
  }else if(nav_block == 1) { 
    nav_block=1;

    if (rc_event_1 ? rc_event_1 = 0, (!0) : 0) { 
      { 
        nav_block=2;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      } 
    }
    if(nav_stage == 0) { 
      //label_while_3: 
      nav_stage=0;

      if (((!0))) { 
        nav_stage++;


        last_x = estimator_x;

        last_y = estimator_y;

        stage_time = 0;

        stage_time_ds = 0;

        return;


      }
    };


    if (nav_stage == 1) { 

      nav_stage=1;

      if (approaching_static(1)) { 

        last_wp = 1;

        { 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        } 
      } 
      fly_to_static(1);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[1].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800003__0X7FFC03,C__7FFC03);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X7FFC03__0X800003=0;
/* __KLEE__ */ C__800003=0;

      pre_climb = 0.;

      return;

    }

    if (nav_stage == 2) { 

      nav_stage=2;

      if (approaching_static(4)) { 

        last_wp = 4;


        nav_stage++;


        last_x = estimator_x;

        last_y = estimator_y;

        stage_time = 0;

        stage_time_ds = 0;

        return;



      } 
      // else {
      fly_to_static(4);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[4].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800004__0X7FFC04,C__7FFC04);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X7FFC04__0X800004=0;
/* __KLEE__ */ C__800004=0;

      pre_climb = 0.;

      //}
      return;

    }	

    if (nav_stage == 3) { 

      nav_stage=3;

      { 
        //goto label_while_3;

        //label_while_3: 
        nav_stage=0;

        if ((!0)) { 
          //goto label_endwhile_4;

          //label_endwhile_4: 
          nav_stage++;

          last_x = estimator_x;

          last_y = estimator_y;

          stage_time = 0;

          stage_time_ds = 0;

          return;

        }
      }
    }

    if (nav_stage == 4) { 
      //label_endwhile_4: 
      nav_stage=4;

      { 
        nav_block++;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      }}
    
  } else if(nav_block == 2) { 
    nav_block=2;


    if (rc_event_1 ? rc_event_1 = 0, (!0) : 0) { 
      { 
        nav_block=3;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      } 
    }

    if(nav_stage == 0) { 
      //label_while_5
      nav_stage=0;


      if ((!0)) { 
        //goto label_endwhile_6;

        //label_endwhile_6
        nav_stage++;

        { 
          last_x = estimator_x;

          last_y = estimator_y;

          stage_time = 0;

          stage_time_ds = 0;

          return;

        } 
      }
    };


    if (nav_stage == 1) { 
      nav_stage=1;


      if (approaching_static(6)) { 
        last_wp = 6;

        { 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        } 
      }
      fly_to_static(6);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[6].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800005__0X7FFC05,C__7FFC05);
/* __KLEE__ */klee_assert( C__7FFC05 <= 0 | C__800005 <= 0 );
/* __KLEE__ */ flag__0X7FFC05__0X800005=0;
/* __KLEE__ */ C__800005=0;

      pre_climb = 0.;


      return;

    }	

    if(nav_stage == 2) { 
      nav_stage=2;

      if (approaching_static(1)) { 
        last_wp = 1;

        { 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        } 
      }
      fly_to_static(1);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[1].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800003__0X7FFC03,C__7FFC03);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X7FFC03__0X800003=0;
/* __KLEE__ */ C__800003=0;

      pre_climb = 0.;


      return;

    }	
    if (nav_stage == 3) { 
      nav_stage=3;

      if (approaching_static(2)) { 
        last_wp = 2;

        { 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        } 
      }
      route_to_static(last_wp, 2);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[2].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800004__0X7FFC04,C__7FFC04);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X7FFC04__0X800004=0;
/* __KLEE__ */ C__800004=0;

      pre_climb = 0.;


      return;

    }	
    if (nav_stage == 4) { 
      nav_stage=4;

      if (approaching_static(3)) 
      { 
        last_wp = 3;

        { 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        } 
      }
      fly_to_static(3);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[3].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800004__0X7FFC04,C__7FFC04);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X7FFC04__0X800004=0;
/* __KLEE__ */ C__800004=0;

      pre_climb = 0.;


      return;

    }	
    if (nav_stage == 5){ 
      nav_stage=5;

      if (approaching_static(4)) 
      { 
        last_wp = 4;

        { 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        } 
      }
      fly_to_static(4);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[4].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800004__0X7FFC04,C__7FFC04);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X7FFC04__0X800004=0;
/* __KLEE__ */ C__800004=0;

      pre_climb = 0.;


      return;

    }	

    if (nav_stage == 6) { 
      nav_stage=6;

      if (approaching_static(5)) 
      { 
        last_wp = 5;

        { 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        } 
      } 
      route_to_static(last_wp, 5);

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[5].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800005__0X7FFC05,C__7FFC05);
/* __KLEE__ */klee_assert( C__7FFC05 <= 0 | C__800005 <= 0 );
/* __KLEE__ */ flag__0X7FFC05__0X800005=0;
/* __KLEE__ */ C__800005=0;

      pre_climb = 0.;


      return;

    }

    if (nav_stage == 7) { 

      nav_stage=7;


      { 
        //goto label_while_5;

        //label_while_5: 
        nav_stage=0;


        if ((!0)) { 
          // goto label_endwhile_6;

          // label_endwhile_6: 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        }
      }
    }

    if (nav_stage == 8) { 
      //label_endwhile_6: 
      nav_stage=8;

      { 
        nav_block++;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      }
    }

  }else if (nav_block == 3) { 
  

    nav_block=3;

    if (rc_event_1 ? rc_event_1 = 0, (!0) : 0) 
    { 
      { 
        nav_block=4;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      } 
    }

    if (nav_stage == 0) { 

      nav_stage=0;


      { 
        static float carrot_x, carrot_y;

        int16_t pitch;

        int16_t roll;

        if (pprz_mode == 2) { 
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 9){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X3FFFE00,C__3FFFE00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X3FFFE00__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          int16_t yaw = from_fbw.channels[3];
/* __KLEE__ */__COUNT_MACRO(flag__0X80002E__0X7FFC0E,C__7FFC0E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X7FFC0E__0X80002E=0;
/* __KLEE__ */ C__80002E=0;

          if (yaw > ((int16_t)((600 * 16) * 0.05)) || yaw < -((int16_t)((600 * 16) * 0.05))) { 

            carrot_x += ((float)yaw / (float)(600 * 16) * -20. + 0);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;

            carrot_x = (carrot_x < 250. ? carrot_x : 250.);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;

            carrot_x = (carrot_x > -250. ? carrot_x : -250.);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC04__0X800004,C__800004);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X800004__0X7FFC04=0;
/* __KLEE__ */ C__7FFC04=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC04__0X800004,C__800004);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X800004__0X7FFC04=0;
/* __KLEE__ */ C__7FFC04=0;

          } 

          pitch = from_fbw.channels[2];
/* __KLEE__ */__COUNT_MACRO(flag__0X80002E__0X7FFC0E,C__7FFC0E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X7FFC0E__0X80002E=0;
/* __KLEE__ */ C__80002E=0;


          if (pitch > ((int16_t)((600 * 16) * 0.05)) || pitch < -((int16_t)((600 * 16) * 0.05))) { 

            carrot_y += ((float)pitch / (float)(600 * 16) * -20. + 0);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;

            carrot_y = (carrot_y < 250. ? carrot_y : 250.);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;

            carrot_y = (carrot_y > -250. ? carrot_y : -250.);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC04__0X800004,C__800004);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X800004__0X7FFC04=0;
/* __KLEE__ */ C__7FFC04=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC04__0X800004,C__800004);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X800004__0X7FFC04=0;
/* __KLEE__ */ C__7FFC04=0;
          } 
          vertical_mode = 3;

          roll = from_fbw.channels[1];
/* __KLEE__ */__COUNT_MACRO(flag__0X80002E__0X7FFC0E,C__7FFC0E);
/* __KLEE__ */klee_assert( C__7FFC0E <= 0 | C__80002E <= 0 );
/* __KLEE__ */ flag__0X7FFC0E__0X80002E=0;
/* __KLEE__ */ C__80002E=0;

          if (roll > ((int16_t)((600 * 16) * 0.05)) || roll < -((int16_t)((600 * 16) * 0.05))) { 
            desired_altitude += ((float)roll / (float)(600 * 16) * -1.0 + 0);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC03__0X800003,C__800003);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X800003__0X7FFC03=0;
/* __KLEE__ */ C__7FFC03=0;

            desired_altitude = (desired_altitude > 50. +125. ? desired_altitude : 50. +125.);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC05__0X800005,C__800005);
/* __KLEE__ */klee_assert( C__7FFC05 <= 0 | C__800005 <= 0 );
/* __KLEE__ */ flag__0X800005__0X7FFC05=0;
/* __KLEE__ */ C__7FFC05=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC04__0X800004,C__800004);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X800004__0X7FFC04=0;
/* __KLEE__ */ C__7FFC04=0;

            desired_altitude = (desired_altitude < 150. +125. ? desired_altitude : 150. +125.);
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC05__0X800005,C__800005);
/* __KLEE__ */klee_assert( C__7FFC05 <= 0 | C__800005 <= 0 );
/* __KLEE__ */ flag__0X800005__0X7FFC05=0;
/* __KLEE__ */ C__7FFC05=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC05__0X800005,C__800005);
/* __KLEE__ */klee_assert( C__7FFC05 <= 0 | C__800005 <= 0 );
/* __KLEE__ */ flag__0X800005__0X7FFC05=0;
/* __KLEE__ */ C__7FFC05=0;

          } 
        } 
      }

      return;

    }

    if (nav_stage == 1) { 
      nav_stage=1;


      { 
        nav_block++;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      }

    }
  }else if (nav_block == 4) {
   
    nav_block=4;

    if (rc_event_1 ? rc_event_1 = 0, (!0) : 0) 
    { 
      { 
        nav_block=5;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      } 
    }

    if (nav_stage == 0) { 
      nav_stage=0;

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[0].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800003__0X7FFC03,C__7FFC03);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X7FFC03__0X800003=0;
/* __KLEE__ */ C__800003=0;

      pre_climb = 0.;

      return;

    }	
    if (nav_stage == 1) { 
      nav_stage=1;

      { 
        nav_block++;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      }

    }
  }else if (nav_block == 5) { 
  
    nav_block=5;


    if (rc_event_1 ? rc_event_1 = 0, (!0) : 0) 
    { 
      { 
        nav_block=1;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      } 
    }


    if (nav_stage == 0) 
    { 
      //label_while_7: 
      nav_stage=0;


      if ((!0)) 
      { 
        //goto label_endwhile_8;

        //label_endwhile_8: 
        nav_stage++;

        { 
          last_x = estimator_x;

          last_y = estimator_y;

          stage_time = 0;

          stage_time_ds = 0;

          return;

        } 
      }
    };


    if (nav_stage == 1) { 
      nav_stage=1;

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[1].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800003__0X7FFC03,C__7FFC03);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X7FFC03__0X800003=0;
/* __KLEE__ */ C__800003=0;

      pre_climb = 0.;


      if (((0 < 350 ? 0 : 350) < qdr && qdr < 0 +10)) { 
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC05__0X800005,C__800005);
/* __KLEE__ */klee_assert( C__7FFC05 <= 0 | C__800005 <= 0 );
/* __KLEE__ */ flag__0X800005__0X7FFC05=0;
/* __KLEE__ */ C__7FFC05=0;
        nav_stage++;

        { 
          last_x = estimator_x;

          last_y = estimator_y;

          stage_time = 0;

          stage_time_ds = 0;

          return;

        } 
      };

      return;

    }	

    if (nav_stage == 2) { nav_stage=2;

      auto_pitch = 0;

      nav_pitch = 0.000000;

      vertical_mode = 3;

      desired_altitude = waypoints[4].a;
/* __KLEE__ */__COUNT_MACRO(flag__0X800004__0X7FFC04,C__7FFC04);
/* __KLEE__ */klee_assert( C__800004 <= 0 | C__7FFC04 <= 0 );
/* __KLEE__ */ flag__0X7FFC04__0X800004=0;
/* __KLEE__ */ C__800004=0;

      pre_climb = 0.;


      if (((180 < 350 ? 180 : 350) < qdr && qdr < 180 +10)) { 
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC05__0X800005,C__800005);
/* __KLEE__ */klee_assert( C__7FFC05 <= 0 | C__800005 <= 0 );
/* __KLEE__ */ flag__0X800005__0X7FFC05=0;
/* __KLEE__ */ C__7FFC05=0;
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC05__0X800005,C__800005);
/* __KLEE__ */klee_assert( C__7FFC05 <= 0 | C__800005 <= 0 );
/* __KLEE__ */ flag__0X800005__0X7FFC05=0;
/* __KLEE__ */ C__7FFC05=0;
        nav_stage++;
        { 
          last_x = estimator_x;

          last_y = estimator_y;

          stage_time = 0;

          stage_time_ds = 0;

          return;

        } 
      };

      return;


    }	

    if (nav_stage == 3) { 
      nav_stage=3;

      { 
        // goto label_while_7;

        //label_while_7: 
        nav_stage=0;


        if ((!0)) 
        { 
          //  goto label_endwhile_8;

          //label_endwhile_8: 
          nav_stage++;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          } 
        }
      }
    }

    if (nav_stage == 4) { 
      //label_endwhile_8: 
      nav_stage=4;

      { 
        nav_block++;

        { 
          nav_stage = 0;

          block_time = 0;

          { 
            last_x = estimator_x;

            last_y = estimator_y;

            stage_time = 0;

            stage_time_ds = 0;

            return;

          };

        };

      }
    }
  }
}



bool_t approaching_static(uint8_t wp) {

  float pw_x = waypoints[wp].x - estimator_x;

  float pw_y = waypoints[wp].y - estimator_y;

  float scal_prod;


  dist2_to_wp = pw_x*pw_x + pw_y *pw_y;

  carrot = 5. * estimator_hspeed_mod;

  carrot = (carrot < 40 ? 40 : carrot);


  if (dist2_to_wp < carrot*carrot)
    return (!0);


  scal_prod = (waypoints[wp].x - last_x) * pw_x + (waypoints[wp].y - last_y) * pw_y;

  return (scal_prod < 0);


}

void compute_dist2_to_home_static(void) {
  float ph_x = waypoints[0].x - estimator_x;
/* __KLEE__ */__COUNT_MACRO(flag__0X800003__0X7FFC03,C__7FFC03);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X7FFC03__0X800003=0;
/* __KLEE__ */ C__800003=0;

  float ph_y = waypoints[0].y - estimator_y;
/* __KLEE__ */__COUNT_MACRO(flag__0X800003__0X7FFC03,C__7FFC03);
/* __KLEE__ */klee_assert( C__800003 <= 0 | C__7FFC03 <= 0 );
/* __KLEE__ */ flag__0X7FFC03__0X800003=0;
/* __KLEE__ */ C__800003=0;

  dist2_to_home = ph_x*ph_x + ph_y *ph_y;

  too_far_from_home = dist2_to_home > (500.*500.);

}

void nav_home(void) {

  nav_pitch = 0.;

  vertical_mode = 3;

  desired_altitude = 125. +50;


  compute_dist2_to_home_static();

  dist2_to_wp = dist2_to_home;



}void nav_update(void) {

  compute_dist2_to_home_static();

  auto_nav_lib();

}

int main()
{
  char str[256];
/* __KLEE__ */SCP_1 = 0;

  uint32_t cur_block;

  int Iter = 0;
      klee_make_symbolic(&nav_stage, sizeof(nav_stage), "nav_stage");
          klee_make_symbolic(&nav_block, sizeof(nav_block), "nav_block");

  while (Iter<10){

    lateral_mode = 3;

    if (pprz_mode == 3){
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 9){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X3FFFE00,C__3FFFE00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X3FFFE00__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

      nav_home();
    }

    else{
      nav_update();
    }


    cur_block = nav_block;


    DOWNLINK_SEND_NAVIGATION(&cur_block, &nav_stage, &estimator_x, &estimator_y, &desired_course, &dist2_to_wp, &course_pgain, &dist2_to_home);


    if (pprz_mode == 2 || pprz_mode == 3){
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 9){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X3FFFE00,C__3FFFE00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X3FFFE00__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

      if (lateral_mode >= 3) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 9){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X3FFFE00,C__3FFFE00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__3FFFE00 <= 0 );
/* __KLEE__ */ flag__0X3FFFE00__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

        course_pid_run();
      }

      desired_roll = nav_desired_roll;

    }

    Iter++;

/* __KLEE__ */SCP_1++;
  }

}
