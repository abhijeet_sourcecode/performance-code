#include "klee.h"
#define __COUNT_CODE(flag_mem_blk, conflict) {conflict+=(flag_mem_blk==0)?1:0; flag_mem_blk=1;}
#define __COUNT_MACRO(flag_mem_blk, conflict)	__COUNT_CODE(flag_mem_blk, conflict)
/* __KLEE__ */ int C__7FFC00=0;
/* __KLEE__ */ int C__800020=0;
/* __KLEE__ */ char flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ char flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ int SCP_1=0;
#ifdef DO_TRACING   // ON PC

#include <stdio.h>
#define TRACE(x) trace((x))
#undef TEST                   /* finished testing! */
void trace(char *s)
{
   printf("%s\n",s);
}

#else               // ON TARGET

#define TRACE(x)
#undef TEST

#endif



volatile int	P1_is_marked = 3;
volatile long	P1_marking_member_0[3];
volatile int	P2_is_marked = 5;
volatile long	P2_marking_member_0[5];
volatile int	P3_is_marked = 0;
volatile long	P3_marking_member_0[6];



/**void NSicherNeu()**/
int main()
{
   int dummy_i;
/*   dummy_i = 17; Takes too much time */
   dummy_i = 5;
			klee_make_symbolic(&P1_is_marked, sizeof(P1_is_marked), "P1_is_marked");
			klee_make_symbolic(&P2_is_marked, sizeof(P2_is_marked), "P2_is_marked");
			klee_make_symbolic(&P3_is_marked, sizeof(P3_is_marked), "P3_is_marked");


/* __KLEE__ */SCP_1 = 0;
   while (dummy_i > 0) {

      dummy_i--;
      /* Permutation for Place P1 : 0, 1, 2 */
      /* Transition T1 */
      if ( (P1_is_marked >= 3) && (P3_is_marked + 3 <= 6) && (P1_marking_member_0[1] == P1_marking_member_0[2]) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         long	x;
         long	y;
         long	z;

         x = P1_marking_member_0[0];
         y = P1_marking_member_0[1];

         /* Transition condition */
         if (x < y) {

				/* demarking of input places */
            P1_is_marked -= 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            z = x - y;
				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = x;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = y;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = z;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P1 : 0, 2, 1 */
      /* Transition T1 */
      if ( (P1_is_marked >= 3) && (P3_is_marked + 3 <= 6) && (P1_marking_member_0[2] == P1_marking_member_0[1]) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           

         long	x;
         long	y;
         long	z;

         x = P1_marking_member_0[0];
         y = P1_marking_member_0[2];

         /* Transition condition */
         if ((x < y)) {


				/* demarking of input places */
            P1_is_marked -= 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            z = x - y;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = x;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = y;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = z;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P1 : 1, 0, 2 */
      /* Transition T1 */
      if ( (P1_is_marked >= 3) && (P3_is_marked + 3 <= 6) && (P1_marking_member_0[0] == P1_marking_member_0[2]) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           

         long	x;
         long	y;
         long	z;

         x = P1_marking_member_0[1];
         y = P1_marking_member_0[0];

         /* Transition condition */
         if (x < y) {


				/* demarking of input places */
            P1_is_marked -= 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            z = x - y;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = x;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = y;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = z;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P1 : 1, 2, 0 */
      /* Transition T1 */
      if ( (P1_is_marked >= 3) && (P3_is_marked + 3 <= 6) && (P1_marking_member_0[2] == P1_marking_member_0[0])) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           

         long	x;
         long	y;
         long	z;

         x = P1_marking_member_0[1];
         y = P1_marking_member_0[2];
					 

         /* Transition condition */
         if ((x < y)) {


				/* demarking of input places */
            P1_is_marked -= 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            z = x - y;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = x;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = y;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = z;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P1 : 2, 0, 1 */
      /* Transition T1 */
      if ( (P1_is_marked >= 3) && (P3_is_marked + 3 <= 6) && (P1_marking_member_0[0] == P1_marking_member_0[1]) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
         long	x;
         long	y;
         long	z;

         x = P1_marking_member_0[2];
         y = P1_marking_member_0[0];
					 

         /* Transition condition */
         if ((x < y)) {

				/* demarking of input places */
            P1_is_marked -= 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            z = x - y;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = x;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = y;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = z;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P1 : 2, 1, 0 */
      /* Transition T1 */
      if ( (P1_is_marked >= 3) && (P3_is_marked + 3 <= 6) && (P1_marking_member_0[1] == P1_marking_member_0[0]) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
         long	x;
         long	y;
         long	z;

         x = P1_marking_member_0[2];
         y = P1_marking_member_0[1];
					 

         /* Transition condition */
         if ((x < y)) {

				/* demarking of input places */
            P1_is_marked -= 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            z = x - y;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = x;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = y;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = z;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 0, 1, 2, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && (((P3_is_marked + 3) <= 6)) && ( ((P2_marking_member_0[1] == P2_marking_member_0[2])) && ((P2_marking_member_0[1] == P2_marking_member_0[3])) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[1];
					 

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 0, 1, 3, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && (((P3_is_marked + 3) <= 6)) &&  ( (P2_marking_member_0[1] == P2_marking_member_0[3]) && (P2_marking_member_0[1] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 0, 2, 1, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[2] == P2_marking_member_0[1]) && (P2_marking_member_0[2] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 0, 2, 3, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[2] == P2_marking_member_0[3]) && (P2_marking_member_0[2] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 0, 3, 1, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[3] == P2_marking_member_0[1]) && (P2_marking_member_0[3] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 0, 3, 2, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[3] == P2_marking_member_0[2]) && (P2_marking_member_0[3] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 1, 0, 2, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[0] == P2_marking_member_0[2]) && (P2_marking_member_0[0] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 1, 0, 3, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) &&  ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[0] == P2_marking_member_0[3]) &&  (P2_marking_member_0[0] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 1, 2, 0, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[2] == P2_marking_member_0[0]) && (P2_marking_member_0[2] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 1, 2, 3, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[2] == P2_marking_member_0[3]) && (P2_marking_member_0[2] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
             
           
           
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {
				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 1, 3, 0, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) &&  ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[3] == P2_marking_member_0[0]) && (P2_marking_member_0[3] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {
				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 3, 2, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[3] == P2_marking_member_0[2]) && (P2_marking_member_0[3] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 0, 1, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[0] == P2_marking_member_0[1]) && (P2_marking_member_0[0] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {
				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 2, 0, 3, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[0] == P2_marking_member_0[3]) && (P2_marking_member_0[0] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {
				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 2, 1, 0, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[1] == P2_marking_member_0[0]) && (P2_marking_member_0[1] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {
				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 2, 1, 3, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[1] == P2_marking_member_0[3]) && (P2_marking_member_0[1] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {
				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 2, 3, 0, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) &&  ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[3] == P2_marking_member_0[0]) && (P2_marking_member_0[3] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {
				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 2, 3, 1, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) &&  ( (P2_marking_member_0[3] == P2_marking_member_0[1]) && (P2_marking_member_0[3] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {
				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }

      /* Permutation for Place P2 : 3, 0, 1, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ((P3_is_marked + 3) <= 6) && ( (P2_marking_member_0[0] == P2_marking_member_0[1]) && (P2_marking_member_0[0] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 0, 2, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[0] == P2_marking_member_0[2]) && ( P2_marking_member_0[0] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 1, 0, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[1] == P2_marking_member_0[0]) && ( P2_marking_member_0[1] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 1, 2, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) &&  ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[1] == P2_marking_member_0[2]) && ( P2_marking_member_0[1] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 2, 0, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[2] == P2_marking_member_0[0]) && ( P2_marking_member_0[2] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 2, 1, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 4) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[2] == P2_marking_member_0[1]) && ( P2_marking_member_0[2] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 1, 2, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[1] == P2_marking_member_0[2]) &&  ( P2_marking_member_0[1] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[3];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 1, 3, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[1] == P2_marking_member_0[3]) && ( P2_marking_member_0[1] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[2];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 1, 4, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[1] == P2_marking_member_0[4]) &&  ( P2_marking_member_0[1] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[3];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 1, 4, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[1] == P2_marking_member_0[4]) && ( P2_marking_member_0[1] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[2];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 2, 1, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[2] == P2_marking_member_0[1]) &&  ( P2_marking_member_0[2] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[3];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 2, 3, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[2] == P2_marking_member_0[3]) && ( P2_marking_member_0[2] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[1];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 2, 4, 1 */
      /* Transition T2 */ 
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  ( ( P2_marking_member_0[2] == P2_marking_member_0[4]) && ( P2_marking_member_0[2] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[3];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 2, 4, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[2] == P2_marking_member_0[4]) && ( P2_marking_member_0[2] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[1];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 3, 1, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  ( ( P2_marking_member_0[3] == P2_marking_member_0[1]) && ( P2_marking_member_0[3] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[2];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 3, 2, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[3] == P2_marking_member_0[2]) && ( P2_marking_member_0[3] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[1];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 3, 4, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[3] == P2_marking_member_0[4]) && ( P2_marking_member_0[3] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[2];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 3, 4, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[3] == P2_marking_member_0[4]) && ( P2_marking_member_0[3] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[1];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 4, 1, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[4] == P2_marking_member_0[1]) && ( P2_marking_member_0[4] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[3];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 4, 1, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[4] == P2_marking_member_0[1]) && ( P2_marking_member_0[4] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[2];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 4, 2, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[4] == P2_marking_member_0[2]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[3];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 4, 2, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[4] == P2_marking_member_0[2]) && ( P2_marking_member_0[4] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
             

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[1];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 4, 3, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[4] == P2_marking_member_0[3]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[2];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 0, 4, 3, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && ( ( P2_marking_member_0[4] == P2_marking_member_0[3]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[0];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
            P2_marking_member_0[0] = P2_marking_member_0[1];
            P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
            c = a + b;

				/* marking of output places */
            P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 0, 2, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[2]) && ( P2_marking_member_0[0] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 0, 3, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[3]) && ( P2_marking_member_0[0] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 0, 4, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[4]) && ( P2_marking_member_0[0] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 0, 4, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[4]) && ( P2_marking_member_0[0] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 2, 0, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[0]) && ( P2_marking_member_0[2] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 2, 3, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[2] == P2_marking_member_0[3]) && ( P2_marking_member_0[2] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 2, 4, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[4]) && ( P2_marking_member_0[2] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 2, 4, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[2] == P2_marking_member_0[4]) &&   ( P2_marking_member_0[2] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
          
          

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 3, 0, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[0]) && ( P2_marking_member_0[3] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 3, 2, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[2]) &&  ( P2_marking_member_0[3] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 3, 4, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[4]) && ( P2_marking_member_0[3] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 3, 4, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[3] == P2_marking_member_0[4]) &&  ( P2_marking_member_0[3] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 4, 0, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[0]) && ( P2_marking_member_0[4] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 4, 0, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[0]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 4, 2, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[4] == P2_marking_member_0[2]) && ( P2_marking_member_0[4] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 4, 2, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[2]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 4, 3, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[4] == P2_marking_member_0[3]) && ( P2_marking_member_0[4] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 1, 4, 3, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[3]) && ( P2_marking_member_0[4] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[1];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 0, 1, 4 */
      /* Transition T2 */ 
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[1]) && ( P2_marking_member_0[0] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 0, 3, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[3]) && ( P2_marking_member_0[0] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 0, 4, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[4]) && ( P2_marking_member_0[0] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 0, 4, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[4]) && ( P2_marking_member_0[0] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 1, 0, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[0]) && ( P2_marking_member_0[1] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 1, 3, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[1] == P2_marking_member_0[3]) &&  ( P2_marking_member_0[1] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 1, 4, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[4]) &&  ( P2_marking_member_0[1] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 1, 4, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[4]) &&  ( P2_marking_member_0[1] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 3, 0, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[3] == P2_marking_member_0[0]) && ( P2_marking_member_0[3] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 3, 1, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[1]) &&  ( P2_marking_member_0[3] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 3, 4, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[4]) && ( P2_marking_member_0[3] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 3, 4, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[3] == P2_marking_member_0[4]) && ( P2_marking_member_0[3] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 4, 0, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[0]) && ( P2_marking_member_0[4] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 4, 0, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[0]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 4, 1, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[1]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 4, 1, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[1]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 4, 3, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[3]) && ( P2_marking_member_0[4] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 2, 4, 3, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[3]) && ( P2_marking_member_0[4] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[2];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 0, 1, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[1]) && ( P2_marking_member_0[0] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 0, 2, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[2]) && ( P2_marking_member_0[0] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 0, 4, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[4]) && ( P2_marking_member_0[0] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 0, 4, 2 */
      /* Transition T2 */ 
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[4]) &&  ( P2_marking_member_0[0] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 1, 0, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[0]) && ( P2_marking_member_0[1] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 1, 2, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[2]) && ( P2_marking_member_0[1] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 1, 4, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[4]) && ( P2_marking_member_0[1] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 1, 4, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[4]) &&  ( P2_marking_member_0[1] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 2, 0, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[0]) && ( P2_marking_member_0[2] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 2, 1, 4 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[2] == P2_marking_member_0[1]) && ( P2_marking_member_0[2] == P2_marking_member_0[4]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 2, 4, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[4]) && ( P2_marking_member_0[2] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 2, 4, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[4]) && ( P2_marking_member_0[2] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 4, 0, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[0]) && ( P2_marking_member_0[4] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 4, 0, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[0]) && ( P2_marking_member_0[4] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 4, 1, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&   ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[4] == P2_marking_member_0[1]) &&  ( P2_marking_member_0[4] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         
          
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 4, 1, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[1]) && ( P2_marking_member_0[4] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 4, 2, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[4] == P2_marking_member_0[2]) && ( P2_marking_member_0[4] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 3, 4, 2, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[4] == P2_marking_member_0[2]) && ( P2_marking_member_0[4] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[3];
         b = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 0, 1, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&   (( P2_marking_member_0[0] == P2_marking_member_0[1]) &&  ( P2_marking_member_0[0] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
         
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 0, 1, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[1]) && ( P2_marking_member_0[0] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 0, 2, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[0] == P2_marking_member_0[2]) && ( P2_marking_member_0[0] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 0, 2, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[2]) && ( P2_marking_member_0[0] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 0, 3, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[3]) && ( P2_marking_member_0[0] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 0, 3, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[0] == P2_marking_member_0[3]) && ( P2_marking_member_0[0] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[0];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 1, 0, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[0]) && ( P2_marking_member_0[1] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 1, 0, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[0]) &&  ( P2_marking_member_0[1] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 1, 2, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[2]) && ( P2_marking_member_0[1] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
            
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 1, 2, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[2]) && ( P2_marking_member_0[1] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

           
           
            
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 1, 3, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[3]) && ( P2_marking_member_0[1] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 1, 3, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[1] == P2_marking_member_0[3]) &&  ( P2_marking_member_0[1] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

          
           
           
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[1];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 2, 0, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[0]) &&  ( P2_marking_member_0[2] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 2, 0, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[0]) && ( P2_marking_member_0[2] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 2, 1, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[1]) && ( P2_marking_member_0[2] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

           
           
            
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[3];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 2, 1, 3 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[1]) &&   ( P2_marking_member_0[2] == P2_marking_member_0[3]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
          

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 2, 3, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&   ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[3]) &&  ( P2_marking_member_0[2] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 2, 3, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[2] == P2_marking_member_0[3]) && ( P2_marking_member_0[2] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

          
           
            
         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[2];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 3, 0, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[0]) &&  ( P2_marking_member_0[3] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 3, 0, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[0]) && ( P2_marking_member_0[3] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 3, 1, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[1]) &&  ( P2_marking_member_0[3] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[2];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 3, 1, 2 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[3] == P2_marking_member_0[1]) && ( P2_marking_member_0[3] == P2_marking_member_0[2]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 3, 2, 0 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) && ( (P3_is_marked + 3) <= 6) &&  (( P2_marking_member_0[3] == P2_marking_member_0[2]) && ( P2_marking_member_0[3] == P2_marking_member_0[0]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
           
          
            

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_marking_member_0[0] = P2_marking_member_0[1];
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }


      /* Permutation for Place P2 : 4, 3, 2, 1 */
      /* Transition T2 */
      if ( (P2_is_marked >= 5) &&  ( (P3_is_marked + 3) <= 6) && (( P2_marking_member_0[3] == P2_marking_member_0[2]) &&  ( P2_marking_member_0[3] == P2_marking_member_0[1]) ) ) {
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
          
           
           

         long	a;
         long	b;
         long	c;

         a = P2_marking_member_0[4];
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X800020__0X7FFC00,C__7FFC00);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X7FFC00__0X800020=0;
/* __KLEE__ */ C__800020=0;
/* __KLEE__ */}
         b = P2_marking_member_0[3];

         /* Transition condition */
         if ((b > a)) {

				/* demarking of input places */
         P2_is_marked -= 4;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

				/* preaction */
         c = a + b;

				/* marking of output places */
         P3_marking_member_0[P3_is_marked+0] = a;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+1] = b;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_marking_member_0[P3_is_marked+2] = c;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}
         P3_is_marked += 3;
/* __KLEE__ */if(SCP_1 >= 0 && SCP_1<= 1){
/* __KLEE__ */__COUNT_MACRO(flag__0X7FFC00__0X800020,C__800020);
/* __KLEE__ */klee_assert( C__7FFC00 <= 0 | C__800020 <= 0 );
/* __KLEE__ */ flag__0X800020__0X7FFC00=0;
/* __KLEE__ */ C__7FFC00=0;
/* __KLEE__ */}

         } /* end of if (Transition condition) */
      }
/* __KLEE__ */SCP_1++;
   }
	dummy_i = 77;

}

/***************************************************************************
 *
 * end of file
 *
 ***************************************************************************/

