int a;
int a1,a2,a3,a4,a5,a6,a7,a8;
#define NO_OF_ITERATIONS 4

int main()
{
				int x, i;
				klee_make_symbolic(&a1, sizeof(a1), "a1");	
				klee_make_symbolic(&a2, sizeof(a2), "a2");	
				klee_make_symbolic(&a3, sizeof(a3), "a3");	
				klee_make_symbolic(&a4, sizeof(a4), "a4");	
				klee_make_symbolic(&a5, sizeof(a5), "a5");	
				klee_make_symbolic(&a6, sizeof(a6), "a6");	
				klee_make_symbolic(&a7, sizeof(a7), "a7");	
				klee_make_symbolic(&a8, sizeof(a8), "a8");	

				for (i = 0; i < NO_OF_ITERATIONS; i++) {
						if (i == 0)
								a = a1;
						if (i == 1)
								a = a2;
						if (i == 2)
								a = a3;
						if (i == 3)
								a = a4;
						if (i == 4)
								a = a5;
						if (i == 5)
								a = a6;
						if (i == 6)
								a = a7;
						if (i == 7)
								a = a8;
						if (a < 5)
								x += a - 5;
						else
								x += a + 12;	
				}
}
				
